package com.jankiel.nendoroidhunter.asynctasks;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.jankiel.genlibs.utils.StringTransformations;
import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.utils.FeedbackListener;
import com.jankiel.nendoroidhunter.utils.Utils;

public class UpdateNendoroidStatusTask extends AsyncTask<String, String, Boolean>
			implements FeedbackListener {
	
	private Context mContext;
	
	private OnUpdateNendoroidStatusCompleteListener mListener;

	private ProgressDialog mProgressDialog;
	
	private DatabaseManager mDbManager;
	private Nendoroid mNendoroid;
	
	private int mOldStatus;
	private int mNewStatus;
	
	private String mExceptionMessage;
	
	public UpdateNendoroidStatusTask(Context context, DatabaseManager dbManager, Nendoroid nendoroid,
			int oldStatus, int newStatus) {
		this.mContext = context;
		this.mDbManager = dbManager;
		this.mNendoroid = nendoroid;
		
		this.mOldStatus = oldStatus;
		this.mNewStatus = newStatus;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_updating_nendoroid, mNendoroid.getDescriptiveName()));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		
		mProgressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			mNendoroid.setStatus(mNewStatus);
			
			if (mNewStatus == Nendoroid.STATUS_LOOKING_FOR
					|| mNewStatus == Nendoroid.STATUS_ACQUIRED_LOOKING_FOR) {
				
				if (mOldStatus != Nendoroid.STATUS_LOOKING_FOR
						&& mOldStatus != Nendoroid.STATUS_ACQUIRED_LOOKING_FOR) {
					mNendoroid.setLookingForPriority("999"); //999 to indicate that this should be at the end of its group
				}
			} 
			if (mNewStatus == Nendoroid.STATUS_ACQUIRED
					|| mNewStatus == Nendoroid.STATUS_ACQUIRED_LOOKING_FOR) {
				
				if (mOldStatus != Nendoroid.STATUS_ACQUIRED
						&& mOldStatus != Nendoroid.STATUS_ACQUIRED_LOOKING_FOR) {
					mNendoroid.setAcquisitionOrder("999"); //999 to indicate that this should be at the end of its group
				}
			}
			
			mDbManager.updateNendoroid(mNendoroid);
			
			//TODO make priority system editable by user
			String whereClause = "";
			List<Nendoroid> recalculatedNendoroidsList = new ArrayList<Nendoroid>();

			publishProgress(mContext.getString(R.string.progress_message_recalculating_acquisition_orders));
			
			//fix the acquisition order first
			int acquisitionOrder = 1;
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ACQUIRED
						+ " OR " + Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ACQUIRED_LOOKING_FOR;
			List<Nendoroid> acquiredNendoroidList 
					= mDbManager.getAllNendoroidsWithConstraint(whereClause, Nendoroid.FIELD_ACQUISITION_ORDER);
			
			for (Nendoroid acquiredNendoroid : acquiredNendoroidList) {
				acquiredNendoroid.setAcquisitionOrder(StringTransformations.padInFront(acquisitionOrder++, 3));
				recalculatedNendoroidsList.add(acquiredNendoroid);
			}
			
			//next are ordered/arrived, because what's only left for them to be acquired is picking them up; 
			//no need to reset counter here to continue the count
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ORDERED_ARRIVED;
			List<Nendoroid> orderedArrivedNendoroidList 
					= mDbManager.getAllNendoroidsWithConstraint(whereClause, Nendoroid.FIELD_ACQUISITION_ORDER);
			
			for (Nendoroid orderedArrivedNendoroid : orderedArrivedNendoroidList) {
				orderedArrivedNendoroid.setAcquisitionOrder(StringTransformations.padInFront(acquisitionOrder++, 3));
				recalculatedNendoroidsList.add(orderedArrivedNendoroid);
			}

			//last in the order are pre-ordered, because these they might get cancelled etc.
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_PRE_ORDERED;
			List<Nendoroid> preOrderedNendoroidList 
					= mDbManager.getAllNendoroidsWithConstraint(whereClause, Nendoroid.FIELD_ACQUISITION_ORDER);
			
			for (Nendoroid preOrderedNendoroid : preOrderedNendoroidList) {
				preOrderedNendoroid.setAcquisitionOrder(StringTransformations.padInFront(acquisitionOrder++, 3));
				recalculatedNendoroidsList.add(preOrderedNendoroid);
			}

			publishProgress(mContext.getString(R.string.progress_message_recalculating_looking_for_priorities));
			
			//in fixing the looking for priorities, the acquired/looking for is always the highest of priorites
			int lookingForPriority = 1;
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ACQUIRED_LOOKING_FOR;
			List<Nendoroid> acquiredLookingForNendoroidList 
					= mDbManager.getAllNendoroidsWithConstraint(whereClause, Nendoroid.FIELD_LOOKING_FOR_PRIORITY);
			
			for (Nendoroid acquiredLookingForNendoroid : acquiredLookingForNendoroidList) {
				int indexInRecalculatedNendoroidList = Utils.nendoroidIndexInList(acquiredLookingForNendoroid, recalculatedNendoroidsList);
				if (indexInRecalculatedNendoroidList != -1) {
					//if existing on recalculatedNendoroidsList, meaning the acquisition order was updated
					//we get the one on the list so as not to undo the update on acquisition order
					acquiredLookingForNendoroid = recalculatedNendoroidsList.get(indexInRecalculatedNendoroidList);
					recalculatedNendoroidsList.remove(acquiredLookingForNendoroid); 
				}
				
				acquiredLookingForNendoroid.setLookingForPriority(StringTransformations.padInFront(lookingForPriority++, 3));
				recalculatedNendoroidsList.add(acquiredLookingForNendoroid);
			}
			
			//next in priority are the looking for ones, we don't reset the lookingForPriority here to continue the count
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_LOOKING_FOR;
			List<Nendoroid> lookingForNendoroidList 
					= mDbManager.getAllNendoroidsWithConstraint(whereClause, Nendoroid.FIELD_LOOKING_FOR_PRIORITY);
			
			for (Nendoroid lookingForNendoroid : lookingForNendoroidList) {
				lookingForNendoroid.setLookingForPriority(StringTransformations.padInFront(lookingForPriority++, 3));
				recalculatedNendoroidsList.add(lookingForNendoroid);
			}
			
			//last in priority are found, because you are looking for them that's why you found them, but might not care that much
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_FOUND;
			List<Nendoroid> foundNendoroidList 
					= mDbManager.getAllNendoroidsWithConstraint(whereClause, Nendoroid.FIELD_LOOKING_FOR_PRIORITY);
			
			for (Nendoroid foundNendoroid : foundNendoroidList) {
				foundNendoroid.setLookingForPriority(StringTransformations.padInFront(lookingForPriority++, 3));
				recalculatedNendoroidsList.add(foundNendoroid);
			}
			
			//publishProgress(mContext.getString(R.string.progress_message_updating_recalculated_nendoroids));
			publishProgress(mContext.getString(R.string.progress_message_updating));
			
			for (Nendoroid recalculatedNendoroid : recalculatedNendoroidsList) {
				mDbManager.updateNendoroid(recalculatedNendoroid);
			}
			
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);
		
		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	public void onFeedback(String... messages) {
		publishProgress(messages[0]);
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_error_updating_nendoroid_exception_message, 
					mNendoroid.getDescriptiveName(), mExceptionMessage));
		}
		
		mListener.onUpdateNendoroidStatusComplete(isSuccessful.equals(Boolean.TRUE));
	}
	
	public void setNendoroid(Nendoroid nendoroid) {
		this.mNendoroid = nendoroid;
	}
	
	public void setOnUpdateNendoroidStatusCompleteListener(OnUpdateNendoroidStatusCompleteListener listener) {
		this.mListener = listener;
	}
	
	public interface OnUpdateNendoroidStatusCompleteListener {
		public void onUpdateNendoroidStatusComplete(boolean isSuccessful);
	}
}
