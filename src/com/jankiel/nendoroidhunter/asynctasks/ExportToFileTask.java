package com.jankiel.nendoroidhunter.asynctasks;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;

import com.jankiel.genlibs.utils.FileOperations;
import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.models.Shop;
import com.jankiel.nendoroidhunter.utils.AppConstant;
import com.jankiel.nendoroidhunter.utils.CSVUtils;
import com.jankiel.nendoroidhunter.utils.FeedbackListener;
import com.jankiel.nendoroidhunter.utils.JsonUtils;
import com.jankiel.nendoroidhunter.utils.XmlUtils;

public class ExportToFileTask extends NendoroidListAsyncTask
			implements FeedbackListener {
	
	private Context mContext;
	
	private OnExportToFileCompleteListener mListener;

	private ProgressDialog mProgressDialog;

	private File mFile;
	private String mFileFormat;
	@SuppressWarnings("rawtypes")
	private List mItemList;
	@SuppressWarnings("unused")
	private int mListToExpect;
	
	private String mExceptionMessage;
	
	@SuppressWarnings("rawtypes")
	public ExportToFileTask(Context context, List itemList, File file, int listToExpect) {
		this.mContext = context;
		
		this.mItemList = itemList;
		this. mFile = file;
		this.mListToExpect = listToExpect;
		
		mFileFormat = FileOperations.getFileFormat(mFile);
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_exporting_to, mFileFormat));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		
		mProgressDialog.show();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Boolean doInBackground(String... params) {
		try {
			if (mItemList == null || mItemList.size() == 0) {
				mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_list_empty);
				return Boolean.FALSE;
			}
			
			FileOutputStream fileOutputStream = new FileOutputStream(mFile);
			
			if (mFileFormat.equals(AppConstant.FILE_FORMAT_XML)) {
				
				if (Nendoroid.isNendoroidList(mItemList)) {
					XmlUtils.serializeNendoroidXml(fileOutputStream, mItemList);
				} else if (Shop.isShopList(mItemList)) {
					//XmlUtils.serializeShopXml(fileOutputStream, mItemList);
				} else {
					mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_unknown_list_type);
					fileOutputStream.close();
					return Boolean.FALSE;
				}		
				
			} else if (mFileFormat.equals(AppConstant.FILE_FORMAT_JSON)) {
				
				if (Nendoroid.isNendoroidList(mItemList)) {
					JsonUtils.writeNendoroidJson(fileOutputStream, mItemList);
				} else if (Shop.isShopList(mItemList)) {
					JsonUtils.writeShopJson(fileOutputStream, mItemList);
				} else {
					mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_unknown_list_type);;
					fileOutputStream.close();
					return Boolean.FALSE;
				}
				
			} else if (mFileFormat.equals(AppConstant.FILE_FORMAT_CSV)) {
				//TODO
				
				if (Nendoroid.isNendoroidList(mItemList)) {
					CSVUtils.writeNendoroidCSV(fileOutputStream, (List<Nendoroid>) mItemList);
				} else if (Shop.isShopList(mItemList)) {
					//CSVUtils.writeShopCSV(fileOutputStream, mItemList);
				} else {
					mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_unknown_list_type);;
					fileOutputStream.close();
					return Boolean.FALSE;
				}
				
			} else {
				
				mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_unsupported_file_type);
				fileOutputStream.close();
				return Boolean.FALSE;
			}
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);
		
		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	public void onFeedback(String... messages) {
		publishProgress(messages[0]);
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_export_error_occurred_exception_message, 
					mFileFormat, mExceptionMessage));
		}
		
		mListener.onExportToFileComplete(mFile, isSuccessful.equals(Boolean.TRUE));
	}
	
	@Override
	public void setNendoroidList(List<Nendoroid> nendoroidList) {
		super.setNendoroidList(nendoroidList);
		
		mItemList = nendoroidList;
	}
	
	@SuppressWarnings("rawtypes")
	public void setItemList(List itemList) {
		mItemList = itemList;
	}
	
	public void setOnExportToFileCompleteListener(OnExportToFileCompleteListener listener) {
		this.mListener = listener;
	}
	
	public interface OnExportToFileCompleteListener {
		public void onExportToFileComplete(File file, boolean isSuccessful);
	}
}
