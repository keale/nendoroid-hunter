package com.jankiel.nendoroidhunter.asynctasks;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.utils.FeedbackListener;
import com.jankiel.nendoroidhunter.utils.NendoroidDataScraper;

public class ScrapeNendoroidDataTask extends NendoroidListAsyncTask
			implements FeedbackListener {
	
	public static final int SCRAPE_ALL = 0;
	public static final int SCRAPE_NON_EXISTING = 1;
	
	private int mScrapeTask;
	
	private Context mContext;
	
	private OnNendoroidDataScrapeCompleteListener mListener;

	private ProgressDialog mProgressDialog;
	
	private NendoroidDataScraper mNendoroidDataScraper;
	private List<Nendoroid> mExistingNendoroidList;
	private List<Nendoroid> mNendoroidList;
	
	private String mExceptionMessage;
	
	public ScrapeNendoroidDataTask(Context context) {
		this(context, SCRAPE_ALL, null);
	}
	
	public ScrapeNendoroidDataTask(Context context, int scrapeTask, List<Nendoroid> existingNendoroidList) {
		this.mContext = context;
		
		this.mScrapeTask = scrapeTask;
		this.mExistingNendoroidList = existingNendoroidList;
		
		if (mScrapeTask == SCRAPE_NON_EXISTING && mExistingNendoroidList != null) {
			mNendoroidDataScraper = new NendoroidDataScraper(mContext, this, mExistingNendoroidList);
		} else {
			mNendoroidDataScraper = new NendoroidDataScraper(mContext, this);
		}
		
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_scraping_nendoroid_data));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		
		mProgressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			mNendoroidList = mNendoroidDataScraper.scrapeNendoroidData();
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);
		
		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	public void onFeedback(String... messages) {
		publishProgress(messages[0]);
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_error_occurred_exception_message, 
					mExceptionMessage));
		}
		
		mListener.onNendoroidDataScrapeComplete(mNendoroidList, isSuccessful.equals(Boolean.TRUE));
	}
	
	@Override
	public void setNendoroidList(List<Nendoroid> nendoroidList) {
		super.setNendoroidList(nendoroidList);
		
		mNendoroidDataScraper.setExistingNendoroidList(nendoroidList);
	}
	
	public void setOnNendoroidDataScrapeCompleteListener(OnNendoroidDataScrapeCompleteListener listener) {
		this.mListener = listener;
	}
	
	public interface OnNendoroidDataScrapeCompleteListener {
		public void onNendoroidDataScrapeComplete(List<Nendoroid> nendoroidList, boolean isSuccessful);
	}
}
