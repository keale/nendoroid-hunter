package com.jankiel.nendoroidhunter.asynctasks;

import java.io.InputStream;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.models.Shop;
import com.jankiel.nendoroidhunter.utils.AppConstant;
import com.jankiel.nendoroidhunter.utils.JsonUtils;

public class LoadInitialDatabaseTask extends AsyncTask<String, String, Boolean> {
	
	private Context mContext;
	
	private DatabaseManager mDbManager;
	
	private OnInitialDatabaseLoadCompleteListener mListener;

	private ProgressDialog mProgressDialog;
	
	private String mExceptionMessage;
	
	public LoadInitialDatabaseTask(Context context, DatabaseManager dbManager) {
		this.mContext = context;
		this.mDbManager = dbManager;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_initializing_database));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		
		mProgressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			publishProgress(mContext.getString(R.string.progress_message_importing_nendoroids_from, 
					AppConstant.FILE_FORMAT_JSON));
			
			InputStream nendoroidInputStream = mContext.getAssets().open("NendoroidList.json");
			List<Nendoroid> nendoroidList = JsonUtils.readNendoroidJson(nendoroidInputStream, mContext);
			
			if (nendoroidList == null || nendoroidList.size() == 0) {
				mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_list_empty);
			}
			
			for (Nendoroid nendoroid : nendoroidList) {
				
				publishProgress(mContext.getString(R.string.progress_message_inserting_nendoroid,
						nendoroid.getDescriptiveName()));
				
				Image thumbImage = nendoroid.getThumbImage();
				if (thumbImage != null) {
					int thumbImageId = (int) mDbManager.insertImage(thumbImage);
					thumbImage.setId(thumbImageId);
					
					nendoroid.setThumbImage(thumbImage);
				}

				Image mainImage = nendoroid.getMainImage();
				if (mainImage != null) {
					int mainImageId = (int) mDbManager.insertImage(mainImage);
					mainImage.setId(mainImageId);
					
					nendoroid.setMainImage(mainImage);
				}
				
				int nendoroidId = (int) mDbManager.insertNendoroid(nendoroid);
				nendoroid.setId(nendoroidId);

				if (thumbImage != null) {
					thumbImage.setNendoroid(nendoroid);
					mDbManager.updateImage(thumbImage);
				}
				
				if (mainImage != null) {
					mainImage.setNendoroid(nendoroid);
					mDbManager.updateImage(mainImage);
				}
				List<Image> imageList = nendoroid.getImageList();
				if (imageList != null) {
					for	(Image image : imageList) {
						if (image != null) {
							image.setNendoroid(nendoroid);
							mDbManager.insertImage(image);
						}
					}
				}
			}
			publishProgress(mContext.getString(R.string.progress_message_importing_shops_from, 
					AppConstant.FILE_FORMAT_JSON));

			InputStream shopInputStream = mContext.getAssets().open("ShopList.json");
			List<Shop> shopList = JsonUtils.readShopJson(shopInputStream, mContext);
			
			for (Shop shop : shopList) {
				publishProgress(mContext.getString(R.string.progress_message_inserting_shop,
						shop.getName()));
				
				mDbManager.insertShop(shop);
			}
			
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);
		
		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_error_occurred_exception_message, 
					mExceptionMessage));
		}
		
		mListener.onInitialDatabaseLoadComplete(isSuccessful.equals(Boolean.TRUE));
	}
	
	public OnInitialDatabaseLoadCompleteListener getOnInitialDatabaseLoadCompleteListener() {
		return mListener;
	}
	
	public void setOnInitialDatabaseLoadCompleteListener(OnInitialDatabaseLoadCompleteListener listener) {
		this.mListener = listener;
	}
	
	public interface OnInitialDatabaseLoadCompleteListener {
		public void onInitialDatabaseLoadComplete(boolean isSuccessful);
	}
}
