package com.jankiel.nendoroidhunter.asynctasks;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.models.Shop;

public class InsertIntoDatabaseTask extends AsyncTask<String, String, Boolean> {
	
	private Context mContext;
	private DatabaseManager mDbManager;
	
	private OnDatabaseInsertCompleteListener mListener;

	private ProgressDialog mProgressDialog;
	@SuppressWarnings("rawtypes")
	private List mItemList;
	
	private String mExceptionMessage;
	
	@SuppressWarnings("rawtypes")
	public InsertIntoDatabaseTask(Context context, DatabaseManager dbManager,
			List itemList) {
		this.mContext = context;
		this.mDbManager = dbManager;
		this.mItemList = itemList;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_initializing_database));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		
		mProgressDialog.show();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Boolean doInBackground(String... params) {
		try {
			if (mItemList == null || mItemList.size() == 0) {
				mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_list_empty);
				return Boolean.FALSE;
			}
			
			if (Nendoroid.isNendoroidList(mItemList)) {
				insertNendoroidList(mItemList);
			} else if (Shop.isShopList(mItemList)) {
				insertShopList(mItemList);
			} else {
				mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_unknown_list_type);
				return Boolean.FALSE;
			}			
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);
		
		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_error_occurred_exception_message, 
					mExceptionMessage));
		}
		
		mListener.onDatabaseInsertComplete(mItemList, isSuccessful.equals(Boolean.TRUE));
	}
	
	public OnDatabaseInsertCompleteListener getOnDatabaseInsertCompleteListener() {
		return mListener;
	}
	
	public void setOnDatabaseInsertCompleteListener(OnDatabaseInsertCompleteListener listener) {
		this.mListener = listener;
	}

	@SuppressWarnings("rawtypes")
	public interface OnDatabaseInsertCompleteListener {
		public void onDatabaseInsertComplete(List itemList, boolean isSuccessful);
	}
	
	private void insertShopList(List<Shop> shopList) 
			throws Exception {
		
		for (Shop shop : shopList) {
			int shopId = (int) mDbManager.insertShop(shop);
			shop.setId(shopId);	
		}
	}
	
	private void insertNendoroidList(List<Nendoroid> nendoroidList) 
			throws Exception {
		for (Nendoroid nendoroid : nendoroidList) {
			publishProgress(mContext.getString(R.string.progress_message_inserting_nendoroid, 
					nendoroid.getDescriptiveName()));
			
			Image thumbImage = nendoroid.getThumbImage();
			if (thumbImage != null) {
				int thumbImageId = (int) mDbManager.insertImage(thumbImage);
				thumbImage.setId(thumbImageId);
				
				nendoroid.setThumbImage(thumbImage);
			}

			Image mainImage = nendoroid.getMainImage();
			if (mainImage != null) {
				int mainImageId = (int) mDbManager.insertImage(mainImage);
				mainImage.setId(mainImageId);
				
				nendoroid.setMainImage(mainImage);
			}
			
			int nendoroidId = (int) mDbManager.insertNendoroid(nendoroid);
			nendoroid.setId(nendoroidId);

			if (thumbImage != null) {
				thumbImage.setNendoroid(nendoroid);
				mDbManager.updateImage(thumbImage);
			}
			
			if (mainImage != null) {
				mainImage.setNendoroid(nendoroid);
				mDbManager.updateImage(mainImage);
			}
			
			List<Image> imageList = nendoroid.getImageList();
			if (imageList != null) {
				for	(Image image : imageList) {
					if (image != null) {
						image.setNendoroid(nendoroid);
						mDbManager.insertImage(image);
					}
				}
			}
		}
	}
}
