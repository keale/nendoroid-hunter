package com.jankiel.nendoroidhunter.asynctasks;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;

import com.jankiel.genlibs.utils.FileOperations;
import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.utils.AppConstant;
import com.jankiel.nendoroidhunter.utils.CSVUtils;
import com.jankiel.nendoroidhunter.utils.FeedbackListener;
import com.jankiel.nendoroidhunter.utils.JsonUtils;
import com.jankiel.nendoroidhunter.utils.XmlUtils;

public class ImportFromFileTask extends NendoroidListAsyncTask implements
		FeedbackListener {

	private Context mContext;

	private OnImportFromFileCompleteListener mListener;

	private ProgressDialog mProgressDialog;

	private File mFile;
	private String mFileFormat;
	@SuppressWarnings("rawtypes")
	private List mItemList;
	private int mListToExpect;

	private String mExceptionMessage;

	public ImportFromFileTask(Context context, File file, int listToExpect) {
		this.mContext = context;
		
		this.mFile = file;
		this.mListToExpect = listToExpect;
		
		mFileFormat = FileOperations.getFileFormat(mFile);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_importing_from, mFileFormat));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);

		mProgressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			FileInputStream fileInputStream = new FileInputStream(mFile);
			
			if (mFileFormat.equalsIgnoreCase(AppConstant.FILE_FORMAT_KML)) {
				
				mItemList = XmlUtils.parseGoogleKml(new FileInputStream(mFile), mContext);
				
			} else if (mFileFormat.equalsIgnoreCase(AppConstant.FILE_FORMAT_XML)) {
				
				if (mListToExpect == AppConstant.EXPECTATION_NENDOROID) {
					mItemList = XmlUtils.parseNendoroidXml(fileInputStream, mContext);
				} else if (mListToExpect == AppConstant.EXPECTATION_SHOP) {
					mItemList = XmlUtils.parseShopXml(fileInputStream, mContext);
				}
				
			} else if (mFileFormat.equalsIgnoreCase(AppConstant.FILE_FORMAT_JSON)) {
				
				if (mListToExpect == AppConstant.EXPECTATION_NENDOROID) {
					mItemList = JsonUtils.readNendoroidJson(fileInputStream, mContext);
				} else if (mListToExpect == AppConstant.EXPECTATION_SHOP) {
					mItemList = JsonUtils.readShopJson(fileInputStream, mContext);
				}
				
			} else if (mFileFormat.equalsIgnoreCase(AppConstant.FILE_FORMAT_CSV)) {
				
				if (mListToExpect == AppConstant.EXPECTATION_NENDOROID) {
					mItemList = CSVUtils.readNendoroidCSV(fileInputStream, mContext);
				} else if (mListToExpect == AppConstant.EXPECTATION_SHOP) {
					//mItemList = CSVUtils.readShopCSV(fileInputStream, mContext);
				}
				
			} else {
				
				mExceptionMessage = mContext.getString(R.string.dialog_message_exception_message_unsupported_file_type);
				fileInputStream.close();
				return Boolean.FALSE;
				
			}
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);

		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	public void onFeedback(String... messages) {
		publishProgress(messages[0]);
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);

		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}

		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_error_occurred_exception_message, 
					mExceptionMessage));
		}

		mListener.onImportFromFileComplete(mItemList, isSuccessful.equals(Boolean.TRUE));
	}
	
	public void setFile(File file) {
		mFile = file;
	}

	public void setOnImportFromFileCompleteListener(OnImportFromFileCompleteListener listener) {
		this.mListener = listener;
	}
	
	@SuppressWarnings("rawtypes")
	public interface OnImportFromFileCompleteListener {
		public void onImportFromFileComplete(List itemList, boolean isSuccessful);
	}
}
