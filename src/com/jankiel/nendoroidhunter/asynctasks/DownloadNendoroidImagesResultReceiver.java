package com.jankiel.nendoroidhunter.asynctasks;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DownloadNendoroidImagesResultReceiver extends ResultReceiver {

	private Receiver mReceiver;
	private static DownloadNendoroidImagesResultReceiver mInstance;

	public static DownloadNendoroidImagesResultReceiver getInstance() {
		return getInstance(new Handler());
	}
	
	public static DownloadNendoroidImagesResultReceiver getInstance(Handler handler) {
		if (mInstance == null) {
			mInstance = new DownloadNendoroidImagesResultReceiver(handler);
		}
		
		return mInstance;
	}
	
	private DownloadNendoroidImagesResultReceiver() {
		this(new Handler());
	}
	
	private DownloadNendoroidImagesResultReceiver(Handler handler) {
		super(handler);
	}
	
	public void setReceiver(Receiver receiver) {
		this.mReceiver = receiver;
	}
	
	public interface Receiver {
		public void onReceiveResult(int resultCode, Bundle resultData);
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		if (mReceiver != null) {
			mReceiver.onReceiveResult(resultCode, resultData);
		}
	}
	
}
