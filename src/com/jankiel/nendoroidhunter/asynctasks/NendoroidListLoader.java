package com.jankiel.nendoroidhunter.asynctasks;

import java.util.List;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.jankiel.nendoroidhunter.adapters.NendoroidListAdapater;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Nendoroid;

public class NendoroidListLoader extends AsyncTaskLoader<List<Nendoroid>> {

	private int mNendoroidListType;
	
	private DatabaseManager mDbManager;
	
	@SuppressWarnings("unused")
	private Context mContext;
	
	public NendoroidListLoader(Context context, DatabaseManager dbManager, int nendoroidListType) {
		super(context);

		this.mContext = context;
		this.mDbManager = dbManager;
		this.mNendoroidListType = nendoroidListType;
	}

	@Override
	public List<Nendoroid> loadInBackground() {
		String whereClause = null;
		String orderClause = null;
		
		switch (mNendoroidListType) {
		case NendoroidListAdapater.TYPE_LOOKING_FOR:
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_LOOKING_FOR
					+ " OR " + Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ACQUIRED_LOOKING_FOR
					+ " OR " + Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_FOUND;
			orderClause = Nendoroid.FIELD_LOOKING_FOR_PRIORITY;
			break;
		case NendoroidListAdapater.TYPE_ACQUIRED:
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ACQUIRED
					+ " OR " + Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ACQUIRED_LOOKING_FOR
					+ " OR " + Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_PRE_ORDERED
					+ " OR " + Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_ORDERED_ARRIVED;
			orderClause = Nendoroid.FIELD_ACQUISITION_ORDER;
			break;
		case NendoroidListAdapater.TYPE_INTERESTED:
			whereClause = Nendoroid.FIELD_STATUS + " = " + Nendoroid.STATUS_INTERESTED;
			orderClause = Nendoroid.FIELD_NUMBER;
			break;
		default:
			whereClause = null;
			orderClause = Nendoroid.FIELD_NUMBER;
			break;
		}

		return mDbManager.getAllNendoroidsWithConstraint(whereClause, orderClause);
	}

}
