package com.jankiel.nendoroidhunter.asynctasks;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.utils.FeedbackListener;

public class GetAllNendoroidsFromDatabaseTask extends AsyncTask<String, String, Boolean>
			implements FeedbackListener {
	
	private final int WHERE_CLAUSE_INDEX = 0;
	private final int ORDER_CLAUSE_INDEX = 1;
	
	private Context mContext;
	
	private OnGetAllNendoroidsCompleteListener mListener;

	private ProgressDialog mProgressDialog;
	
	private DatabaseManager mDbManager;
	private NendoroidListAsyncTask mAsyncTask;
	private List<Nendoroid> mNendoroidList;
	
	private String mExceptionMessage;
	
	public GetAllNendoroidsFromDatabaseTask(Context context, DatabaseManager dbManager, NendoroidListAsyncTask asyncTask) {
		this.mContext = context;
		this.mDbManager = dbManager;
		this.mAsyncTask = asyncTask;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(mContext.getString(R.string.progress_message_querying_database));
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		
		mProgressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			if (params == null || params.length == 0) {
				mNendoroidList = mDbManager.getAllNendoroids();
			} else if (params.length == 1) {
				mNendoroidList = mDbManager.getAllNendoroidsWithConstraint(params[WHERE_CLAUSE_INDEX]);
			} else if (params.length == 2) {
				mNendoroidList = mDbManager.getAllNendoroidsWithConstraint(params[WHERE_CLAUSE_INDEX], params[ORDER_CLAUSE_INDEX]);
			}
		} catch (Exception e) {
			mExceptionMessage = e.getMessage();
			e.printStackTrace();
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	protected void onProgressUpdate(String... messages) {
		super.onProgressUpdate(messages);
		
		if (messages[0] != null && !messages[0].equals("")) {
			mProgressDialog.setMessage(messages[0]);
		}
	}

	@Override
	public void onFeedback(String... messages) {
		publishProgress(messages[0]);
	}

	@Override
	protected void onPostExecute(Boolean isSuccessful) {
		super.onPostExecute(isSuccessful);
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
		if (isSuccessful.equals(Boolean.FALSE)) {
			DialogUtilities.showBasicErrorDialog(mContext, mContext.getString(R.string.dialog_message_error_occurred_exception_message, 
					mExceptionMessage));
		} else if (mAsyncTask != null)  {
			mAsyncTask.setNendoroidList(mNendoroidList);
			mAsyncTask.execute();
		}
		
		if (mAsyncTask == null) {
			mListener.onGetAllNendoroidsComplete(mNendoroidList, isSuccessful.equals(Boolean.TRUE));
		}
	}
	
	public void setOnGetAllNendoroidsCompleteListener(OnGetAllNendoroidsCompleteListener listener) {
		this.mListener = listener;
	}
	
	public interface OnGetAllNendoroidsCompleteListener {
		public void onGetAllNendoroidsComplete(List<Nendoroid> nendoroidList, boolean isSuccessful);
	}
}
