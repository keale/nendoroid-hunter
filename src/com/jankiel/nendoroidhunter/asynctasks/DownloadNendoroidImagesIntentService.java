package com.jankiel.nendoroidhunter.asynctasks;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;

import com.jankiel.genlibs.utils.FileOperations;
import com.jankiel.genlibs.utils.StringTransformations;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.utils.AppConstant;

public class DownloadNendoroidImagesIntentService extends IntentService {

	public static final String EXTRA_KEY_GET_ALL_NENDOROIDS = "com.jankiel.nendoroidhunter.EXTRA_KEY_GET_ALL_NENDOROIDS";
	public static final String EXTRA_KEY_NENDOROID_ID_ARRAY = "com.jankiel.nendoroidhunter.EXTRA_KEY_NENDOROID_ID_ARRAY";
	public static final String EXTRA_KEY_DOWNLOAD_NENDOROID_IMAGES_RESULT_RECEIVER = "com.jankiel.nendoroidhunter.EXTRA_KEY_DOWNLOAD_NENDOROID_IMAGES_RESULT_RECEIVER";
	
	public static final String EXTRA_KEY_DESTINATION_DIRECTORY = "com.jankiel.nendoroidhunter.EXTRA_KEY_DESTINATION_DIRECTORY";
	public static final String EXTRA_KEY_IMAGES_TO_DOWNLOAD = "com.jankiel.nendoroidhunter.EXTRA_KEY_IMAGES_TO_DOWNLOAD";
	
	public static final String BUNDLE_KEY_PROGRESS = "com.jankiel.nendoroidhunter.BUNDLE_KEY_PROGRESS";
	public static final String BUNDLE_KEY_MESSAGE = "com.jankiel.nendoroidhunter.BUNDLE_KEY_MESSAGE";
	public static final String BUNDLE_KEY_MAX = "com.jankiel.nendoroidhunter.BUNDLE_KEY_MAX";
	public static final String BUNDLE_KEY_IMAGES_TO_DOWNLOAD = "com.jankiel.nendoroidhunter.BUNDLE_KEY_IMAGES_TO_DOWNLOAD";
	public static final String BUNDLE_KEY_EXCEPTION = "com.jankiel.nendoroidhunter.BUNDLE_KEY_EXCEPTION";
	
	public static final int BASIC_IMAGES = 0;
	public static final int THUMB_IMAGE = 1;
	public static final int MAIN_IMAGE = 2;
	public static final int OTHER_IMAGES = 3;
	
	//change to result code
	public static final int STATUS_RUNNING = 0;
	public static final int STATUS_FINISHED = 1;
	public static final int STATUS_ERROR = -1;
	
	private Context mContext;
	private ResultReceiver mResultReceiver;
	
	private int mImagesToDownload;
	
	private DatabaseManager mDbManager;
	private List<Nendoroid> mNendoroidList;
	private File mDestinationDirectory;
	
	//http://javatechig.com/android/creating-a-background-service-in-android
	public DownloadNendoroidImagesIntentService(String name) {
		super(name);
	}
	
	public DownloadNendoroidImagesIntentService() {
		this("DownloadNendoroidImagesIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mContext = getApplicationContext();
		mDbManager = DatabaseManager.getInstance(mContext);
		
		mResultReceiver = (ResultReceiver) intent.getParcelableExtra(EXTRA_KEY_DOWNLOAD_NENDOROID_IMAGES_RESULT_RECEIVER);
		Bundle bundle = new Bundle();
		
		mResultReceiver.send(STATUS_RUNNING, Bundle.EMPTY);
		
		mImagesToDownload = intent.getIntExtra(EXTRA_KEY_IMAGES_TO_DOWNLOAD, BASIC_IMAGES);
		
		String destinationDirectory = intent.getStringExtra(EXTRA_KEY_DESTINATION_DIRECTORY);
		if (destinationDirectory == null || destinationDirectory.equals("")) { //TODO or other invalid paths
			destinationDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
		}
		
		this.mDestinationDirectory = new File(destinationDirectory+ "/Android/data/" + mContext.getPackageName() + "/nendoroid-images/");
		if (!mDestinationDirectory.exists()) {
			mDestinationDirectory.mkdirs();
		}
		
		boolean getAllNendoroids = intent.getBooleanExtra(EXTRA_KEY_GET_ALL_NENDOROIDS, false);
		
		bundle.putString(BUNDLE_KEY_MESSAGE, mContext.getString(R.string.progress_message_querying_database));//TODO create keys;
		bundle.putInt(BUNDLE_KEY_PROGRESS, 1);
		bundle.putInt(BUNDLE_KEY_MAX, 600);
		bundle.putInt(BUNDLE_KEY_IMAGES_TO_DOWNLOAD, mImagesToDownload);
		mResultReceiver.send(STATUS_RUNNING, bundle);
		
		int[] nendoroidIdArray = intent.getIntArrayExtra(EXTRA_KEY_NENDOROID_ID_ARRAY);
		
		if (!getAllNendoroids && nendoroidIdArray.length != 0) {
			StringBuilder whereClause = new StringBuilder("");
			
			for (int nendoroidId : nendoroidIdArray) {
				if (whereClause.length() == 0) {
					whereClause.append(Nendoroid.FIELD_ID + " IN(");
				} else {
					whereClause.append(",");
				}
				whereClause.append(nendoroidId);
			}
			whereClause.append(")");
			
			mNendoroidList = mDbManager.getAllNendoroidsWithConstraint(whereClause.toString());
		} else {
			mNendoroidList = mDbManager.getAllNendoroids();
		}

		bundle.putString(BUNDLE_KEY_MESSAGE, mContext.getString(R.string.progress_message_querying_database));
		bundle.putInt(BUNDLE_KEY_PROGRESS, 1);
		bundle.putInt(BUNDLE_KEY_MAX, mNendoroidList.size());
		bundle.putInt(BUNDLE_KEY_IMAGES_TO_DOWNLOAD, mImagesToDownload);
		mResultReceiver.send(STATUS_RUNNING, bundle);
		
		try {
			double progress = 0.0;
			double increment = (mImagesToDownload == BASIC_IMAGES) ? 0.5 : 1.0;

			for (Nendoroid nendoroid : mNendoroidList) {
				if (mImagesToDownload == BASIC_IMAGES || mImagesToDownload == THUMB_IMAGE) {
					progress += increment;
					
					bundle.putString(BUNDLE_KEY_MESSAGE, mContext.getString(R.string.progress_message_downloading_nendoroid_thumbnail_image, 
							nendoroid.getDescriptiveName()));
					bundle.putInt(BUNDLE_KEY_PROGRESS, (int) progress);
					bundle.putInt(BUNDLE_KEY_MAX, mNendoroidList.size());
					bundle.putInt(BUNDLE_KEY_IMAGES_TO_DOWNLOAD, mImagesToDownload);
					
					mResultReceiver.send(STATUS_RUNNING, bundle);
					
					Image thumbImage = nendoroid.getThumbImage();
					if (thumbImage != null && !thumbImage.exists()) {
						downloadImage(nendoroid, thumbImage, "thumbImg");
					}
				}
				
				if (mImagesToDownload == BASIC_IMAGES || mImagesToDownload == MAIN_IMAGE) {
					progress += increment;
					
					bundle.putString(BUNDLE_KEY_MESSAGE, mContext.getString(R.string.progress_message_downloading_nendoroid_main_image, 
							nendoroid.getDescriptiveName()));
					bundle.putInt(BUNDLE_KEY_PROGRESS, (int) progress);
					bundle.putInt(BUNDLE_KEY_MAX, mNendoroidList.size());
					bundle.putInt(BUNDLE_KEY_IMAGES_TO_DOWNLOAD, mImagesToDownload);
					
					mResultReceiver.send(STATUS_RUNNING, bundle);
					
					Image mainImage = nendoroid.getMainImage();
					if (mainImage != null && !mainImage.exists()) {
						if (!nendoroid.getNumber().equals("026")) { //XXX dirty workaround for nendo 026
							downloadImage(nendoroid, mainImage, "mainImg");
						}
					}
				}

				if (mImagesToDownload == OTHER_IMAGES) {
					List<Image> imageList = nendoroid.getImageList();
					
					for (int i = 0; i < imageList.size(); i++) {
						progress += increment;
						
						bundle.putString(BUNDLE_KEY_MESSAGE, mContext.getString(R.string.progress_message_downloading_nendoroid_other_image,
								nendoroid.getDescriptiveName(), StringTransformations.convertNumberToOrdinal(i + 2)));
						bundle.putInt(BUNDLE_KEY_PROGRESS, (int) progress);
						bundle.putInt(BUNDLE_KEY_MAX, nendoroid.getImageList().size());
						bundle.putInt(BUNDLE_KEY_IMAGES_TO_DOWNLOAD, mImagesToDownload);
						
						mResultReceiver.send(STATUS_RUNNING, bundle);
						
						Image image = imageList.get(i);
						if (image != null && !image.exists()) {
							downloadImage(nendoroid, image, "img" + StringTransformations.padInFront(i, 2));
						}
					}
					
					progress = 0.0;
				}
			}
		} catch (Exception e) {
			bundle.putString(BUNDLE_KEY_EXCEPTION, e.getMessage());
			mResultReceiver.send(STATUS_ERROR, bundle);
			e.printStackTrace();
		}
		
		mResultReceiver.send(STATUS_FINISHED, Bundle.EMPTY);
	}
	
	private void downloadImage(Nendoroid nendoroid, Image image, String prefix) throws IOException {
		if (image != null && image.getWebpath() != null && !image.getWebpath().equals("")) {

			String url = image.getWebpath();
			if (url.startsWith("//")) {//FIXME catch other possibilities
				url = "http:" + image.getWebpath();
			}

			Connection.Response response = Jsoup.connect(url).timeout(AppConstant.SIXTY_SECONDS)
					.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
					+ "AppleWebKit/537.36 (KHTML, like Gecko) "
					+ "Chrome/33.0.1750.152 Safari/537.36").ignoreContentType(true)
					.execute();

			byte[] originalImageByteArray = response.bodyAsBytes();
			byte[] resizedImageByteArray;
			
			if (prefix.equalsIgnoreCase("thumbImg")) {
				//double scaleFactor = 0.72; //TODO find better thumbImgs for missing list
				
				resizedImageByteArray = originalImageByteArray;
			} else {
				double scaleFactor = 0.64;
				
				if (url.startsWith("http://s1.tsuki-board.net")) {
					scaleFactor = 1.0;
				}
				
				Bitmap bitmap = BitmapFactory.decodeByteArray(originalImageByteArray, 0, originalImageByteArray.length);
				Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * scaleFactor), 
						(int) (bitmap.getHeight() * scaleFactor), true);
				
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				resizedBitmap.compress(CompressFormat.PNG, 0, byteArrayOutputStream);
				
				resizedImageByteArray = byteArrayOutputStream.toByteArray();
			}
			
			File imageFile = new File(mDestinationDirectory.getAbsolutePath() + File.separator + "nendoroid" 
					+ nendoroid.getNumber() + prefix + "." + FileOperations.getFileFormat(image.getWebpath()));
			
			if (!imageFile.exists()) {
				imageFile.createNewFile();
			}
			
			FileOutputStream outputStream = new FileOutputStream(imageFile, false);
			outputStream.write(resizedImageByteArray);
			outputStream.close();
			
			image.setFilepath(imageFile.getAbsolutePath());
			mDbManager.updateImage(image);
		}
	}
	
	public static boolean isRunning(Context context) {
	    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (DownloadNendoroidImagesIntentService.class.getName()
	        		.equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

}
