package com.jankiel.nendoroidhunter.activities;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.adapters.TabPagerAdapter;
import com.jankiel.nendoroidhunter.fragments.NendoroidListFragment;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.utils.AppConstant;

public class MainActivity extends FragmentActivity 
			implements ActionBar.TabListener {
	
	public static final int RESULT_DB_UPDATED = 100;
	
	public static final int REQUEST_IMPORT_NENDOROID_LIST = 3;
	public static final int REQUEST_SCRAPE_NENDOROID_LIST = 4;
	public static final int REQUEST_LOAD_INITIAL_DATABASE = 5;
	
	public static final String EXTRA_KEY_REQUEST_CODE = "com.jankiel.nendoroidhunter.EXTRA_KEY_REQUEST_CODE";
	
	private TabPagerAdapter mTabPagerAdapter;
	private ViewPager mViewPager;
	
	private SharedPreferences mSharedPreferences;

	@SuppressWarnings("unused")
	private DatabaseManager mDbManager;

	//TODO implement actions on text selection
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		mTabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), this);

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mTabPagerAdapter);

		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		for (int i = 0; i < mTabPagerAdapter.getCount(); i++) {
			Tab tab = actionBar.newTab();
			tab.setText(mTabPagerAdapter.getPageTitle(i));
			tab.setTabListener(this);
			
			actionBar.addTab(tab);
		}
		
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
		mSharedPreferences.edit()
			.putString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, "").commit();
		
		mDbManager = DatabaseManager.getInstance(MainActivity.this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		boolean hasLoadedInitialDatabase = 
			mSharedPreferences.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_HAS_LOADED_INITIAL_DATABASE, false);
		boolean isJankiel = 
				mSharedPreferences.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL, false);
		
		if (!hasLoadedInitialDatabase) {
			if (isJankiel) {
				showScrapeOrImportDialog();
			} else {
				showLoadInitialDatabaseDialog();
			}
			
		}
		
		String userCurrency = mSharedPreferences.getString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, null);
		if (isJankiel && !userCurrency.equals(getString(R.string.prefix_currency_yen))) {//TODO make it possible to change currency if Jankiel
			mSharedPreferences.edit()
				.putString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, getString(R.string.prefix_currency_yen))
				.commit();
		}
		
		if (userCurrency == null) {
			//FIXME get it via a better way
//			Map<String, String> currencyCodeSymbolMap = new HashMap<String, String>();
//			
//			for (Locale locale : Locale.getAvailableLocales()) {
//				try {
//					Currency currency = Currency.getInstance(locale);
//					currencyCodeSymbolMap.put(currency.getCurrencyCode(), currency.getSymbol());
//				} catch (Exception e) {
//					
//				}
//			}
//			
//			Currency currency = Currency.getInstance(Locale.getDefault());
//			userCurrency = currencyCodeSymbolMap.get(currency.getCurrencyCode());
			userCurrency = getString(R.string.prefix_currency_php);
			
			mSharedPreferences.edit()
				.putString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, userCurrency)
				.commit();
		}
	}
	
	@Override
	@SuppressWarnings("unused")
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		NendoroidListFragment currentFragment = 
				(NendoroidListFragment) mTabPagerAdapter.getItem(mViewPager.getCurrentItem());
		boolean returnValue = currentFragment.handleKeyUp(keyCode, event);

		return super.onKeyUp(keyCode, event);
	}
	
	@Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(EXTRA_KEY_REQUEST_CODE, requestCode);
        super.startActivityForResult(intent, requestCode);
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//FIXME resultCode incorrect here
		//Log.e("jankieldev", "onActivityResult");
		//Log.e("jankieldev", "requestCode: " + requestCode);
		//Log.e("jankieldev", "resultCode: " + resultCode);
		if (resultCode == MainActivity.RESULT_DB_UPDATED) {
			for (int i = 0; i < mTabPagerAdapter.getCount(); i++) {
				reloadTab(i);
			}
			if (requestCode == REQUEST_IMPORT_NENDOROID_LIST
					|| requestCode == REQUEST_SCRAPE_NENDOROID_LIST
					|| requestCode == REQUEST_LOAD_INITIAL_DATABASE) {
				mViewPager.setCurrentItem(TabPagerAdapter.TAB_ALL, true);
			}
			
		} else {
			NendoroidListFragment currentFragment = 
					(NendoroidListFragment) mTabPagerAdapter.getItem(mViewPager.getCurrentItem());
			currentFragment.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	public void reloadTab(int position) {
		NendoroidListFragment currentFragment = 
				(NendoroidListFragment) mTabPagerAdapter.getItem(position);
		currentFragment.hideListView(R.string.list_view_loading);
		currentFragment.loadNendoroidList();
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		
	}
	
	private void showScrapeOrImportDialog() {
		//FIXME gets popped up up two times on initial install
		Dialog dialog = DialogUtilities.buildDialog(MainActivity.this, 
				getString(R.string.dialog_title_welcome), getString(R.string.dialog_message_db_empty_scrape_or_import), 
				getString(R.string.dialog_button_scrape), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						DialogUtilities.showDialog(MainActivity.this, 
								getString(R.string.dialog_title_scrape_data), getString(R.string.dialog_message_scrape_data), 
								getString(R.string.dialog_button_start), new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent preferencesIntent = new Intent(MainActivity.this, PreferencesActivity.class);
										
										startActivityForResult(preferencesIntent, REQUEST_SCRAPE_NENDOROID_LIST);
									}
								}, null, null);
					}
				}, 
				getString(R.string.dialog_button_import), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent preferencesIntent = new Intent(MainActivity.this, PreferencesActivity.class);
						
						startActivityForResult(preferencesIntent, REQUEST_IMPORT_NENDOROID_LIST);
					}
				});
		
		//dialog.setCancelable(false);
		dialog.show();
	}
	
	private void showLoadInitialDatabaseDialog() {
		Dialog dialog = DialogUtilities.buildDialog(MainActivity.this, 
				getString(R.string.dialog_title_welcome), getString(R.string.dialog_message_db_empty_load_internal_resource), 
				getString(R.string.dialog_button_start), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent preferencesIntent = new Intent(MainActivity.this, PreferencesActivity.class);
						
						startActivityForResult(preferencesIntent, REQUEST_LOAD_INITIAL_DATABASE);
					}
				});
		
		//dialog.setCancelable(false);
		dialog.show();
	}
}
