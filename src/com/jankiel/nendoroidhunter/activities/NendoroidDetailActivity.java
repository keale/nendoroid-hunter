package com.jankiel.nendoroidhunter.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.adapters.NendoroidListAdapater;
import com.jankiel.nendoroidhunter.fragments.NendoroidDetailFragment;

public class NendoroidDetailActivity extends FragmentActivity {
	
	private NendoroidDetailFragment mNendoroidDetailFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nendoroid_detail);
		
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		
		if (savedInstanceState == null) {
			int nendoroidId = getIntent().getIntExtra(NendoroidDetailFragment.EXTRA_KEY_NENDOROID_ID, -1);
			int nendoroidListType = getIntent().getIntExtra(NendoroidDetailFragment.EXTRA_KEY_NENDOROID_LIST_TYPE, NendoroidListAdapater.TYPE_ALL);
			
			Bundle arguments = new Bundle();
			arguments.putInt(NendoroidDetailFragment.EXTRA_KEY_NENDOROID_ID, nendoroidId);
			
			mNendoroidDetailFragment = new NendoroidDetailFragment(nendoroidListType);
			mNendoroidDetailFragment.setArguments(arguments);
			
			getSupportFragmentManager().beginTransaction()
					.add(R.id.nendoroid_container, mNendoroidDetailFragment)
					.commit();
			
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Log.e("jankieldev", "onBackPressed");
		if (mNendoroidDetailFragment.wasNendoroidUpdated()) {
			Log.e("jankieldev", "wasNendoroidUpdated");
			if (getParent() == null) {
				Log.e("jankieldev", "getChilda");
				setResult(MainActivity.RESULT_DB_UPDATED);
			} else {
				Log.e("jankieldev", "getParent");
				getParent().setResult(MainActivity.RESULT_DB_UPDATED);
			}
		}
	}
	
	@Override
	@SuppressWarnings("unused")
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		boolean returnValue = mNendoroidDetailFragment.handleKeyUp(keyCode, event);

		return super.onKeyUp(keyCode, event);
	}
	
	@Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(MainActivity.EXTRA_KEY_REQUEST_CODE, requestCode);
        super.startActivityForResult(intent, requestCode);
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mNendoroidDetailFragment.onActivityResult(requestCode, resultCode, data);
	}

}
