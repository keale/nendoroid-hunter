package com.jankiel.nendoroidhunter.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.fragments.PreferencesFragment;

public class PreferencesActivity extends FragmentActivity {
	
	private PreferencesFragment mPreferencesFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preferences);
		
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		
		Bundle arguments = new Bundle();
		
		mPreferencesFragment = new PreferencesFragment();
		mPreferencesFragment.setArguments(arguments);
		
		getFragmentManager().beginTransaction()
				.add(R.id.preferences_container, mPreferencesFragment)
				.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	//FIXME crash here on initial db load!
	@Override
	public void onBackPressed() {
		if (mPreferencesFragment.wasDbUpdated()) {
			setResult(MainActivity.RESULT_DB_UPDATED);
		}
		
		super.onBackPressed();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mPreferencesFragment.onActivityResult(requestCode, resultCode, data);
	}
}
