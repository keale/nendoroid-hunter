package com.jankiel.nendoroidhunter.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.fragments.ShopListFragment;

public class ShopListActivity extends FragmentActivity {
	
	private boolean mShouldAddShop;
	
	private ShopListFragment mShopListFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_list);
		
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		Intent intent = getIntent();
		mShouldAddShop = intent.getBooleanExtra(ShopListFragment.EXTRA_KEY_SHOULD_ADD_SHOP, false);
		
		if (mShopListFragment == null) {
			Bundle arguments = new Bundle();
			
			mShopListFragment = new ShopListFragment(mShouldAddShop);
			mShopListFragment.setArguments(arguments);
			
			getSupportFragmentManager().beginTransaction()
					.add(R.id.shop_list_container, mShopListFragment)
					.commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		if (mShouldAddShop) {
			setResult(RESULT_OK);
		}
		
		super.onBackPressed();
	}
	
	@Override
	@SuppressWarnings("unused")
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		boolean returnValue = mShopListFragment.handleKeyUp(keyCode, event);

		return super.onKeyUp(keyCode, event);
	}
	
	@Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(MainActivity.EXTRA_KEY_REQUEST_CODE, requestCode);
        super.startActivityForResult(intent, requestCode);
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mShopListFragment.onActivityResult(requestCode, resultCode, data);
	}

}
