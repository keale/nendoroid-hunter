package com.jankiel.nendoroidhunter.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.fragments.AboutFragment;

public class AboutActivity extends FragmentActivity {
	
	private AboutFragment mAboutFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		mAboutFragment = new AboutFragment();
		getSupportFragmentManager().beginTransaction()
				.add(R.id.about_container, mAboutFragment)
				.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
	
	@Override
	@SuppressWarnings("unused")
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		boolean returnValue = mAboutFragment.handleKeyUp(keyCode, event);
		
		return super.onKeyUp(keyCode, event);
	}
	
	@Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(MainActivity.EXTRA_KEY_REQUEST_CODE, requestCode);
        super.startActivityForResult(intent, requestCode);
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mAboutFragment.onActivityResult(requestCode, resultCode, data);
	}

}
