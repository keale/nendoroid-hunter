package com.jankiel.nendoroidhunter.models;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.jankiel.nendoroidhunter.R;


/***
 * 
 */
public class NendoroidInShop implements Comparable<NendoroidInShop> {
	
	public static final String TABLE_NAME = "NendoroidInShop";
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_NENDOROID = "nendoroid";
	public static final String FIELD_SHOP = "shop";
	public static final String FIELD_LINK = "link";
	public static final String FIELD_EXACT_PRICE = "exactPrice";
	public static final String FIELD_PRICE_RANGE = "priceRange";
	public static final String FIELD_CONDITION = "condition";
	
	public static final int CONDITION_LIB = 0;
	public static final int CONDITION_BIB = 1;
	public static final int CONDITION_MIB = 2;
	public static final int CONDITION_MISB = 3;
	public static final int CONDITION_BNEW = 4;
	
	private int _id;
	private Nendoroid _nendoroid;
	private Shop _shop;
	private String _link;
	private double _exactPrice;
	private String _priceRange;
	private int _condition;
	
	private Context _context;
	
	public NendoroidInShop(Context context) {
		this(context, 0, null, null, null, 0.0, null, 0);
	}
	
	public NendoroidInShop(Context context, int id) {
		this(context, id, null, null, null, 0.0, null, 0);
	}
	
	public NendoroidInShop(Context context, Nendoroid nendoroid, Shop shop, 
			String priceRange, int condition) {
		this(context, 0, nendoroid, shop, null, 0.0, priceRange, condition);
	}
	
	public NendoroidInShop(Context context, int id, Nendoroid nendoroid, Shop shop, 
				String link, double exactPrice, String priceRange, int condition) {
		this._id = id;
		this._nendoroid = nendoroid;
		this._shop = shop;
		this._link = link;
		this._priceRange = priceRange;
		this._exactPrice = exactPrice;
		this._condition = condition;
		
		this._context = context;
	}

	public static String getTableCreationSql() {
		StringBuilder sqlBuilder = new StringBuilder("");
		
		sqlBuilder.append("CREATE TABLE " + TABLE_NAME + "(");
		sqlBuilder.append(FIELD_ID + " INTEGER NOT NULL PRIMARY KEY, ");
		sqlBuilder.append(FIELD_NENDOROID + " INTEGER NOT NULL REFERENCES " 
							+ Nendoroid.TABLE_NAME + "(" + Nendoroid.FIELD_ID + "), ");
		sqlBuilder.append(FIELD_SHOP + " INTEGER NOT NULL REFERENCES " 
							+ Shop.TABLE_NAME + "(" + Shop.FIELD_ID + ") ON DELETE CASCADE, ");
		sqlBuilder.append(FIELD_LINK + " TEXT, ");
		sqlBuilder.append(FIELD_EXACT_PRICE + " REAL, ");
		sqlBuilder.append(FIELD_PRICE_RANGE + " TEXT NOT NULL, ");
		sqlBuilder.append(FIELD_CONDITION + " INTEGER NOT NULL)");
		
		return sqlBuilder.toString();
	}

	public ContentValues packFieldsIntoContentValues() {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(FIELD_ID, _id);
		contentValues.put(FIELD_NENDOROID, _nendoroid.getId());
		contentValues.put(FIELD_SHOP, _shop.getId());
		contentValues.put(FIELD_LINK, _link);
		contentValues.put(FIELD_EXACT_PRICE, _exactPrice);
		contentValues.put(FIELD_PRICE_RANGE, _priceRange);
		contentValues.put(FIELD_CONDITION, _condition);
		
		return contentValues;
	}

	public void initializeByCursor(Cursor cursor) {
		DatabaseManager dbManager = DatabaseManager.getInstance(this._context);
		
		this._id = cursor.getInt(cursor.getColumnIndex(FIELD_ID));
		
		int nendoroidId = cursor.getInt(cursor.getColumnIndex(FIELD_NENDOROID));
		Nendoroid nendoroid = dbManager.getNendoroid(nendoroidId, null);
		
		this._nendoroid = nendoroid;
		
		int shopId = cursor.getInt(cursor.getColumnIndex(FIELD_SHOP));
		Shop shop = dbManager.getShop(shopId);
		
		this._shop = shop;
		
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_LINK))) {
			this._link = cursor.getString(cursor.getColumnIndex(FIELD_LINK));
		} else {
			this._link = null;
		}
		this._exactPrice = cursor.getDouble(cursor.getColumnIndex(FIELD_EXACT_PRICE));
		this._priceRange = cursor.getString(cursor.getColumnIndex(FIELD_PRICE_RANGE));
		this._condition = cursor.getInt(cursor.getColumnIndex(FIELD_CONDITION));
		
		dbManager.closeDatabase();
	}

	public static NendoroidInShop createNewObjectByCursor(Cursor cursor, Context context) {
		DatabaseManager dbManager = DatabaseManager.getInstance(context);
		NendoroidInShop nendoroidInShop = new NendoroidInShop(context);
		
		nendoroidInShop.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
		
		int nendoroidId = cursor.getInt(cursor.getColumnIndex(FIELD_NENDOROID));
		Nendoroid nendoroid = dbManager.getNendoroid(nendoroidId);
		
		nendoroidInShop.setNendoroid(nendoroid);
		
		int shopId = cursor.getInt(cursor.getColumnIndex(FIELD_SHOP));
		Shop shop = dbManager.getShop(shopId);
		
		nendoroidInShop.setShop(shop);
		
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_LINK))) {
			nendoroidInShop.setLink(cursor.getString(cursor.getColumnIndex(FIELD_LINK)));
		} else {
			nendoroidInShop.setLink(null);
		}
		nendoroidInShop.setExactPrice(cursor.getDouble(cursor.getColumnIndex(FIELD_EXACT_PRICE)));
		nendoroidInShop.setPriceRange(cursor.getString(cursor.getColumnIndex(FIELD_PRICE_RANGE)));
		nendoroidInShop.setCondition(cursor.getInt(cursor.getColumnIndex(FIELD_CONDITION)));
		
		return nendoroidInShop;
	}
	
	public int getId() {		
		return _id;
	}
	
	public Nendoroid getNendoroid() {		
		return _nendoroid;
	}
	
	public Shop getShop() {		
		return _shop;
	}
	
	public String getLink() {		
		return _link;
	}
	
	public double getExactPrice() {		
		return _exactPrice;
	}
	
	public String getPriceRange() {		
		return _priceRange;
	}
	
	public int getCondition() {		
		return _condition;
	}

	public Context getContext() {
		return _context;
	}
	
	public void setId(int id) {		
		this._id = id;
	}
	
	public void setNendoroid(Nendoroid nendoroid) {		
		this._nendoroid = nendoroid;
	}
	
	public void setShop(Shop shop) {		
		this._shop = shop;
	}
	
	public void setLink(String link) {		
		this._link = link;
	}
	
	public void setExactPrice(double exactPrice) {		
		this._exactPrice = exactPrice;
	}
	
	public void setExactPrice(String formattedExactPrice, String currencyPrefix) {
		try {
			if (formattedExactPrice.startsWith(currencyPrefix)) {
				formattedExactPrice = formattedExactPrice.substring(currencyPrefix.length());
			}
			formattedExactPrice = formattedExactPrice.replaceAll(",", "");
			
			double exactPrice = Double.parseDouble(formattedExactPrice);
			
			setExactPrice(exactPrice);
		} catch (Exception e) {
			e.printStackTrace();
			setExactPrice(0.0); //FIXME maybe throw this?
		}
	}
	
	public void setPriceRange(String priceRange) {		
		this._priceRange = priceRange;
	}
	
	public void setCondition(int condition) {		
		this._condition = condition;
	}

	public void setContext(Context context) {
		this._context = context;
	}
	
	public void refreshFromDatabase() {
		DatabaseManager dbManager = DatabaseManager.getInstance(_context);
		NendoroidInShop refreshedNendoroidInShop = dbManager.getNendoroidInShop(_id);
		
		_id = refreshedNendoroidInShop.getId();
		_nendoroid = refreshedNendoroidInShop.getNendoroid();
		_shop = refreshedNendoroidInShop.getShop();
		_link = refreshedNendoroidInShop.getLink();
		_exactPrice = refreshedNendoroidInShop.getExactPrice();
		_priceRange = refreshedNendoroidInShop.getPriceRange();
		_condition = refreshedNendoroidInShop.getCondition();
	
	}

	@Override
	public int compareTo(NendoroidInShop another) {
		Integer thisId = getId();
		Integer anotherId = another.getId();
		return thisId.compareTo(anotherId);
	}

	public String getDescriptiveName() {
		return _shop.getName();
	}

	@SuppressWarnings("rawtypes")
	public static boolean isNendoroidInShopList(List itemList) {
		if (itemList == null || itemList.size() == 0) {
			return false;
		}
		
		Object testObject = itemList.get(0);
		return testObject instanceof NendoroidInShop;
	}
	
	public double getExactPriceFromPriceRange(String userCurrency) {
		if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_less_1k, userCurrency))) {
			return 750.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_1k_to_1_5k, userCurrency))) {
			return 1250.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_1_5k_to_2k, userCurrency))) {
			return 1750.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_2k_to_2_5k, userCurrency))) {
			return 2250.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_2_5k_to_3k, userCurrency))) {
			return 2750.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_3k_to_3_5k, userCurrency))) {
			return 3250.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_3_5k_to_4k, userCurrency))) {
			return 3750.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_4k_to_4_5k, userCurrency))) {
			return 4250.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_4_5k_to_5k, userCurrency))) {
			return 4750.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_5k_to_5_5k, userCurrency))) {
			return 5250.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_5_5k_to_6k, userCurrency))) {
			return 5750.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_6k_to_7k, userCurrency))) {
			return 6500.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_7k_to_8k, userCurrency))) {
			return 7500.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_8k_to_9k, userCurrency))) {
			return 8500.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_9k_to_10k, userCurrency))) {
			return 9500.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_10k_to_12k, userCurrency))) {
			return 11000.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_12k_to_15k, userCurrency))) {
			return 13500.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_15k_to_20k, userCurrency))) {
			return 17500.0;
		} else if (_priceRange.equals(_context.getString(R.string.nendoroid_in_shop_price_range_more_20k, userCurrency))) {
			return 22500.0;
		} else {
			return 0.0;
		}
	}
	
	public static String[] getPriceRangeArray(Context context, String userCurrency) {
		return new String[] {
				context.getString(R.string.nendoroid_in_shop_price_range_less_1k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_1k_to_1_5k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_1_5k_to_2k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_2k_to_2_5k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_2_5k_to_3k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_3k_to_3_5k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_3_5k_to_4k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_4k_to_4_5k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_4_5k_to_5k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_5k_to_5_5k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_5_5k_to_6k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_6k_to_7k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_7k_to_8k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_8k_to_9k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_9k_to_10k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_10k_to_12k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_12k_to_15k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_15k_to_20k, userCurrency),
				context.getString(R.string.nendoroid_in_shop_price_range_more_20k, userCurrency),
		};
		
	}

	public static String getConditionName(Context context, int condition) {
		
		switch(condition) {
		case CONDITION_LIB: return context.getString(R.string.nendoroid_in_shop_condition_lib);
		case CONDITION_BIB: return context.getString(R.string.nendoroid_in_shop_condition_bib);
		case CONDITION_MIB: return context.getString(R.string.nendoroid_in_shop_condition_mib);
		case CONDITION_MISB: return context.getString(R.string.nendoroid_in_shop_condition_misb);
		case CONDITION_BNEW: return context.getString(R.string.nendoroid_in_shop_condition_bnew);
		default: return context.getString(R.string.nendoroid_in_shop_condition_invalid);
		}
	}
	
	public static String[][] getConditionNameTable(Context context) {
		return new String[][] {
			{
				String.valueOf(NendoroidInShop.CONDITION_LIB),
				String.valueOf(NendoroidInShop.CONDITION_BIB),
				String.valueOf(NendoroidInShop.CONDITION_MIB),
				String.valueOf(NendoroidInShop.CONDITION_MISB),
				String.valueOf(NendoroidInShop.CONDITION_BNEW)
			},
			{
				NendoroidInShop.getConditionName(context, NendoroidInShop.CONDITION_LIB),
				NendoroidInShop.getConditionName(context, NendoroidInShop.CONDITION_BIB),
				NendoroidInShop.getConditionName(context, NendoroidInShop.CONDITION_MIB),
				NendoroidInShop.getConditionName(context, NendoroidInShop.CONDITION_MISB),
				NendoroidInShop.getConditionName(context, NendoroidInShop.CONDITION_BNEW),
			}
		};
	}
}
