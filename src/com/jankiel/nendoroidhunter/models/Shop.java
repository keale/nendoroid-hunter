package com.jankiel.nendoroidhunter.models;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/***
 * 
 */
public class Shop {
	
	public static final String TABLE_NAME = "Shop";
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_TYPE = "type";
	public static final String FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR = "isGscOfficialDistributor";
	public static final String FIELD_WEBSITE = "website";
	public static final String FIELD_FACEBOOK = "facebook";
	public static final String FIELD_MODE_OF_PAYMENT = "modeOfPayment";
	public static final String FIELD_MAIN_ADDRESS = "mainAddress";
	public static final String FIELD_LATITUDE = "latitude";
	public static final String FIELD_LONGITUDE = "longitude";
	public static final String FIELD_LOCATIONS = "locations";
	public static final String FIELD_CONTACT_NO = "contactNo";
	public static final String FIELD_NOTES = "notes";
	public static final String FIELD_OTHERS = "others";

	public static final int TYPE_UNKNOWN = -1;
	public static final int TYPE_ONLINE = 1;
	public static final int TYPE_CONVENTIONS = 2;
	public static final int TYPE_PHYSICAL = 4;
	//TODO combinations of above; see http://stackoverflow.com/a/648026/2431281
	
	private int _id;
	private String _name;
	private int _type;
	private boolean _isGscOfficialDistributor;
	private String _website;
	private String _facebook;
	private String _modeOfPayment;
	private String _mainAddress;
	private double _latitude;
	private double _longitude;
	private String _locations;
	private String _contactNo;
	private String _notes;
	private String _others;
	
	private Context _context;
	
	public Shop(Context context) {
		this(context, 0, null, TYPE_UNKNOWN, false, null, null, null, null, 0.0, 0.0, null, null, null, null);
	}
	
	public Shop(Context context, int id) {
		this(context, id, null, TYPE_UNKNOWN, false, null, null, null, null, 0.0, 0.0, null, null, null, null);
	}
	
	public Shop(Context context, String name) {
		this(context, 0, name, TYPE_UNKNOWN, false, null, null, null, null, 0.0, 0.0, null, null, null, null);
	}
	
	public Shop(Context context, String name, double latitude, double longitude) {
		this(context, 0, name, TYPE_UNKNOWN, false, null, null, null, null, latitude, longitude, null, null, null, null);
	}
	
	public Shop(Context context, int id, String name, int type, boolean isGscOfficialDistributor, String website, String facebook, String modeOfPayment, String mainAddress, double latitude, double longitude, String locations, String contactNo, String notes, String others) {
		
		this._id = id;
		this._name = name;
		this._type = type;
		this._isGscOfficialDistributor = isGscOfficialDistributor;
		this._website = website;
		this._facebook = facebook;
		this._modeOfPayment = modeOfPayment;
		this._mainAddress = mainAddress;
		this._latitude = latitude;
		this._longitude = longitude;
		this._locations = locations;
		this._contactNo = contactNo;
		this._notes = notes;
		this._others = others;
		
		this._context = context;
	}

	public static String getTableCreationSql() {
		StringBuilder sqlBuilder = new StringBuilder("");
		
		sqlBuilder.append("CREATE TABLE " + TABLE_NAME + "(");
		sqlBuilder.append(FIELD_ID + " INTEGER NOT NULL PRIMARY KEY, ");
		sqlBuilder.append(FIELD_NAME + " TEXT NOT NULL, ");
		sqlBuilder.append(FIELD_TYPE + " INTEGER, ");
		sqlBuilder.append(FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR + " INTEGER, ");
		sqlBuilder.append(FIELD_WEBSITE + " TEXT, ");
		sqlBuilder.append(FIELD_FACEBOOK + " TEXT, ");
		sqlBuilder.append(FIELD_MODE_OF_PAYMENT + " TEXT, ");
		sqlBuilder.append(FIELD_MAIN_ADDRESS + " TEXT, ");
		sqlBuilder.append(FIELD_LATITUDE + " REAL NOT NULL, ");
		sqlBuilder.append(FIELD_LONGITUDE + " REAL NOT NULL, ");
		sqlBuilder.append(FIELD_LOCATIONS + " TEXT, ");
		sqlBuilder.append(FIELD_CONTACT_NO + " TEXT, ");
		sqlBuilder.append(FIELD_NOTES + " TEXT, ");
		sqlBuilder.append(FIELD_OTHERS + " TEXT)");
		
		return sqlBuilder.toString();
	}

	public ContentValues packFieldsIntoContentValues() {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(FIELD_ID, _id);
		contentValues.put(FIELD_NAME, _name);
		contentValues.put(FIELD_TYPE, _type);
		contentValues.put(FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR, _isGscOfficialDistributor);
		if(_website != null) {
			contentValues.put(FIELD_WEBSITE, _website);
		}
		if(_facebook != null) {
			contentValues.put(FIELD_FACEBOOK, _facebook);
		}
		if(_modeOfPayment != null) {
			contentValues.put(FIELD_MODE_OF_PAYMENT, _modeOfPayment);
		}
		if(_mainAddress != null) {
			contentValues.put(FIELD_MAIN_ADDRESS, _mainAddress);
		}
		contentValues.put(FIELD_LATITUDE, _latitude);
		contentValues.put(FIELD_LONGITUDE, _longitude);
		if(_locations != null) {
			contentValues.put(FIELD_LOCATIONS, _locations);
		}
		if(_contactNo != null) {
			contentValues.put(FIELD_CONTACT_NO, _contactNo);
		}
		if(_notes != null) {
			contentValues.put(FIELD_NOTES, _notes);
		}
		if(_others != null) {
			contentValues.put(FIELD_OTHERS, _others);
		}
		
		return contentValues;
	}

	public void initializeByCursor(Cursor cursor) {
		this._id = cursor.getInt(cursor.getColumnIndex(FIELD_ID));
		this._name = cursor.getString(cursor.getColumnIndex(FIELD_NAME));
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_TYPE))) {
			this._type = cursor.getInt(cursor.getColumnIndex(FIELD_TYPE));
		} else {
			this._type = TYPE_UNKNOWN;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR))) {
			this._isGscOfficialDistributor = (cursor.getInt(cursor.getColumnIndex(FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR)) > 0);
		} else {
			this._isGscOfficialDistributor = false;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_WEBSITE))) {
			this._website = cursor.getString(cursor.getColumnIndex(FIELD_WEBSITE));
		} else {
			this._website = null;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_FACEBOOK))) {
			this._facebook = cursor.getString(cursor.getColumnIndex(FIELD_FACEBOOK));
		} else {
			this._facebook = null;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_MODE_OF_PAYMENT))) {
			this._modeOfPayment = cursor.getString(cursor.getColumnIndex(FIELD_MODE_OF_PAYMENT));
		} else {
			this._modeOfPayment = null;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_MAIN_ADDRESS))) {
			this._mainAddress = cursor.getString(cursor.getColumnIndex(FIELD_MAIN_ADDRESS));
		} else {
			this._mainAddress = null;
		}
		this._latitude = cursor.getDouble(cursor.getColumnIndex(FIELD_LATITUDE));
		this._longitude = cursor.getDouble(cursor.getColumnIndex(FIELD_LONGITUDE));
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_LOCATIONS))) {
			this._locations = cursor.getString(cursor.getColumnIndex(FIELD_LOCATIONS));
		} else {
			this._locations = null;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_CONTACT_NO))) {
			this._contactNo = cursor.getString(cursor.getColumnIndex(FIELD_CONTACT_NO));
		} else {
			this._contactNo = null;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_NOTES))) {
			this._notes = cursor.getString(cursor.getColumnIndex(FIELD_NOTES));
		} else {
			this._notes = null;
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_OTHERS))) {
			this._others = cursor.getString(cursor.getColumnIndex(FIELD_OTHERS));
		} else {
			this._others = null;
		}
		
	}

	public static Shop createNewObjectByCursor(Cursor cursor, Context context) {
		Shop shop = new Shop(context);
		
		shop.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
		shop.setName(cursor.getString(cursor.getColumnIndex(FIELD_NAME)));
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_TYPE))) {
			shop.setType(cursor.getInt(cursor.getColumnIndex(FIELD_TYPE)));
		} else {
			shop.setType(TYPE_UNKNOWN);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR))) {
			shop.setIsGscOfficialDistributor((cursor.getInt(cursor.getColumnIndex(FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR)) > 0));
		} else {
			shop.setIsGscOfficialDistributor(false);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_WEBSITE))) {
			shop.setWebsite(cursor.getString(cursor.getColumnIndex(FIELD_WEBSITE)));
		} else {
			shop.setWebsite(null);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_FACEBOOK))) {
			shop.setFacebook(cursor.getString(cursor.getColumnIndex(FIELD_FACEBOOK)));
		} else {
			shop.setFacebook(null);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_MODE_OF_PAYMENT))) {
			shop.setModeOfPayment(cursor.getString(cursor.getColumnIndex(FIELD_MODE_OF_PAYMENT)));
		} else {
			shop.setModeOfPayment(null);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_MAIN_ADDRESS))) {
			shop.setMainAddress(cursor.getString(cursor.getColumnIndex(FIELD_MAIN_ADDRESS)));
		} else {
			shop.setMainAddress(null);
		}
		shop.setLatitude(cursor.getDouble(cursor.getColumnIndex(FIELD_LATITUDE)));
		shop.setLongitude(cursor.getDouble(cursor.getColumnIndex(FIELD_LONGITUDE)));
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_LOCATIONS))) {
			shop.setLocations(cursor.getString(cursor.getColumnIndex(FIELD_LOCATIONS)));
		} else {
			shop.setLocations(null);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_CONTACT_NO))) {
			shop.setContactNo(cursor.getString(cursor.getColumnIndex(FIELD_CONTACT_NO)));
		} else {
			shop.setContactNo(null);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_NOTES))) {
			shop.setNotes(cursor.getString(cursor.getColumnIndex(FIELD_NOTES)));
		} else {
			shop.setNotes(null);
		}
		if(!cursor.isNull(cursor.getColumnIndex(FIELD_OTHERS))) {
			shop.setOthers(cursor.getString(cursor.getColumnIndex(FIELD_OTHERS)));
		} else {
			shop.setOthers(null);		
		}
		
		return shop;
	}
	
	public int getId() {		
		return _id;
	}
	
	public String getName() {		
		return _name;
	}
	
	public int getType() {		
		return _type;
	}
	
	public boolean getIsGscOfficialDistributor() {		
		return _isGscOfficialDistributor;
	}
	
	public String getWebsite() {		
		return _website;
	}
	
	public String getFacebook() {		
		return _facebook;
	}
	
	public String getWebsiteOrFacebook() {		
		if (_website == null || _website.equals("")) {
			return _facebook;
		}
		return _website;
	}
	
	public String getModeOfPayment() {		
		return _modeOfPayment;
	}
	
	public String getMainAddress() {		
		return _mainAddress;
	}
	
	public double getLatitude() {		
		return _latitude;
	}
	
	public double getLongitude() {		
		return _longitude;
	}
	
	public String getLocations() {		
		return _locations;
	}
	
	public String getContactNo() {		
		return _contactNo;
	}
	
	public String getNotes() {		
		return _notes;
	}
	
	public String getOthers() {		
		return _others;
	}
	
	public Context getContext() {
		return _context;
	}
	
	public void setId(int id) {		
		this._id = id;
	}
	
	public void setName(String name) {		
		this._name = name;
	}
	
	public void setType(int type) {		
		this._type = type;
	}
	
	public void setIsGscOfficialDistributor(boolean isGscOfficialDistributor) {		
		this._isGscOfficialDistributor = isGscOfficialDistributor;
	}
	
	public void setWebsite(String website) {		
		this._website = website;
	}
	
	public void setFacebook(String facebook) {		
		this._facebook = facebook;
	}
	
	public void setModeOfPayment(String modeOfPayment) {		
		this._modeOfPayment = modeOfPayment;
	}
	
	public void setMainAddress(String mainAddress) {		
		this._mainAddress = mainAddress;
	}
	
	public void setLatitude(double latitude) {		
		this._latitude = latitude;
	}
	
	public void setLongitude(double longitude) {		
		this._longitude = longitude;
	}
	
	public void setLocations(String locations) {		
		this._locations = locations;
	}
	
	public void setContactNo(String contactNo) {		
		this._contactNo = contactNo;
	}
	
	public void setNotes(String notes) {		
		this._notes = notes;
	}
	
	public void setOthers(String others) {		
		this._others = others;
	}

	public void setContext(Context context) {
		this._context = context;
	}
	
	public void refreshFromDatabase() {
		DatabaseManager dbManager = DatabaseManager.getInstance(_context);
		Shop refreshedShop = dbManager.getShop(_id);
		
		_id = refreshedShop.getId();
		_name = refreshedShop.getName();
		_type = refreshedShop.getType();
		_isGscOfficialDistributor = refreshedShop.getIsGscOfficialDistributor();
		_website = refreshedShop.getWebsite();
		_facebook = refreshedShop.getFacebook();
		_modeOfPayment = refreshedShop.getModeOfPayment();
		_mainAddress = refreshedShop.getMainAddress();
		_latitude = refreshedShop.getLatitude();
		_longitude = refreshedShop.getLongitude();
		_locations = refreshedShop.getLocations();
		_contactNo = refreshedShop.getContactNo();
		_notes = refreshedShop.getNotes();
		_others = refreshedShop.getOthers();
	
	}
	
	public boolean isTypePhysical() {
		return (_type & TYPE_PHYSICAL) == TYPE_PHYSICAL;
	}
	
	public boolean isTypeOnline() {
		return (_type & TYPE_ONLINE) == TYPE_ONLINE;
	}
	
	public boolean isTypeConventions() {
		return (_type & TYPE_CONVENTIONS) == TYPE_CONVENTIONS;
	}
	
	public String getTypeName() {
		StringBuilder typeName = new StringBuilder("");
		
		//TODO move to strings
		if (isTypePhysical()) {
			typeName.append("Physical");
		}
		
		if (isTypeOnline()) {
			if (typeName.length() != 0) {
				typeName.append("/");
			}
			typeName.append("Online");
		}
		
		if (isTypeConventions()) {
			if (typeName.length() != 0) {
				typeName.append("/");
			}
			typeName.append("Conventions");
		}
		
		return typeName.toString();
	}
	
	public boolean hasValidCoordinates() {
		return _latitude != 0.0 && _longitude != 0.0;
	}
	
	//TODO add toStrings, implement comaprable

	@SuppressWarnings("rawtypes")
	public static boolean isShopList(List itemList) {
		if (itemList == null || itemList.size() == 0) {
			return false;
		}
		
		Object testObject = itemList.get(0);
		return testObject instanceof Shop;
	}
	
}