package com.jankiel.nendoroidhunter.models;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseManager extends SQLiteOpenHelper {
	
	private SQLiteDatabase mDatabase;
	private Context mContext;
	
	private static DatabaseManager mInstance;
	
	private DatabaseManager(Context context, String databaseName, int databaseVersion) {
		super(context, databaseName, null, databaseVersion);
		
		this.mContext = context;
	}
	
	public static DatabaseManager getInstance(Context context) {
		return getInstance(context, context.getPackageName() + ".db", 1);
	}
	
	public static DatabaseManager getInstance(Context context, String databaseName, int databaseVersion) {
		if (mInstance == null) {
			mInstance = new DatabaseManager(context, databaseName, databaseVersion);
		}
		
		return mInstance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) throws SQLException {
		db.execSQL(Image.getTableCreationSql());
		db.execSQL(Shop.getTableCreationSql());
		db.execSQL(Nendoroid.getTableCreationSql());
		db.execSQL(NendoroidInShop.getTableCreationSql());
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Image.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + Shop.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + Nendoroid.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + NendoroidInShop.TABLE_NAME);
		onCreate(db);
	}
	
	public void emptyDatabase() {
		mDatabase = getWritableDatabase();

		mDatabase.execSQL("DELETE FROM " + Image.TABLE_NAME + " WHERE 1");
		mDatabase.execSQL("DELETE FROM " + Shop.TABLE_NAME + " WHERE 1");
		mDatabase.execSQL("DELETE FROM " + Nendoroid.TABLE_NAME + " WHERE 1");
		mDatabase.execSQL("DELETE FROM " + NendoroidInShop.TABLE_NAME + " WHERE 1");
	}

	public long insertImage(Image image) throws Exception {
		mDatabase = getWritableDatabase();

		ContentValues values = image.packFieldsIntoContentValues();
		values.remove(Image.FIELD_ID);
		
		long insertedId = mDatabase.insertOrThrow(Image.TABLE_NAME, null, values);
		
		return insertedId;
	}
	
	public Image getImage(int id) {
		return getImage(id, null);
	}
	
	public Image getImage(int id, Nendoroid nendoroid) {
		mDatabase = getReadableDatabase();
		Image image = null;
		
		Cursor cursor = mDatabase.query(Image.TABLE_NAME, null, Image.FIELD_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();

			image = Image.createNewObjectByCursor(cursor, mContext, nendoroid);
			
			cursor.close();
		}
		
		return image;
	}
	
	public ArrayList<Image> getAllImages(Nendoroid nendoroid) {
		return getAllImagesWithSQL(null, null, nendoroid);
	}
	
	public ArrayList<Image> getAllImagesWithConstraint(String whereClause, Nendoroid nendoroid) {
		return getAllImagesWithSQL(whereClause, null, nendoroid);
	}
	
	public ArrayList<Image> getAllImagesWithConstraint(String whereClause, String fieldToOrderBy, Nendoroid nendoroid) {
		return getAllImagesWithSQL(whereClause, fieldToOrderBy, nendoroid);
		
	}
	
	private ArrayList<Image> getAllImagesWithSQL(String whereClause, String orderByClause, Nendoroid nendoroid) {
		ArrayList<Image> imageList = new ArrayList<Image>();
		
		mDatabase = getReadableDatabase();
		//use query
		Cursor cursor = mDatabase.query(Image.TABLE_NAME, null, whereClause, null, 
				null, null, orderByClause, null);
		
		if (cursor != null && cursor.moveToFirst()) {
			do {
				imageList.add(Image.createNewObjectByCursor(cursor, mContext, nendoroid));
			} while (cursor.moveToNext());
			
			cursor.close();
		}
		
		return imageList;
	}
	
	public boolean updateImage(Image image) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = image.packFieldsIntoContentValues();
	    
	    int rowsAffected = mDatabase.update(Image.TABLE_NAME, values, 
	    		Image.FIELD_ID + " = ?",  new String[] { String.valueOf(image.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean softDeleteImage(Image image, String isDeletedColumnName) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = image.packFieldsIntoContentValues();
	    
	    values.remove(isDeletedColumnName);
	    values.put(isDeletedColumnName, true);
	    
	    int rowsAffected = mDatabase.update(Image.TABLE_NAME, values, 
	    		Image.FIELD_ID + " = ?",  new String[] { String.valueOf(image.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean hardDeleteImage(Image image) {
	    mDatabase = getWritableDatabase();
	    mDatabase.execSQL("PRAGMA foreign_keys = ON;");
	    
	    int rowsAffected = mDatabase.delete(Image.TABLE_NAME, 
	    		Image.FIELD_ID + " = ?", new String[] { String.valueOf(image.getId()) });
	    
	    return rowsAffected > 0;
	}

	public long insertShop(Shop shop) throws Exception {
		mDatabase = getWritableDatabase();

		ContentValues values = shop.packFieldsIntoContentValues();
		values.remove(Shop.FIELD_ID);
		
		long insertedId = mDatabase.insertOrThrow(Shop.TABLE_NAME, null, values);
		
		return insertedId;
	}
	
	public Shop getShop(int id) {
		mDatabase = getReadableDatabase();
		Shop shop = null;
		
		Cursor cursor = mDatabase.query(Shop.TABLE_NAME, null, Shop.FIELD_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
			
			shop = Shop.createNewObjectByCursor(cursor, mContext);
			cursor.close();
		}
		
		return shop;
	}
	
	public ArrayList<Shop> getAllShops() {
		return getAllShopsWithSQL(null, null);
	}
	
	public ArrayList<Shop> getAllShopsWithConstraint(String whereClause) {
		return getAllShopsWithSQL(whereClause, null);
	}
	
	public ArrayList<Shop> getAllShopsWithConstraint(String whereClause, String fieldToOrderBy) {
		return getAllShopsWithSQL(whereClause, fieldToOrderBy);
		
	}
	
	private ArrayList<Shop> getAllShopsWithSQL(String whereClause, String orderByClause) {
		ArrayList<Shop> shopList = new ArrayList<Shop>();
		
		mDatabase = getReadableDatabase();
		//use query
		Cursor cursor = mDatabase.query(Shop.TABLE_NAME, null, whereClause, null, 
				null, null, orderByClause, null);
		
		if (cursor != null && cursor.moveToFirst()) {
			do {
				shopList.add(Shop.createNewObjectByCursor(cursor, mContext));
			} while (cursor.moveToNext());
			
			cursor.close();
		}
		
		return shopList;
	}
	
	public boolean updateShop(Shop shop) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = shop.packFieldsIntoContentValues();
	    
	    int rowsAffected = mDatabase.update(Shop.TABLE_NAME, values, 
	    		Shop.FIELD_ID + " = ?",  new String[] { String.valueOf(shop.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean softDeleteShop(Shop shop, String isDeletedColumnName) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = shop.packFieldsIntoContentValues();
	    
	    values.remove(isDeletedColumnName);
	    values.put(isDeletedColumnName, true);
	    
	    int rowsAffected = mDatabase.update(Shop.TABLE_NAME, values, 
	    		Shop.FIELD_ID + " = ?",  new String[] { String.valueOf(shop.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean hardDeleteShop(Shop shop) {
	    mDatabase = getWritableDatabase();
	    mDatabase.execSQL("PRAGMA foreign_keys = ON;");
	    
	    int rowsAffected = mDatabase.delete(Shop.TABLE_NAME, 
	    		Shop.FIELD_ID + " = ?", new String[] { String.valueOf(shop.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean hardDeleteAllShops() {
	    mDatabase = getWritableDatabase();
	    mDatabase.execSQL("DELETE FROM Shop WHERE 1");
	    
	    return true;
	}

	public long insertNendoroid(Nendoroid nendoroid) throws Exception {
		mDatabase = getWritableDatabase();

		ContentValues values = nendoroid.packFieldsIntoContentValues();
		values.remove(Nendoroid.FIELD_ID);
		
		long insertedId = mDatabase.insertOrThrow(Nendoroid.TABLE_NAME, null, values);
		
		return insertedId;
	}

	public Nendoroid getNendoroid(int id) {
		return getNendoroid(id, null);
	}
	
	public Nendoroid getNendoroid(int id, Image image) {
		mDatabase = getReadableDatabase();
		Nendoroid nendoroid = null;
		
		Cursor cursor = mDatabase.query(Nendoroid.TABLE_NAME, null, Nendoroid.FIELD_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
			
			nendoroid = Nendoroid.createNewObjectByCursor(cursor, mContext, image);
			cursor.close();
		}
		
		return nendoroid;
	}
	
	public ArrayList<Nendoroid> getAllNendoroids() {
		return getAllNendoroidsWithSQL(null, null);
	}
	
	public ArrayList<Nendoroid> getAllNendoroidsWithConstraint(String whereClause) {
		return getAllNendoroidsWithSQL(whereClause, null);
	}
	
	public ArrayList<Nendoroid> getAllNendoroidsWithConstraint(String whereClause, String fieldToOrderBy) {
		return getAllNendoroidsWithSQL(whereClause, fieldToOrderBy);
		
	}
	
	private ArrayList<Nendoroid> getAllNendoroidsWithSQL(String whereClause, String orderByClause) {
		ArrayList<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();
		
		mDatabase = getReadableDatabase();
		//use query
		Cursor cursor = mDatabase.query(Nendoroid.TABLE_NAME, null, whereClause, null, 
				null, null, orderByClause, null);
		
		if (cursor != null && cursor.moveToFirst()) {
			do {
				nendoroidList.add(Nendoroid.createNewObjectByCursor(cursor, mContext, null));
			} while (cursor.moveToNext());
			
			cursor.close();
		}
		
		return nendoroidList;
	}
	
	public boolean updateNendoroid(Nendoroid nendoroid) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = nendoroid.packFieldsIntoContentValues();
	    
	    int rowsAffected = mDatabase.update(Nendoroid.TABLE_NAME, values, 
	    		Nendoroid.FIELD_ID + " = ?",  new String[] { String.valueOf(nendoroid.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean softDeleteNendoroid(Nendoroid nendoroid, String isDeletedColumnName) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = nendoroid.packFieldsIntoContentValues();
	    
	    values.remove(isDeletedColumnName);
	    values.put(isDeletedColumnName, true);
	    
	    int rowsAffected = mDatabase.update(Nendoroid.TABLE_NAME, values, 
	    		Nendoroid.FIELD_ID + " = ?",  new String[] { String.valueOf(nendoroid.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean hardDeleteNendoroid(Nendoroid nendoroid) {
	    mDatabase = getWritableDatabase();
	    mDatabase.execSQL("PRAGMA foreign_keys = ON;");
	    
	    int rowsAffected = mDatabase.delete(Nendoroid.TABLE_NAME, 
	    		Nendoroid.FIELD_ID + " = ?", new String[] { String.valueOf(nendoroid.getId()) });
	    
	    return rowsAffected > 0;
	}

	public long insertNendoroidInShop(NendoroidInShop nendoroidInShop) throws Exception {
		mDatabase = getWritableDatabase();

		ContentValues values = nendoroidInShop.packFieldsIntoContentValues();
		values.remove(NendoroidInShop.FIELD_ID);
		
		long insertedId = mDatabase.insertOrThrow(NendoroidInShop.TABLE_NAME, null, values);
		
		return insertedId;
	}
	
	public NendoroidInShop getNendoroidInShop(int id) {
		mDatabase = getReadableDatabase();
		NendoroidInShop nendoroidInShop = null;
		
		Cursor cursor = mDatabase.query(NendoroidInShop.TABLE_NAME, null, NendoroidInShop.FIELD_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
			
			nendoroidInShop = NendoroidInShop.createNewObjectByCursor(cursor, mContext);
			cursor.close();
		}
		
		return nendoroidInShop;
	}
	
	public ArrayList<NendoroidInShop> getAllNendoroidInShops() {
		return getAllNendoroidInShopsWithSQL(null, null);
	}
	
	public ArrayList<NendoroidInShop> getAllNendoroidInShopsWithConstraint(String whereClause) {
		return getAllNendoroidInShopsWithSQL(whereClause, null);
	}
	
	public ArrayList<NendoroidInShop> getAllNendoroidInShopsWithConstraint(String whereClause, String fieldToOrderBy) {
		return getAllNendoroidInShopsWithSQL(whereClause, fieldToOrderBy);
		
	}
	
	private ArrayList<NendoroidInShop> getAllNendoroidInShopsWithSQL(String whereClause, String orderByClause) {
		ArrayList<NendoroidInShop> nendoroidInShopList = new ArrayList<NendoroidInShop>();
		
		mDatabase = getReadableDatabase();
		//use query
		Cursor cursor = mDatabase.query(NendoroidInShop.TABLE_NAME, null, whereClause, null, 
				null, null, orderByClause, null);
		
		if (cursor != null && cursor.moveToFirst()) {
			do {
				nendoroidInShopList.add(NendoroidInShop.createNewObjectByCursor(cursor, mContext));
			} while (cursor.moveToNext());
			
			cursor.close();
		}
		
		return nendoroidInShopList;
	}
	
	public boolean updateNendoroidInShop(NendoroidInShop nendoroidInShop) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = nendoroidInShop.packFieldsIntoContentValues();
	    
	    int rowsAffected = mDatabase.update(NendoroidInShop.TABLE_NAME, values, 
	    		NendoroidInShop.FIELD_ID + " = ?",  new String[] { String.valueOf(nendoroidInShop.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean softDeleteNendoroidInShop(NendoroidInShop nendoroidInShop, String isDeletedColumnName) {
	    mDatabase = getWritableDatabase();
	    
	    ContentValues values = nendoroidInShop.packFieldsIntoContentValues();
	    
	    values.remove(isDeletedColumnName);
	    values.put(isDeletedColumnName, true);
	    
	    int rowsAffected = mDatabase.update(NendoroidInShop.TABLE_NAME, values, 
	    		NendoroidInShop.FIELD_ID + " = ?",  new String[] { String.valueOf(nendoroidInShop.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public boolean hardDeleteNendoroidInShop(NendoroidInShop nendoroidInShop) {
	    mDatabase = getWritableDatabase();
	    mDatabase.execSQL("PRAGMA foreign_keys = ON;");
	    
	    int rowsAffected = mDatabase.delete(NendoroidInShop.TABLE_NAME, 
	    		NendoroidInShop.FIELD_ID + " = ?", new String[] { String.valueOf(nendoroidInShop.getId()) });
	    
	    return rowsAffected > 0;
	}
	
	public Cursor executeSql(String sql) {
		mDatabase = getWritableDatabase();
		
        return mDatabase.rawQuery(sql, null);
	}
	
	/***
	 * Closes the database if it is not null.
	 */
	public void closeDatabase() {
		if(mDatabase != null) {
			mDatabase.close();
		}
	}
	
	public void beginTransaction() {
		mDatabase.beginTransaction();
	}
	
	public void setTransactionSuccesful() {
		mDatabase.setTransactionSuccessful();
	}
	
	public void endTransaction() {
		mDatabase.endTransaction();
	}
}
