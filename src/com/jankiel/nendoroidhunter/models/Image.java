package com.jankiel.nendoroidhunter.models;

import java.io.File;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/***
 * 
 */
public class Image {
	
	public static final String TABLE_NAME = "Image";
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_NENDOROID = "nendoroid";
	public static final String FIELD_FILEPATH = "filepath";
	public static final String FIELD_WEBPATH = "webpath";
	public static final String FIELD_CAPTION = "caption";
	
	private int _id;
	private Nendoroid _nendoroid;
	private String _filepath;
	private String _webpath;
	private String _caption;
	
	private Context _context;
	
	public Image(Context context) {
		this(context, 0, null, null, null, null);
	}
	
	public Image(Context context, int id) {
		this(context, id, null, null, null, null);
	}
	
	public Image(Context context, Nendoroid nendoroid, String webpath) {
		this(context, 0, nendoroid, null, webpath, null);
	}
	
	public Image(Context context, int id, Nendoroid nendoroid, String filepath, String webpath, String caption) {
		
		this._id = id;
		this._nendoroid = nendoroid;
		this._filepath = filepath;
		this._webpath = webpath;
		this._caption = caption;
		
		this._context = context;
	}

	public static String getTableCreationSql() {
		StringBuilder sqlBuilder = new StringBuilder("");
		
		sqlBuilder.append("CREATE TABLE " + TABLE_NAME + "(");
		sqlBuilder.append(FIELD_ID + " INTEGER NOT NULL PRIMARY KEY, ");
		sqlBuilder.append(FIELD_NENDOROID + " INTEGER NOT NULL REFERENCES " 
							+ Nendoroid.TABLE_NAME + "(" + Nendoroid.FIELD_ID + "), ");
		sqlBuilder.append(FIELD_FILEPATH + " TEXT, ");
		sqlBuilder.append(FIELD_WEBPATH + " TEXT NOT NULL, ");
		sqlBuilder.append(FIELD_CAPTION + " TEXT)");
		
		return sqlBuilder.toString();
	}

	public ContentValues packFieldsIntoContentValues() {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(FIELD_ID, _id);
		contentValues.put(FIELD_NENDOROID, _nendoroid.getId());
		if (_filepath != null) {
			contentValues.put(FIELD_FILEPATH, _filepath);
		}
		contentValues.put(FIELD_WEBPATH, _webpath);
		if (_caption != null) {
			contentValues.put(FIELD_CAPTION, _caption);
		}
		
		return contentValues;
	}

	public void initializeByCursor(Cursor cursor, Nendoroid nendoroid) {
		DatabaseManager dbManager = DatabaseManager.getInstance(this._context);

		this._id = cursor.getInt(cursor.getColumnIndex(FIELD_ID));
		
		int nendoroidId = cursor.getInt(cursor.getColumnIndex(FIELD_NENDOROID));
		if (nendoroid != null && nendoroid.getId() != nendoroidId) {
			nendoroid = dbManager.getNendoroid(nendoroidId, this);
		}
		this._nendoroid = nendoroid;

		if (!cursor.isNull(cursor.getColumnIndex(FIELD_FILEPATH))) {
			this._filepath = cursor.getString(cursor.getColumnIndex(FIELD_FILEPATH));
		} else {
			this._filepath = null;
		}
		this._webpath = cursor.getString(cursor.getColumnIndex(FIELD_WEBPATH));
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_CAPTION))) {
			this._caption = cursor.getString(cursor.getColumnIndex(FIELD_CAPTION));
		} else {
			this._caption = null;
		}
		
	}

	public static Image createNewObjectByCursor(Cursor cursor, Context context, Nendoroid nendoroid) {
		DatabaseManager dbManager = DatabaseManager.getInstance(context);
		Image image = new Image(context);
		
		image.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
		
		int nendoroidId = cursor.getInt(cursor.getColumnIndex(FIELD_NENDOROID));
		if (nendoroid == null || (nendoroid != null && nendoroid.getId() != nendoroidId)) {
			nendoroid = dbManager.getNendoroid(nendoroidId, image);
		}
		
		image.setNendoroid(nendoroid);

		if (!cursor.isNull(cursor.getColumnIndex(FIELD_FILEPATH))) {
			image.setFilepath(cursor.getString(cursor.getColumnIndex(FIELD_FILEPATH)));
		} else {
			image.setFilepath(null);
		}
		image.setWebpath(cursor.getString(cursor.getColumnIndex(FIELD_WEBPATH)));
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_CAPTION))) {
			image.setCaption(cursor.getString(cursor.getColumnIndex(FIELD_CAPTION)));
		} else {
			image.setCaption(null);
		}
		
		return image;
	}
	
	public int getId() {		
		return _id;
	}

	public Nendoroid getNendoroid() {
		return _nendoroid;
	}
	
	public String getFilepath() {		
		return _filepath;
	}
	
	public String getWebpath() {		
		return _webpath;
	}
	
	public String getCaption() {		
		return _caption;
	}

	public Context getContext() {
		return _context;
	}
	
	public void setId(int id) {		
		this._id = id;
	}

	public void setNendoroid(Nendoroid _nendoroid) {
		this._nendoroid = _nendoroid;
	}
	
	public void setFilepath(String filepath) {
		this._filepath = filepath;
	}
	
	public void setWebpath(String webpath) {
		this._webpath = webpath;
	}
	
	public void setCaption(String caption) {		
		this._caption = caption;
	}

	public void setContext(Context context) {
		this._context = context;
	}
	
	public void refreshFromDatabase() {
		DatabaseManager dbManager = DatabaseManager.getInstance(_context);
		Image refreshedImage = dbManager.getImage(_id);
		
		_id = refreshedImage.getId();
		_nendoroid = refreshedImage.getNendoroid();
		_filepath = refreshedImage.getFilepath();
		_webpath = refreshedImage.getWebpath();
		_caption = refreshedImage.getCaption();
	
	}
	
	public boolean exists() {
		if (_filepath == null || _filepath.equals("")) {
			return false;
		}
		
		File imageFile = new File(_filepath);
		return imageFile.exists();
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isImageList(List itemList) {
		if (itemList == null || itemList.size() == 0) {
			return false;
		}
		
		Object testObject = itemList.get(0);
		return testObject instanceof Image;
	}
	
}
