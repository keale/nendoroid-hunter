package com.jankiel.nendoroidhunter.models;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.jankiel.genlibs.utils.StringTransformations;
import com.jankiel.nendoroidhunter.R;

/***
 * 
 */
public class Nendoroid implements Comparable<Nendoroid> {
	
	public static final String TABLE_NAME = "Nendoroid";
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_NUMBER = "number";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_STATUS = "status";
	public static final String FIELD_LOOKING_FOR_PRIORITY = "lookingForPriority";
	public static final String FIELD_ACQUISITION_ORDER = "acquisitionOrder";
	public static final String FIELD_MAIN_IMAGE = "mainImage";
	public static final String FIELD_THUMB_IMAGE = "thumbImage";
	public static final String FIELD_GSC_LINK = "gscLink"; //TODO add mfc link and main data sources
	public static final String FIELD_GSC_PRICE = "gscPrice";
	public static final String FIELD_RELEASE_DATE = "releaseDate";
	public static final String FIELD_NICKNAME = "nickname";
	public static final String FIELD_JAPANESE_NAME = "japaneseName";
	public static final String FIELD_OFFICIAL_NAME = "officialName";
	public static final String FIELD_SERIES = "series";
	public static final String FIELD_MANUFACTURER = "manufacturer";
	public static final String FIELD_CATEGORY = "category";
	public static final String FIELD_SPECIFICATIONS = "specifications";
	public static final String FIELD_SCULPTOR = "sculptor";
	public static final String FIELD_COPYRIGHT = "copyright";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_NOTES = "notes";
	public static final String FIELD_OTHERS = "others";
	
	public static final String FIELD_IMAGE_LIST = "imageList";

	public static final int STATUS_NOT_INTERESTED = -1;
	public static final int STATUS_INTERESTED = 0;
	public static final int STATUS_LOOKING_FOR = 1;
	public static final int STATUS_FOUND = 2;
	public static final int STATUS_PRE_ORDERED = 3;
	public static final int STATUS_ORDERED_ARRIVED = 4;
	public static final int STATUS_ACQUIRED = 5;
	public static final int STATUS_ACQUIRED_LOOKING_FOR = 6;
	
	private int _id;
	private String _number;
	private String _name;
	private int _status;
	private String _lookingForPriority;
	private String _acquisitionOrder;
	private Image _mainImage;
	private Image _thumbImage;
	private String _gscLink;
	private double _gscPrice;
	private String _releaseDate;
	private String _nickname;
	private String _japaneseName;
	private String _officialName;
	private String _series;
	private String _manufacturer;
	private String _category;
	private String _specifications;
	private String _sculptor;
	private String _copyright;
	private String _description;
	private String _notes;
	private String _others;
	
	private List<Image> _imageList;
	
	private Context _context;
	
	public Nendoroid(Context context) {
		this(context, 0, null, null, STATUS_NOT_INTERESTED, null, null, null, null, 
			null, 0.0, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null);
	}
	
	public Nendoroid(Context context, int id) {
		this(context, id, null, null, STATUS_NOT_INTERESTED, null, null, null, null, 
			null, 0.0, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null);
	}
	
	public Nendoroid(Context context, String number, String name) {
		this(context, 0, number, name, STATUS_NOT_INTERESTED, null, null, null, null, 
			null, 0.0, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null);
	}
	
	public Nendoroid(Context context, String number, String name, int status) {
		this(context, 0, number, name, status, null, null, null, null, 
			null, 0.0, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null);
	}
	
	public Nendoroid(Context context, String number, String name, int status, 
				String lookingForPriority, String acquisitionOrder) {
		this(context, 0, number, name, status, lookingForPriority, acquisitionOrder, 
			null, null, null, 0.0, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null);
	}
	
	public Nendoroid(Context context, int id, String number, String name, 
				int status, String lookingForPriority, String acquisitionOrder, 
				Image mainImage, Image thumbImage, String gscLink, double gscPrice, 
				String hubbyteLink, String releaseDate, String nickname, 
				String japaneseName, String officialName, String series, 
				String manufacturer, String category, String specifications, 
				String sculptor, String copyright, String description, 
				String notes, String others) {
		
		this._id = id;
		this._number = number;
		this._name = name;
		this._status = status;
		this._lookingForPriority = lookingForPriority;
		this._acquisitionOrder = acquisitionOrder;
		this._mainImage = mainImage;
		this._thumbImage = thumbImage;
		this._gscLink = gscLink;
		this._gscPrice = gscPrice;
		this._releaseDate = releaseDate;
		this._nickname = nickname;
		this._japaneseName = japaneseName;
		this._officialName = officialName;
		this._series = series;
		this._manufacturer = manufacturer;
		this._category = category;
		this._specifications = specifications;
		this._sculptor = sculptor;
		this._copyright = copyright;
		this._description = description;
		this._notes = notes;
		this._others = others;
		
		this._context = context;
	}

	public String getTableName() {
		return TABLE_NAME;
	}

	public static String getTableCreationSql() {
		StringBuilder sqlBuilder = new StringBuilder("");
		
		sqlBuilder.append("CREATE TABLE " + TABLE_NAME + "(");
		sqlBuilder.append(FIELD_ID + " INTEGER NOT NULL PRIMARY KEY, ");
		sqlBuilder.append(FIELD_NUMBER + " TEXT NOT NULL, ");
		sqlBuilder.append(FIELD_NAME + " TEXT NOT NULL, ");
		sqlBuilder.append(FIELD_STATUS + " INTEGER, ");
		sqlBuilder.append(FIELD_LOOKING_FOR_PRIORITY + " TEXT, ");
		sqlBuilder.append(FIELD_ACQUISITION_ORDER + " TEXT, ");
		sqlBuilder.append(FIELD_MAIN_IMAGE + " INTEGER REFERENCES " 
				+ Image.TABLE_NAME + "(" + Image.FIELD_ID + "), ");
		sqlBuilder.append(FIELD_THUMB_IMAGE + " INTEGER REFERENCES " 
				+ Image.TABLE_NAME + "(" + Image.FIELD_ID + "), ");
		sqlBuilder.append(FIELD_GSC_LINK + " TEXT, ");
		sqlBuilder.append(FIELD_GSC_PRICE + " REAL, ");
		sqlBuilder.append(FIELD_RELEASE_DATE + " TEXT, ");
		sqlBuilder.append(FIELD_NICKNAME + " TEXT, ");
		sqlBuilder.append(FIELD_JAPANESE_NAME + " TEXT, ");
		sqlBuilder.append(FIELD_OFFICIAL_NAME + " TEXT, ");
		sqlBuilder.append(FIELD_SERIES + " TEXT, ");
		sqlBuilder.append(FIELD_MANUFACTURER + " TEXT, ");
		sqlBuilder.append(FIELD_CATEGORY + " TEXT, ");
		sqlBuilder.append(FIELD_SPECIFICATIONS + " TEXT, ");
		sqlBuilder.append(FIELD_SCULPTOR + " TEXT, ");
		sqlBuilder.append(FIELD_COPYRIGHT + " TEXT, ");
		sqlBuilder.append(FIELD_DESCRIPTION + " TEXT, ");
		sqlBuilder.append(FIELD_NOTES + " TEXT, ");
		sqlBuilder.append(FIELD_OTHERS + " TEXT)");
		
		return sqlBuilder.toString();
	}

	public ContentValues packFieldsIntoContentValues() {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(FIELD_ID, _id);
		contentValues.put(FIELD_NUMBER, _number);
		contentValues.put(FIELD_NAME, _name);
		contentValues.put(FIELD_STATUS, _status);
		if (_lookingForPriority != null) {
			contentValues.put(FIELD_LOOKING_FOR_PRIORITY, _lookingForPriority);
		}
		if (_acquisitionOrder != null) {
			contentValues.put(FIELD_ACQUISITION_ORDER, _acquisitionOrder);
		}
		if (_mainImage != null) {
			contentValues.put(FIELD_MAIN_IMAGE, _mainImage.getId());
		}
		if (_thumbImage != null) {
			contentValues.put(FIELD_THUMB_IMAGE, _thumbImage.getId());
		}
		if (_gscLink != null) {
			contentValues.put(FIELD_GSC_LINK, _gscLink);
		}
		contentValues.put(FIELD_GSC_PRICE, _gscPrice);
		if (_releaseDate != null) {
			contentValues.put(FIELD_RELEASE_DATE, _releaseDate);
		}
		if (_nickname != null) {
			contentValues.put(FIELD_NICKNAME, _nickname);
		}
		if (_japaneseName != null) {
			contentValues.put(FIELD_JAPANESE_NAME, _japaneseName);
		}
		if (_officialName != null) {
			contentValues.put(FIELD_OFFICIAL_NAME, _officialName);
		}
		if (_series != null) {
			contentValues.put(FIELD_SERIES, _series);
		}
		if (_manufacturer != null) {
			contentValues.put(FIELD_MANUFACTURER, _manufacturer);
		}
		if (_category != null) {
			contentValues.put(FIELD_CATEGORY, _category);
		}
		if (_specifications != null) {
			contentValues.put(FIELD_SPECIFICATIONS, _specifications);
		}
		if (_sculptor != null) {
			contentValues.put(FIELD_SCULPTOR, _sculptor);
		}
		if (_copyright != null) {
			contentValues.put(FIELD_COPYRIGHT, _copyright);
		}
		if (_description != null) {
			contentValues.put(FIELD_DESCRIPTION, _description);
		}
		if (_notes != null) {
			contentValues.put(FIELD_NOTES, _notes);
		}
		if (_others != null) {
			contentValues.put(FIELD_OTHERS, _others);
		}
		
		return contentValues;
	}

	public void initializeByCursor(Cursor cursor, Image image) {
		DatabaseManager dbManager = DatabaseManager.getInstance(this._context);
		
		this._id = cursor.getInt(cursor.getColumnIndex(FIELD_ID));
		this._number = cursor.getString(cursor.getColumnIndex(FIELD_NUMBER));
		this._name = cursor.getString(cursor.getColumnIndex(FIELD_NAME));
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_STATUS))) {
			this._status = cursor.getInt(cursor.getColumnIndex(FIELD_STATUS));
		} else {
			this._status = STATUS_NOT_INTERESTED;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_LOOKING_FOR_PRIORITY))) {
			this._lookingForPriority = cursor.getString(cursor.getColumnIndex(FIELD_LOOKING_FOR_PRIORITY));
		} else {
			this._lookingForPriority = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_ACQUISITION_ORDER))) {
			this._acquisitionOrder = cursor.getString(cursor.getColumnIndex(FIELD_ACQUISITION_ORDER));
		} else {
			this._acquisitionOrder = null;
		}
		
		int mainImageId = cursor.getInt(cursor.getColumnIndex(FIELD_MAIN_IMAGE));
		if (mainImageId != 0) {
			if (image != null && image.getId() == mainImageId) {
				this._mainImage = image;
			} else {
				Image mainImage = dbManager.getImage(mainImageId, this);
				this._mainImage = mainImage;
			}
		} else {
			this._mainImage = null;
		}
		
		int thumbImageId = cursor.getInt(cursor.getColumnIndex(FIELD_THUMB_IMAGE));
		if (thumbImageId != 0) {
			if (image != null && image.getId() == thumbImageId) {
				this._thumbImage = image;
			} else {
				Image thumbImage = dbManager.getImage(thumbImageId, this);
				this._thumbImage = thumbImage;
			}
		} else {
			this._thumbImage = null;
		}
		
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_GSC_LINK))) {
			this._gscLink = cursor.getString(cursor.getColumnIndex(FIELD_GSC_LINK));
		} else {
			this._gscLink = null;
		}
		this._gscPrice = cursor.getDouble(cursor.getColumnIndex(FIELD_GSC_PRICE));
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_RELEASE_DATE))) {
			this._releaseDate = cursor.getString(cursor.getColumnIndex(FIELD_RELEASE_DATE));
		} else {
			this._releaseDate = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_NICKNAME))) {
			this._nickname = cursor.getString(cursor.getColumnIndex(FIELD_NICKNAME));
		} else {
			this._nickname = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_JAPANESE_NAME))) {
			this._japaneseName = cursor.getString(cursor.getColumnIndex(FIELD_JAPANESE_NAME));
		} else {
			this._japaneseName = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_OFFICIAL_NAME))) {
			this._officialName = cursor.getString(cursor.getColumnIndex(FIELD_OFFICIAL_NAME));
		} else {
			this._officialName = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_SERIES))) {
			this._series = cursor.getString(cursor.getColumnIndex(FIELD_SERIES));
		} else {
			this._series = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_MANUFACTURER))) {
			this._manufacturer = cursor.getString(cursor.getColumnIndex(FIELD_MANUFACTURER));
		} else {
			this._manufacturer = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_CATEGORY))) {
			this._category = cursor.getString(cursor.getColumnIndex(FIELD_CATEGORY));
		} else {
			this._category = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_SPECIFICATIONS))) {
			this._specifications = cursor.getString(cursor.getColumnIndex(FIELD_SPECIFICATIONS));
		} else {
			this._specifications = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_SCULPTOR))) {
			this._sculptor = cursor.getString(cursor.getColumnIndex(FIELD_SCULPTOR));
		} else {
			this._sculptor = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_COPYRIGHT))) {
			this._copyright = cursor.getString(cursor.getColumnIndex(FIELD_COPYRIGHT));
		} else {
			this._copyright = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_DESCRIPTION))) {
			this._description = cursor.getString(cursor.getColumnIndex(FIELD_DESCRIPTION));
		} else {
			this._description = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_NOTES))) {
			this._notes = cursor.getString(cursor.getColumnIndex(FIELD_NOTES));
		} else {
			this._notes = null;
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_OTHERS))) {
			this._others = cursor.getString(cursor.getColumnIndex(FIELD_OTHERS));
		} else {
			this._others = null;
		}

		String whereClause = Image.FIELD_NENDOROID + " = " + this._id
				+ " AND " + Image.FIELD_ID + " NOT IN (";
		if (this._thumbImage != null) {
			whereClause += this._thumbImage.getId(); 
		}
		if (this._mainImage != null) {
			if (this._thumbImage != null) {
				whereClause += ", "; 
			}
			
			whereClause += this._mainImage.getId(); 
		}
		whereClause += ")";
		this._imageList = dbManager.getAllImagesWithConstraint(whereClause, this);
		
	}

	public static Nendoroid createNewObjectByCursor(Cursor cursor, Context context, Image image) {
		DatabaseManager dbManager = DatabaseManager.getInstance(context);
		Nendoroid nendoroid = new Nendoroid(context);
		
		nendoroid.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
		nendoroid.setNumber(cursor.getString(cursor.getColumnIndex(FIELD_NUMBER)));
		nendoroid.setName(cursor.getString(cursor.getColumnIndex(FIELD_NAME)));
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_STATUS))) {
			nendoroid.setStatus(cursor.getInt(cursor.getColumnIndex(FIELD_STATUS)));
		} else {
			nendoroid.setStatus(STATUS_NOT_INTERESTED);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_LOOKING_FOR_PRIORITY))) {
			nendoroid.setLookingForPriority(cursor.getString(cursor.getColumnIndex(FIELD_LOOKING_FOR_PRIORITY)));
		} else {
			nendoroid.setLookingForPriority(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_ACQUISITION_ORDER))) {
			nendoroid.setAcquisitionOrder(cursor.getString(cursor.getColumnIndex(FIELD_ACQUISITION_ORDER)));
		} else {
			nendoroid.setAcquisitionOrder(null);	
		}
		
		int mainImageId = cursor.getInt(cursor.getColumnIndex(FIELD_MAIN_IMAGE));
		if (mainImageId != 0) {
			if (image != null && image.getId() == mainImageId) {
				nendoroid.setMainImage(image);
			} else {
				Image mainImage = dbManager.getImage(mainImageId, nendoroid);
				nendoroid.setMainImage(mainImage);
			}
		} else {
			nendoroid.setMainImage(null);
		}
		
		int thumbImageId = cursor.getInt(cursor.getColumnIndex(FIELD_THUMB_IMAGE));
		if (thumbImageId != 0) {
			if (image != null && image.getId() == thumbImageId) {
				nendoroid.setThumbImage(image);
			} else {
				Image thumbImage = dbManager.getImage(thumbImageId, nendoroid);
				nendoroid.setThumbImage(thumbImage);
			}
		} else {
			nendoroid.setThumbImage(null);
		}
		
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_GSC_LINK))) {
			nendoroid.setGscLink(cursor.getString(cursor.getColumnIndex(FIELD_GSC_LINK)));
		} else {
			nendoroid.setGscLink(null);
		}
		nendoroid.setGscPrice(cursor.getDouble(cursor.getColumnIndex(FIELD_GSC_PRICE)));
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_RELEASE_DATE))) {
			nendoroid.setReleaseDate(cursor.getString(cursor.getColumnIndex(FIELD_RELEASE_DATE)));
		} else {
			nendoroid.setReleaseDate(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_NICKNAME))) {
			nendoroid.setNickname(cursor.getString(cursor.getColumnIndex(FIELD_NICKNAME)));
		} else {
			nendoroid.setNickname(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_JAPANESE_NAME))) {
			nendoroid.setJapaneseName(cursor.getString(cursor.getColumnIndex(FIELD_JAPANESE_NAME)));
		} else {
			nendoroid.setJapaneseName(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_OFFICIAL_NAME))) {
			nendoroid.setOfficialName(cursor.getString(cursor.getColumnIndex(FIELD_OFFICIAL_NAME)));
		} else {
			nendoroid.setOfficialName(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_SERIES))) {
			nendoroid.setSeries(cursor.getString(cursor.getColumnIndex(FIELD_SERIES)));
		} else {
			nendoroid.setSeries(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_MANUFACTURER))) {
			nendoroid.setManufacturer(cursor.getString(cursor.getColumnIndex(FIELD_MANUFACTURER)));
		} else {
			nendoroid.setManufacturer(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_CATEGORY))) {
			nendoroid.setCategory(cursor.getString(cursor.getColumnIndex(FIELD_CATEGORY)));
		} else {
			nendoroid.setCategory(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_SPECIFICATIONS))) {
			nendoroid.setSpecifications(cursor.getString(cursor.getColumnIndex(FIELD_SPECIFICATIONS)));
		} else {
			nendoroid.setSpecifications(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_SCULPTOR)))
			nendoroid.setSculptor(cursor.getString(cursor.getColumnIndex(FIELD_SCULPTOR)));
		else nendoroid.setSculptor(null);	
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_COPYRIGHT))) {
			nendoroid.setCopyright(cursor.getString(cursor.getColumnIndex(FIELD_COPYRIGHT)));
		} else {
			nendoroid.setCopyright(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_DESCRIPTION))) {
			nendoroid.setDescription(cursor.getString(cursor.getColumnIndex(FIELD_DESCRIPTION)));
		} else {
			nendoroid.setDescription(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_NOTES))) {
			nendoroid.setNotes(cursor.getString(cursor.getColumnIndex(FIELD_NOTES)));
		} else {
			nendoroid.setNotes(null);
		}
		if (!cursor.isNull(cursor.getColumnIndex(FIELD_OTHERS))) {
			nendoroid.setOthers(cursor.getString(cursor.getColumnIndex(FIELD_OTHERS)));
		} else {
			nendoroid.setOthers(null);
		}

		String whereClause = Image.FIELD_NENDOROID + " = " + nendoroid.getId()
				+ " AND " + Image.FIELD_ID + " NOT IN (";
		if (nendoroid.getThumbImage() != null) {
			whereClause += nendoroid.getThumbImage().getId(); 
		}
		if (nendoroid.getMainImage() != null) {
			if (nendoroid.getThumbImage() != null) {
				whereClause += ", "; 
			}
			
			whereClause += nendoroid.getMainImage().getId(); 
		}
		whereClause += ")";
		nendoroid.setImageList(dbManager.getAllImagesWithConstraint(whereClause, nendoroid));

		return nendoroid;
	}
	
	public int getId() {		
		return _id;
	}
	
	public String getNumber() {		
		return _number;
	}
	
	public String getName() {		
		return _name;
	}
	
	public int getStatus() {		
		return _status;
	}
	
	public String getLookingForPriority() {		
		return _lookingForPriority;
	}
	
	public String getAcquisitionOrder() {		
		return _acquisitionOrder;
	}
	
	public Image getMainImage() {		
		return _mainImage;
	}
	
	public Image getThumbImage() {		
		return _thumbImage;
	}
	
	public String getGscLink() {		
		return _gscLink;
	}
	
	public double getGscPrice() {		
		return _gscPrice;
	}
	
	public String getReleaseDate() {		
		return _releaseDate;
	}
	
	public String getNickname() {		
		return _nickname;
	}
	
	public String getJapaneseName() {		
		return _japaneseName;
	}
	
	public String getOfficialName() {		
		return _officialName;
	}
	
	public String getSeries() {		
		return _series;
	}
	
	public String getManufacturer() {		
		return _manufacturer;
	}
	
	public String getCategory() {		
		return _category;
	}
	
	public String getSpecifications() {		
		return _specifications;
	}
	
	public String getSculptor() {		
		return _sculptor;
	}
	
	public String getCopyright() {		
		return _copyright;
	}
	
	public String getDescription() {		
		return _description;
	}
	
	public String getNotes() {		
		return _notes;
	}
	
	public String getOthers() {		
		return _others;
	}

	public List<Image> getImageList() {
		return _imageList;
	}

	public Context getContext() {
		return _context;
	}
	
	public void setId(int id) {		
		this._id = id;
	}
	
	public void setNumber(String number) {		
		this._number = number;
	}
	
	public void setName(String name) {		
		this._name = name;
	}
	
	public void setStatus(int status) {		
		this._status = status;
	}
	
	public void setLookingForPriority(String lookingForPriority) {		
		this._lookingForPriority = lookingForPriority;
	}
	
	public void setAcquisitionOrder(String acquisitionOrder) {		
		this._acquisitionOrder = acquisitionOrder;
	}
	
	public void setMainImage(Image mainImage) {		
		this._mainImage = mainImage;
	}
	
	public void setThumbImage(Image thumbImage) {		
		this._thumbImage = thumbImage;
	}
	
	public void setGscLink(String gscLink) {		
		this._gscLink = gscLink;
	}
	
	public void setGscPrice(double gscPrice) {		
		this._gscPrice = gscPrice;
	}
	
	public void setReleaseDate(String releaseDate) {		
		this._releaseDate = releaseDate;
	}
	
	public void setNickname(String nickname) {		
		this._nickname = nickname;
	}
	
	public void setJapaneseName(String japaneseName) {		
		this._japaneseName = japaneseName;
	}
	
	public void setOfficialName(String officialName) {		
		this._officialName = officialName;
	}
	
	public void setSeries(String series) {		
		this._series = series;
	}
	
	public void setManufacturer(String manufacturer) {		
		this._manufacturer = manufacturer;
	}
	
	public void setCategory(String category) {		
		this._category = category;
	}
	
	public void setSpecifications(String specifications) {		
		this._specifications = specifications;
	}
	
	public void setSculptor(String sculptor) {		
		this._sculptor = sculptor;
	}
	
	public void setCopyright(String copyright) {		
		this._copyright = copyright;
	}
	
	public void setDescription(String description) {		
		this._description = description;
	}
	
	public void setNotes(String notes) {		
		this._notes = notes;
	}
	
	public void setOthers(String others) {		
		this._others = others;
	}

	public void setImageList(List<Image> _imageList) {
		this._imageList = _imageList;
	}

	public void setContext(Context context) {
		this._context = context;
	}
	
	public void refreshFromDatabase() {
		DatabaseManager dbManager = DatabaseManager.getInstance(_context);
		Nendoroid refreshedNendoroid = dbManager.getNendoroid(_id);
		
		_id = refreshedNendoroid.getId();
		_number = refreshedNendoroid.getNumber();
		_name = refreshedNendoroid.getName();
		_status = refreshedNendoroid.getStatus();
		_lookingForPriority = refreshedNendoroid.getLookingForPriority();
		_acquisitionOrder = refreshedNendoroid.getAcquisitionOrder();
		_mainImage = refreshedNendoroid.getMainImage();
		_thumbImage = refreshedNendoroid.getThumbImage();
		_gscLink = refreshedNendoroid.getGscLink();
		_gscPrice = refreshedNendoroid.getGscPrice();
		_releaseDate = refreshedNendoroid.getReleaseDate();
		_nickname = refreshedNendoroid.getNickname();
		_japaneseName = refreshedNendoroid.getJapaneseName();
		_officialName = refreshedNendoroid.getOfficialName();
		_series = refreshedNendoroid.getSeries();
		_manufacturer = refreshedNendoroid.getManufacturer();
		_category = refreshedNendoroid.getCategory();
		_specifications = refreshedNendoroid.getSpecifications();
		_sculptor = refreshedNendoroid.getSculptor();
		_copyright = refreshedNendoroid.getCopyright();
		_description = refreshedNendoroid.getDescription();
		_notes = refreshedNendoroid.getNotes();
		_others = refreshedNendoroid.getOthers();
	
		_imageList = refreshedNendoroid.getImageList();
	
	}
	
	public String getDescriptiveName() {
		return "#" + _number + " " + _name; //TODO maybe check for nickname
	}
	
	public String getDescriptiveStatus(int mainStatusReference) {
		String statusDetails = "";
		if ((_status == STATUS_LOOKING_FOR
				|| _status == STATUS_ACQUIRED_LOOKING_FOR)
				&& mainStatusReference == STATUS_LOOKING_FOR) {
			statusDetails = _context.getString(R.string.status_detail_looking_for, 
					StringTransformations.convertNumberToOrdinal(_lookingForPriority));
		} else if ((_status == STATUS_ACQUIRED_LOOKING_FOR
				|| _status == STATUS_ACQUIRED)
				&& mainStatusReference == STATUS_ACQUIRED) {
			statusDetails = _context.getString(R.string.status_detail_acquired, 
					StringTransformations.convertNumberToOrdinal(_acquisitionOrder));
		} else {
			switch(_status) {
			case STATUS_NOT_INTERESTED:
				statusDetails = _context.getString(R.string.nendoroid_status_not_interested);
				break;
			case STATUS_INTERESTED:
				statusDetails = _context.getString(R.string.nendoroid_status_interested);
				break;
			case STATUS_LOOKING_FOR:
				statusDetails = _context.getString(R.string.status_detail_looking_for, 
						StringTransformations.convertNumberToOrdinal(_lookingForPriority));
				break;
			case STATUS_FOUND:
				statusDetails = _context.getString(R.string.nendoroid_status_found);
				break;
			case STATUS_PRE_ORDERED:
				statusDetails = _context.getString(R.string.nendoroid_status_pre_ordered);
				break;
			case STATUS_ORDERED_ARRIVED:
				statusDetails = _context.getString(R.string.nendoroid_status_ordered_arrived);
				break;
			case STATUS_ACQUIRED:
			case STATUS_ACQUIRED_LOOKING_FOR:
				statusDetails = _context.getString(R.string.status_detail_acquired, 
						StringTransformations.convertNumberToOrdinal(_acquisitionOrder));
				break;
			default:
				statusDetails = _context.getString(R.string.nendoroid_status_invalid);
			}
		}
		
		return statusDetails;
	}

	@Override
	public int compareTo(Nendoroid other) {
		return this.getNumber().compareToIgnoreCase(other.getNumber());
	}

	@Override
	public int hashCode() {
		return 31 + _number.hashCode();
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (!(object instanceof Nendoroid)) {
			return false;
		}
		
		Nendoroid otherNendoroid = (Nendoroid) object;
		return otherNendoroid.getNumber().equals(_number);
	}

	@SuppressWarnings("rawtypes")
	public static boolean isNendoroidList(List itemList) {
		if (itemList == null || itemList.size() == 0) {//FIXME? really? not a nendo list if size == 0?
			return false;
		}
		
		Object testObject = itemList.get(0);
		return testObject instanceof Nendoroid; 
	}
	
	public static String getStatusName(Context context, int status) {
		
		switch(status) {
		case STATUS_NOT_INTERESTED:
			return context.getString(R.string.nendoroid_status_not_interested);
		case STATUS_INTERESTED:
			return context.getString(R.string.nendoroid_status_interested);
		case STATUS_LOOKING_FOR:
			return context.getString(R.string.nendoroid_status_looking_for);
		case STATUS_FOUND:
			return context.getString(R.string.nendoroid_status_found);
		case STATUS_PRE_ORDERED:
			return context.getString(R.string.nendoroid_status_pre_ordered);
		case STATUS_ORDERED_ARRIVED:
			return context.getString(R.string.nendoroid_status_ordered_arrived);
		case STATUS_ACQUIRED:
			return context.getString(R.string.nendoroid_status_acquired);
		case STATUS_ACQUIRED_LOOKING_FOR:
			return context.getString(R.string.nendoroid_status_acquired_looking_for);
		default:
			return context.getString(R.string.nendoroid_status_invalid);
		}
	}
	
	public static String[][] getStatusNameTable(Context context) {
		return new String[][] {
			{
				String.valueOf(Nendoroid.STATUS_NOT_INTERESTED),
				String.valueOf(Nendoroid.STATUS_INTERESTED),
				String.valueOf(Nendoroid.STATUS_LOOKING_FOR),
				String.valueOf(Nendoroid.STATUS_FOUND),
				String.valueOf(Nendoroid.STATUS_PRE_ORDERED),
				String.valueOf(Nendoroid.STATUS_ORDERED_ARRIVED),
				String.valueOf(Nendoroid.STATUS_ACQUIRED),
				String.valueOf(Nendoroid.STATUS_ACQUIRED_LOOKING_FOR),
			},
			{
				Nendoroid.getStatusName(context, Nendoroid.STATUS_NOT_INTERESTED),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_INTERESTED),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_LOOKING_FOR),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_FOUND),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_PRE_ORDERED),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_ORDERED_ARRIVED),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_ACQUIRED),
				Nendoroid.getStatusName(context, Nendoroid.STATUS_ACQUIRED_LOOKING_FOR),
			},
		};
	}

}
