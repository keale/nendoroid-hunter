package com.jankiel.nendoroidhunter.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class CSVUtils {

	public static List<Nendoroid> readNendoroidCSV(InputStream inputStream, Context context) throws IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(inputStream, "UTF-8"));
		
		try {
			return readNendoroidArray(reader, context);
		} finally {
			reader.close();
		}
	}

	public static List<Nendoroid> readNendoroidArray(CSVReader reader, Context context) throws IOException {
		List<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();
		
		String[] line;
		while ((line = reader.readNext()) != null) {
			nendoroidList.add(readNendoroid(reader, context, line));
		}
		
		return nendoroidList;
	}
	
	public static Nendoroid readNendoroid(CSVReader reader, Context context, String[] line) throws IOException {
		Nendoroid nendoroid = new Nendoroid(context);
		
		return nendoroid;
	}

	public static List<Image> readImageArray(CSVReader reader, Context context, Nendoroid nendoroid) throws IOException {
		List<Image> imageList = new ArrayList<Image>();

		String[] line;
		while ((line = reader.readNext()) != null) {
			imageList.add(readImage(reader, context, line, nendoroid));
		}
		
		return imageList;
	}
	
	public static Image readImage(CSVReader reader, Context context, String[] line, Nendoroid nendoroid) throws IOException {
		Image image = new Image(context);
		
		return image;
	}

	public static void writeNendoroidCSV(OutputStream outputStream, List<Nendoroid> nendoroidList) 
			throws UnsupportedEncodingException, IOException {
		CSVWriter writer = new CSVWriter(new OutputStreamWriter(outputStream, "UTF-8"));
		
		writeNendoroidArray(writer, nendoroidList);
		writer.close();
	}
	
	public static void writeNendoroidArray(CSVWriter writer, List<Nendoroid> nendoroidList) throws IOException {

		for (Nendoroid nendoroid : nendoroidList) {
			writeNendoroid(writer, nendoroid);
		}
	}
	
	public static void writeNendoroid(CSVWriter writer, Nendoroid nendoroid) throws IOException {
		
		
	}
	
	public static void writeImageArray(CSVWriter writer, List<Image> imageList) throws IOException {

		for (Image image : imageList) {
			writeImage(writer, image);
		}
	}
	
	public static void writeImage(CSVWriter writer, Image image) throws IOException {

	}
}
