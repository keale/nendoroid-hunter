package com.jankiel.nendoroidhunter.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.jankiel.nendoroidhunter.models.Nendoroid;


public class Utils {

	public static String hashString(String string) throws NoSuchAlgorithmException, 
			UnsupportedEncodingException {
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
		
		messageDigest.update(string.getBytes("UTF-8"));
		byte[] digest = messageDigest.digest();
		
		return String.format("%064x", new BigInteger(1, digest)).toUpperCase();
	}
	
	public static boolean isValidURL(String string) {
		try {
			URL url = new URL(string);
			url.toURI();
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isValidFacebookURL(String string) {
		try {
			URL url = new URL(string);
			url.toURI();
			
			return url.getHost().contains("facebook");
		} catch (Exception e) {
			return false;
		}
	}
	
	public static int nendoroidIndexInList(Nendoroid nendoroid, List<Nendoroid> nendoroidList) {
		for (int i = 0; i < nendoroidList.size(); i++) {
			Nendoroid currentNendoroid = nendoroidList.get(i);
			if (nendoroid.getNumber().equalsIgnoreCase(currentNendoroid.getNumber())) {
				return i;
			}
		}
		return -1;
	}
}
