package com.jankiel.nendoroidhunter.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;

import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.models.Shop;

public class JsonUtils {

	public static List<Nendoroid> readNendoroidJson(InputStream inputStream, Context context) 
			throws UnsupportedEncodingException, IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		
		try {
			return readNendoroidArray(reader, context);
		} finally {
			reader.close();
		}
	}

	public static List<Nendoroid> readNendoroidArray(JsonReader reader, Context context) throws IOException {
		List<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();
		
		reader.beginArray();
		while (reader.hasNext()) {
			nendoroidList.add(readNendoroid(reader, context));
		}
		reader.endArray();
		
		return nendoroidList;
	}
	
	public static Nendoroid readNendoroid(JsonReader reader, Context context) throws IOException {
		Nendoroid nendoroid = new Nendoroid(context);
		
		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			
			if (name.equals(Nendoroid.FIELD_ID)) {
				nendoroid.setId(reader.nextInt());
			} else if (name.equals(Nendoroid.FIELD_NUMBER)) {
				nendoroid.setNumber(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_NAME)) {
				nendoroid.setName(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_STATUS)) {
				nendoroid.setStatus(reader.nextInt());
			} else if (name.equals(Nendoroid.FIELD_LOOKING_FOR_PRIORITY) && reader.peek() != JsonToken.NULL) {
				nendoroid.setLookingForPriority(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_ACQUISITION_ORDER) && reader.peek() != JsonToken.NULL) {
				nendoroid.setAcquisitionOrder(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_MAIN_IMAGE) && reader.peek() != JsonToken.NULL) {
				nendoroid.setMainImage(readImage(reader, context, nendoroid));
			} else if (name.equals(Nendoroid.FIELD_THUMB_IMAGE) && reader.peek() != JsonToken.NULL) {
				nendoroid.setThumbImage(readImage(reader, context, nendoroid));
			} else if (name.equals(Nendoroid.FIELD_GSC_LINK) && reader.peek() != JsonToken.NULL) {
				nendoroid.setGscLink(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_GSC_PRICE) && reader.peek() != JsonToken.NULL) {
				nendoroid.setGscPrice(reader.nextDouble());
			} else if (name.equals(Nendoroid.FIELD_RELEASE_DATE) && reader.peek() != JsonToken.NULL) {
				nendoroid.setReleaseDate(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_NICKNAME) && reader.peek() != JsonToken.NULL) {
				nendoroid.setNickname(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_JAPANESE_NAME) && reader.peek() != JsonToken.NULL) {
				nendoroid.setJapaneseName(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_OFFICIAL_NAME) && reader.peek() != JsonToken.NULL) {
				nendoroid.setOfficialName(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_SERIES) && reader.peek() != JsonToken.NULL) {
				nendoroid.setSeries(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_MANUFACTURER) && reader.peek() != JsonToken.NULL) {
				nendoroid.setManufacturer(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_CATEGORY) && reader.peek() != JsonToken.NULL) {
				nendoroid.setCategory(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_SPECIFICATIONS) && reader.peek() != JsonToken.NULL) {
				nendoroid.setSpecifications(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_SCULPTOR) && reader.peek() != JsonToken.NULL) {
				nendoroid.setSculptor(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_COPYRIGHT) && reader.peek() != JsonToken.NULL) {
				nendoroid.setCopyright(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_DESCRIPTION) && reader.peek() != JsonToken.NULL) {
				nendoroid.setDescription(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_NOTES) && reader.peek() != JsonToken.NULL) {
				nendoroid.setNotes(reader.nextString());
			} else if (name.equals(Nendoroid.FIELD_OTHERS) && reader.peek() != JsonToken.NULL) {
				nendoroid.setOthers(reader.nextString());
			
			} else if (name.equals(Nendoroid.FIELD_IMAGE_LIST) && reader.peek() != JsonToken.NULL) {
				nendoroid.setImageList(readImageArray(reader, context, nendoroid));
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		
		return nendoroid;
	}

	public static List<Image> readImageArray(JsonReader reader, Context context, Nendoroid nendoroid) throws IOException {
		List<Image> imageList = new ArrayList<Image>();
		
		reader.beginArray();
		while (reader.hasNext()) {
			imageList.add(readImage(reader, context, nendoroid));
		}
		reader.endArray();
		
		return imageList;
	}
	
	//TODO read & write caption too
	public static Image readImage(JsonReader reader, Context context, Nendoroid nendoroid) throws IOException {
		Image image = new Image(context);
		
		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals(Image.FIELD_ID)) {
				image.setId(reader.nextInt());
			} else if (name.equals(Image.FIELD_NENDOROID)) {
				if (nendoroid.getId() == reader.nextInt()) {
					image.setNendoroid(nendoroid);
				}
			} else if (name.equals(Image.FIELD_FILEPATH) && reader.peek() != JsonToken.NULL) {
				image.setFilepath(reader.nextString());
			} else if (name.equals(Image.FIELD_WEBPATH) && reader.peek() != JsonToken.NULL) {
				image.setWebpath(reader.nextString());
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		
		return image;
	}

	public static List<Shop> readShopJson(InputStream inputStream, Context context) 
			throws UnsupportedEncodingException, IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		
		try {
			return readShopArray(reader, context);
		} finally {
			reader.close();
		}
	}

	public static List<Shop> readShopArray(JsonReader reader, Context context) throws IOException {
		List<Shop> shopList = new ArrayList<Shop>();
		
		reader.beginArray();
		while (reader.hasNext()) {
			shopList.add(readShop(reader, context));
		}
		reader.endArray();
		
		return shopList;
	}
	
	public static Shop readShop(JsonReader reader, Context context) throws IOException {
		Shop shop = new Shop(context);
		
		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();

			if (name.equals(Shop.FIELD_ID)) {
				shop.setId(reader.nextInt());
			} else if (name.equals(Shop.FIELD_NAME)) {
				shop.setName(reader.nextString());
			} else if (name.equals(Shop.FIELD_TYPE)) {
				shop.setType(reader.nextInt());
			} else if (name.equals(Shop.FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR)) {
				shop.setIsGscOfficialDistributor(reader.nextBoolean());
			} else if (name.equals(Shop.FIELD_WEBSITE) && reader.peek() != JsonToken.NULL) {
				shop.setWebsite(reader.nextString());
			} else if (name.equals(Shop.FIELD_FACEBOOK) && reader.peek() != JsonToken.NULL) {
				shop.setFacebook(reader.nextString());
			} else if (name.equals(Shop.FIELD_MODE_OF_PAYMENT) && reader.peek() != JsonToken.NULL) {
				shop.setModeOfPayment(reader.nextString());
			} else if (name.equals(Shop.FIELD_MAIN_ADDRESS) && reader.peek() != JsonToken.NULL) {
				shop.setMainAddress(reader.nextString());
			} else if (name.equals(Shop.FIELD_LATITUDE)) {
				shop.setLatitude(reader.nextDouble());
			} else if (name.equals(Shop.FIELD_LONGITUDE)) {
				shop.setLongitude(reader.nextDouble());
			} else if (name.equals(Shop.FIELD_LOCATIONS) && reader.peek() != JsonToken.NULL) {
				shop.setLocations(reader.nextString());
			} else if (name.equals(Shop.FIELD_CONTACT_NO) && reader.peek() != JsonToken.NULL) {
				shop.setContactNo(reader.nextString());
			} else if (name.equals(Shop.FIELD_NOTES) && reader.peek() != JsonToken.NULL) {
				shop.setNotes(reader.nextString());
			} else if (name.equals(Shop.FIELD_OTHERS) && reader.peek() != JsonToken.NULL) {
				shop.setOthers(reader.nextString());
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		
		return shop;
	}

	public static void writeNendoroidJson(OutputStream outputStream, List<Nendoroid> nendoroidList) 
			throws UnsupportedEncodingException, IOException {
		JsonWriter writer = new JsonWriter(new OutputStreamWriter(outputStream, "UTF-8"));
		
		writer.setIndent("  ");
		writeNendoroidArray(writer, nendoroidList);
		writer.close();
	}
	
	public static void writeNendoroidArray(JsonWriter writer, List<Nendoroid> nendoroidList) throws IOException {
		
		writer.beginArray();
		for (Nendoroid nendoroid : nendoroidList) {
			writeNendoroid(writer, nendoroid);
		}
		writer.endArray();
		
	}
	
	public static void writeNendoroid(JsonWriter writer, Nendoroid nendoroid) throws IOException {
		writer.beginObject();

		writer.name(Nendoroid.FIELD_ID).value(nendoroid.getId());
		writer.name(Nendoroid.FIELD_NUMBER).value(nendoroid.getNumber());
		writer.name(Nendoroid.FIELD_NAME).value(nendoroid.getName());
		writer.name(Nendoroid.FIELD_STATUS).value(nendoroid.getStatus());
		if (nendoroid.getLookingForPriority() != null) {
			writer.name(Nendoroid.FIELD_LOOKING_FOR_PRIORITY).value(nendoroid.getLookingForPriority());
		} else {
			writer.name(Nendoroid.FIELD_LOOKING_FOR_PRIORITY).nullValue();
		}
		if (nendoroid.getAcquisitionOrder() != null) {
			writer.name(Nendoroid.FIELD_ACQUISITION_ORDER).value(nendoroid.getAcquisitionOrder());
		} else {
			writer.name(Nendoroid.FIELD_ACQUISITION_ORDER).nullValue();
		}
		if (nendoroid.getMainImage() != null) {
			writer.name(Nendoroid.FIELD_MAIN_IMAGE);
			writeImage(writer, nendoroid.getMainImage());
		} else {
			writer.name(Nendoroid.FIELD_MAIN_IMAGE).nullValue();
		}
		if (nendoroid.getThumbImage() != null) {
			writer.name(Nendoroid.FIELD_THUMB_IMAGE);
			writeImage(writer, nendoroid.getThumbImage());
		} else {
			writer.name(Nendoroid.FIELD_THUMB_IMAGE).nullValue();
		}
		if (nendoroid.getGscLink() != null) {
			writer.name(Nendoroid.FIELD_GSC_LINK).value(nendoroid.getGscLink());
		} else {
			writer.name(Nendoroid.FIELD_GSC_LINK).nullValue();
		}
		writer.name(Nendoroid.FIELD_GSC_PRICE).value(nendoroid.getGscPrice());
		if (nendoroid.getReleaseDate() != null) {
			writer.name(Nendoroid.FIELD_RELEASE_DATE).value(nendoroid.getReleaseDate());
		} else {
			writer.name(Nendoroid.FIELD_RELEASE_DATE).nullValue();
		}
		if (nendoroid.getNickname() != null) {
			writer.name(Nendoroid.FIELD_NICKNAME).value(nendoroid.getNickname());
		} else {
			writer.name(Nendoroid.FIELD_NICKNAME).nullValue();
		}
		if (nendoroid.getJapaneseName() != null) {
			writer.name(Nendoroid.FIELD_JAPANESE_NAME).value(nendoroid.getJapaneseName());
		} else {
			writer.name(Nendoroid.FIELD_JAPANESE_NAME).nullValue();
		}
		if (nendoroid.getOfficialName() != null) {
			writer.name(Nendoroid.FIELD_OFFICIAL_NAME).value(nendoroid.getOfficialName());
		} else {
			writer.name(Nendoroid.FIELD_OFFICIAL_NAME).nullValue();
		}
		if (nendoroid.getSeries() != null) {
			writer.name(Nendoroid.FIELD_SERIES).value(nendoroid.getSeries());
		} else {
			writer.name(Nendoroid.FIELD_SERIES).nullValue();
		}
		if (nendoroid.getManufacturer() != null) {
			writer.name(Nendoroid.FIELD_MANUFACTURER).value(nendoroid.getManufacturer());
		} else {
			writer.name(Nendoroid.FIELD_MANUFACTURER).nullValue();
		}
		if (nendoroid.getCategory() != null) {
			writer.name(Nendoroid.FIELD_CATEGORY).value(nendoroid.getCategory());
		} else {
			writer.name(Nendoroid.FIELD_CATEGORY).nullValue();
		}
		if (nendoroid.getSpecifications() != null) {
			writer.name(Nendoroid.FIELD_SPECIFICATIONS).value(nendoroid.getSpecifications());
		} else {
			writer.name(Nendoroid.FIELD_SPECIFICATIONS).nullValue();
		}
		if (nendoroid.getSculptor() != null) {
			writer.name(Nendoroid.FIELD_SCULPTOR).value(nendoroid.getSculptor());
		} else {
			writer.name(Nendoroid.FIELD_SCULPTOR).nullValue();
		}
		if (nendoroid.getCopyright() != null) {
			writer.name(Nendoroid.FIELD_COPYRIGHT).value(nendoroid.getCopyright());
		} else {
			writer.name(Nendoroid.FIELD_COPYRIGHT).nullValue();
		}
		if (nendoroid.getDescription() != null) {
			writer.name(Nendoroid.FIELD_DESCRIPTION).value(nendoroid.getDescription());
		} else {
			writer.name(Nendoroid.FIELD_DESCRIPTION).nullValue();
		}
		if (nendoroid.getNotes() != null) {
			writer.name(Nendoroid.FIELD_NOTES).value(nendoroid.getNotes());
		} else {
			writer.name(Nendoroid.FIELD_NOTES).nullValue();
		}
		if (nendoroid.getOthers() != null) {
			writer.name(Nendoroid.FIELD_OTHERS).value(nendoroid.getOthers());
		} else {
			writer.name(Nendoroid.FIELD_OTHERS).nullValue();
		}
		
		if (nendoroid.getImageList() != null) {
			writer.name(Nendoroid.FIELD_IMAGE_LIST);
			writeImageArray(writer, nendoroid.getImageList());
		} else {
			writer.name(Nendoroid.FIELD_IMAGE_LIST).nullValue();
		}
		
		writer.endObject();
	}
	
	public static void writeImageArray(JsonWriter writer, List<Image> imageList) throws IOException {
		
		writer.beginArray();
		for (Image image : imageList) {
			writeImage(writer, image);
		}
		writer.endArray();
		
	}
	
	public static void writeImage(JsonWriter writer, Image image) throws IOException {
		writer.beginObject();
		
		//TODO remove export of filepath
		writer.name(Image.FIELD_ID).value(image.getId());
		writer.name(Image.FIELD_NENDOROID).value(image.getNendoroid().getId());
		writer.name(Image.FIELD_FILEPATH).value(image.getFilepath());
		//writer.name(Image.FIELD_FILEPATH).nullValue();
		if (image.getWebpath() != null) {
			writer.name(Image.FIELD_WEBPATH).value(image.getWebpath());
		} else {
			writer.name(Image.FIELD_WEBPATH).nullValue();
		}
		writer.endObject();
	}

	public static void writeShopJson(OutputStream outputStream, List<Shop> shopList) 
			throws UnsupportedEncodingException, IOException {
		JsonWriter writer = new JsonWriter(new OutputStreamWriter(outputStream, "UTF-8"));
		
		writer.setIndent("  ");
		writeShopArray(writer, shopList);
		writer.close();
	}
	
	public static void writeShopArray(JsonWriter writer, List<Shop> shopList) throws IOException {
		
		writer.beginArray();
		for (Shop shop : shopList) {
			writeShop(writer, shop);
		}
		writer.endArray();
		
	}
	
	public static void writeShop(JsonWriter writer, Shop shop) throws IOException {
		writer.beginObject();
		
		writer.name(Shop.FIELD_ID).value(shop.getId());
		writer.name(Shop.FIELD_NAME).value(shop.getName());
		writer.name(Shop.FIELD_TYPE).value(shop.getType());
		writer.name(Shop.FIELD_IS_GSC_OFFICIAL_DISTRIBUTOR).value(shop.getIsGscOfficialDistributor());
		if (shop.getWebsite() != null) {
			writer.name(Shop.FIELD_WEBSITE).value(shop.getWebsite());
		} else {
			writer.name(Shop.FIELD_WEBSITE).nullValue();
		}
		if (shop.getFacebook() != null) {
			writer.name(Shop.FIELD_FACEBOOK).value(shop.getFacebook());
		} else {
			writer.name(Shop.FIELD_FACEBOOK).nullValue();
		}
		if (shop.getModeOfPayment() != null) {
			writer.name(Shop.FIELD_MODE_OF_PAYMENT).value(shop.getModeOfPayment());
		} else {
			writer.name(Shop.FIELD_MODE_OF_PAYMENT).nullValue();
		}
		if (shop.getMainAddress() != null) {
			writer.name(Shop.FIELD_MAIN_ADDRESS).value(shop.getMainAddress());
		} else {
			writer.name(Shop.FIELD_MAIN_ADDRESS).nullValue();
		}
		writer.name(Shop.FIELD_LATITUDE).value(shop.getLatitude());
		writer.name(Shop.FIELD_LONGITUDE).value(shop.getLongitude());
		if (shop.getLocations() != null) {
			writer.name(Shop.FIELD_LOCATIONS).value(shop.getLocations());
		} else {
			writer.name(Shop.FIELD_LOCATIONS).nullValue();
		}
		if (shop.getContactNo() != null) {
			writer.name(Shop.FIELD_CONTACT_NO).value(shop.getContactNo());
		} else {
			writer.name(Shop.FIELD_CONTACT_NO).nullValue();
		}
		if (shop.getNotes() != null) {
			writer.name(Shop.FIELD_NOTES).value(shop.getNotes());
		} else {
			writer.name(Shop.FIELD_NOTES).nullValue();
		}
		if (shop.getOthers() != null) {
			writer.name(Shop.FIELD_OTHERS).value(shop.getOthers());
		} else {
			writer.name(Shop.FIELD_OTHERS).nullValue();
		}
		
		writer.endObject();
	}
}
