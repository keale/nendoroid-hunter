package com.jankiel.nendoroidhunter.utils;

public interface FeedbackListener {
	public void onFeedback(String... messages);
}
