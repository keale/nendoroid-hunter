package com.jankiel.nendoroidhunter.utils;

public class AppConstant {

	public static final String SHARED_PREFERENCES_KEY_IS_FIRST_RUN = "is_first_run";
	public static final String SHARED_PREFERENCES_KEY_HAS_SEEN_NENDOROID_DETAIL_TIP = "has_seen_nendoroid_detail_tip";
	public static final String SHARED_PREFERENCES_KEY_HAS_LOADED_INITIAL_DATABASE = "has_loaded_initial_database";
	public static final String SHARED_PREFERENCES_KEY_IS_JANKIEL = "is_jankiel";
	public static final String SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD = "default_search_by_field";
	public static final String SHARED_PREFERENCES_KEY_USER_CURRENCY = "user_currency";
	
	public static final String SHARED_PREFERENCES_KEY_IMPORT_NENDOROID_LIST = "import_nendoroid_list";
	public static final String SHARED_PREFERENCES_KEY_EXPORT_NENDOROID_LIST = "export_nendoroid_list";
	public static final String SHARED_PREFERENCES_KEY_UPDATE_NENDOROID_LIST = "update_nendoroid_list";
	public static final String SHARED_PREFERENCES_KEY_OPEN_SHOP_LIST_ACTIVITY = "open_shop_list_activity";
	public static final String SHARED_PREFERENCES_KEY_DOWNLOAD_IMAGES = "download_images";
	public static final String SHARED_PREFERENCES_KEY_IMPORT_SHOP_LIST = "import_shop_list";
	public static final String SHARED_PREFERENCES_KEY_EXPORT_SHOP_LIST = "export_shop_list";
	public static final String SHARED_PREFERENCES_KEY_OPEN_HELP_ACTIVITY = "open_help_activity";
	public static final String SHARED_PREFERENCES_KEY_OPEN_ABOUT_ACTIVITY = "open_about_activity";
	public static final String SHARED_PREFERENCES_KEY_OPEN_LEGAL_STUFFS_ACTIVITY = "open_legal_stuffs_activity";
	
	public static final String PREFERENCE_CATEGORY_KEY_NENDOROID_LIST = "pref_category_nendoroid_list";
	public static final String PREFERENCE_CATEGORY_KEY_SHOP_LIST = "pref_category_shop_list";
	public static final String PREFERENCE_CATEGORY_KEY_MISCELLANEOUS = "pref_category_miscellaneous";
	
	public static final String FILE_FORMAT_XML = "xml";
	public static final String FILE_FORMAT_JSON = "json";
	public static final String FILE_FORMAT_CSV = "csv";
	public static final String FILE_FORMAT_KML = "kml";

	public static final int EXPECTATION_NENDOROID = 0;
	public static final int EXPECTATION_SHOP = 1;
	
	public static final String ADVANCED_USER_PASSWORD = "8266EB999A793225E96CF0CCAA9884E3F10F947BDD0F3B4A21C38DA2845E31E6";
	
	public static final int ONE_SECOND = 1000;
	public static final int SIXTY_SECONDS = 60 * ONE_SECOND;
	
}
