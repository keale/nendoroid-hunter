package com.jankiel.nendoroidhunter.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.content.Context;

import com.jankiel.genlibs.utils.StringTransformations;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;

public class NendoroidDataScraper {
	
	private Context mContext;
	private List<Nendoroid> mExistingNendoroidList;

	private FeedbackListener mFeedbackListener;
	
	public NendoroidDataScraper(Context context, FeedbackListener feedbackListener) {
		this(context, feedbackListener, null);
	}
	
	public NendoroidDataScraper(Context context, FeedbackListener feedbackListener,
			List<Nendoroid> existingNendoroidList) {
		this.mContext = context;
		this.mExistingNendoroidList = existingNendoroidList;
		this.mFeedbackListener = feedbackListener;
	}
	
	public List<Nendoroid> scrapeNendoroidData() throws IOException {
		return scrapeNendoroidBasicData(true);
	}
	
	public List<Nendoroid> scrapeNendoroidBasicData(boolean shouldScrapeOtherData) throws IOException {
		String baseUrl = "http://www.goodsmile.info/en/";
		List<String> validUrlList = new ArrayList<String>();
		
		int statusCode = 200;
		int minNendoroidNumber = 0;
		
		while (statusCode == 200) {
			int increment = minNendoroidNumber == 0 ? 100 : 99;
			
			String url = baseUrl + "nendoroid" 
					+ StringTransformations.padInFront(minNendoroidNumber, 3) + "-"
					+ StringTransformations.padInFront(minNendoroidNumber +  increment, 3);
			
			mFeedbackListener.onFeedback(mContext.getString(R.string.progress_message_connecting_to_url, url));
			Connection.Response response = Jsoup.connect(url).timeout(AppConstant.SIXTY_SECONDS)
					.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
						+ "AppleWebKit/537.36 (KHTML, like Gecko) "
						+ "Chrome/33.0.1750.152 Safari/537.36")
					.ignoreHttpErrors(true).execute();
			
			statusCode = response.statusCode();
			
			increment += 1;
			if (statusCode == 200) {
				validUrlList.add(url);
				minNendoroidNumber += increment;
			}
		}
		
		List<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();
		
		for (String validUrl : validUrlList) {
			//String currentNendoroidRange = validUrl.substring((baseUrl + "nendoroid").length());
			//mFeedbackListener.onFeedback(mContext.getString(R.string.progress_message_processing_nendoroid_range, 
			//		currentNendoroidRange));
			mFeedbackListener.onFeedback(mContext.getString(R.string.progress_message_building_master_list));
			
			Document doc = Jsoup.connect(validUrl).timeout(AppConstant.SIXTY_SECONDS)
						.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
						+ "AppleWebKit/537.36 (KHTML, like Gecko) "
						+ "Chrome/33.0.1750.152 Safari/537.36").get();
		
			Elements nendoroidDivs = doc.select(".hitBox");

			for (Element nendoroidDiv : nendoroidDivs) {
				Nendoroid nendoroid = new Nendoroid(mContext);
				
				Element gscLinkAnchor = nendoroidDiv.select("a").first();
				nendoroid.setGscLink(gscLinkAnchor.attr("href"));
				
				Element thumbImg = gscLinkAnchor.select("img").first();
				nendoroid.setThumbImage(new Image(mContext, nendoroid, thumbImg.attr("data-original")));
				
				Element nameSpan = gscLinkAnchor.select("span.hitTtl > span").first();
				nendoroid.setName(StringTransformations.unescapeHtml3(nameSpan.html()));
				
				Element numberSpan = gscLinkAnchor.select("span.hitNum.nendoroid").first();
				nendoroid.setNumber(numberSpan.html().replaceAll("[\\-]", "")); //remove all dashes
				
				if (mExistingNendoroidList != null && Utils.nendoroidIndexInList(nendoroid, mExistingNendoroidList) != -1) {
					continue; //skip this nendoroid if we are doing an update of existing list
				}
				
				nendoroidList.add(nendoroid);
			}
		}
		
		if (shouldScrapeOtherData) {
			nendoroidList = scrapeNendoroidOtherData(nendoroidList);
		}

		//dont insert missing nendos on update
		if (mExistingNendoroidList == null) {
			InputStream nendoroidInputStream = mContext.getAssets().open("MissingNendoroidList.json");
			List<Nendoroid> missingNendoroidList = JsonUtils.readNendoroidJson(nendoroidInputStream, mContext);
			
			nendoroidList.addAll(missingNendoroidList);
		}
		
		Collections.sort(nendoroidList);
		
		return nendoroidList;
	}
	
	public List<Nendoroid> scrapeNendoroidOtherData(List<Nendoroid> nendoroidList) throws IOException {
		List<Nendoroid> nendoroidWithOtherDataList = new ArrayList<Nendoroid>();
		
		for (Nendoroid nendoroid : nendoroidList) {
			mFeedbackListener.onFeedback(mContext.getString(R.string.progress_message_processing_nendoroid, 
					nendoroid.getDescriptiveName()));
			
			if (nendoroid.getGscLink() != null) {
				Document gscDoc = Jsoup.connect(nendoroid.getGscLink()).timeout(AppConstant.SIXTY_SECONDS)
						.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
						+ "AppleWebKit/537.36 (KHTML, like Gecko) "
						+ "Chrome/33.0.1750.152 Safari/537.36").get();
				
				Element itemBoxDiv = gscDoc.select("div#itemBox").first();
				
				Element descpriptionDiv = itemBoxDiv.select("div.description").first();
				nendoroid.setDescription(descpriptionDiv.html());
				
				Element itemDetailDiv = itemBoxDiv.select("div.itemDetail").first();
				Element detailBoxDiv = itemDetailDiv.select("div.detailBox").first();
				
				Elements dts = detailBoxDiv.select("dt");
				Elements dds = detailBoxDiv.select("dd");
				
				if (dts.size() == dds.size()) {
					StringBuilder others = new StringBuilder("");
					
					for (int i = 0; i < dts.size(); i++) {
						Element dt = dts.get(i);
						Element dd = dds.get(i);
						
						if (dt.html().equals("Product Name")) {
							nendoroid.setOfficialName(StringTransformations.unescapeHtml3(dd.html()));
						} else if (dt.html().equals("Series")) {
							nendoroid.setSeries(StringTransformations.unescapeHtml3(dd.html()));
						} else if (dt.html().equals("Manufacturer")) {
							nendoroid.setManufacturer(StringTransformations.unescapeHtml3(dd.html()));
						} else if (dt.html().equals("Category")) {
							nendoroid.setCategory(StringTransformations.unescapeHtml3(dd.html()));
						} else if (dt.html().equals("Price")) {
							double gscPrice = 0.0;
							try {
								gscPrice = Double.parseDouble(dd.attr("content"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							nendoroid.setGscPrice(gscPrice);
						} else if (dt.html().equals("Release Date")) {
							nendoroid.setReleaseDate(dd.html());
						} else if (dt.html().equals("Specifications")) {
							nendoroid.setSpecifications(StringTransformations.unescapeHtml3(dd.html()));
						} else if (dt.html().equals("Sculptor")) {
							nendoroid.setSculptor(StringTransformations.unescapeHtml3(dd.html()));
						} else {
							others.append("<b>" + dt.html() + "</b><br/>&nbsp;&nbsp;");
							others.append(dd.html() + "<br/>");
						}
					}
					
					nendoroid.setOthers(others.toString());
				}
				
				Element itemCopyDiv = detailBoxDiv.select("div.itemCopy").first();
				if (itemCopyDiv != null) {
					nendoroid.setCopyright(itemCopyDiv.html());
				}
				
				Element itemPhotosDiv = itemBoxDiv.select("div.itemPhotos").first();
				Elements itemImgs = itemPhotosDiv.select("img.itemImg");
				
				if (itemImgs != null && itemImgs.size() > 0) {
					Element mainImg = itemImgs.get(0);
					nendoroid.setMainImage(new Image(mContext, nendoroid, mainImg.attr("src")));
					
					List<Image> imageList = new ArrayList<Image>();
					for (int i = 1; i < itemImgs.size(); i++) {
						Element img = itemImgs.get(i);
						imageList.add(new Image(mContext, nendoroid, img.attr("src")));
					}
					nendoroid.setImageList(imageList);
				}
			}
			
			nendoroidWithOtherDataList.add(nendoroid);
		}
		
		return nendoroidWithOtherDataList;
	}
	
	public List<Nendoroid> getExistingNendoroidList() {
		return mExistingNendoroidList;
	}

	public void setExistingNendoroidList(List<Nendoroid> existingNendoroidList) {
		this.mExistingNendoroidList = existingNendoroidList;
	}
	
	public void OnFeedbackListener(FeedbackListener feedbackListener) {
		this.mFeedbackListener = feedbackListener;
	}
}
