package com.jankiel.nendoroidhunter.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.util.Xml;

import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.models.Shop;

public class XmlUtils {
	
	public static List<Shop> parseGoogleKml(InputStream inputStream, Context context) 
			throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(inputStream, "UTF-8");
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		
		try {
			parser.nextTag();
			return parseGoogleKmlShopArray(parser, context);
		} finally {
			inputStream.close();
		}
	}
	
	public static List<Shop> parseGoogleKmlShopArray(XmlPullParser parser, Context context) 
			throws XmlPullParserException, IOException {
		List<Shop> shopList = new ArrayList<Shop>();

		parser.require(XmlPullParser.START_TAG, null, "kml");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();

			if (name.equalsIgnoreCase("Document")) {
				shopList = parseGoogleKmlDocument(parser, context);
			} else {
				skip(parser);
			}
		}

		return shopList;
	}
	
	public static List<Shop> parseGoogleKmlDocument(XmlPullParser parser, Context context) 
			throws XmlPullParserException, IOException {
		List<Shop> shopList = new ArrayList<Shop>();

		parser.require(XmlPullParser.START_TAG, null, "Document");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();

			if (name.equalsIgnoreCase("Placemark")) {
				shopList.add(parseGoogleKmlPlacemark(parser, context));
			} else {
				skip(parser);
			}
		}

		return shopList;
	}

	public static Shop parseGoogleKmlPlacemark(XmlPullParser parser, Context context) 
			throws XmlPullParserException, IOException {
		Shop shop = new Shop(context);
		shop.setType(Shop.TYPE_PHYSICAL);

		parser.require(XmlPullParser.START_TAG, null, "Placemark");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			
			if (name.equalsIgnoreCase("name")) {
				shop.setName(parseString(parser));
			} else if (name.equalsIgnoreCase("Point")) {
				String point = parseGoogleKmlPoint(parser);
				String[] coordinates = point.split(",");
				
				if (coordinates.length == 2) {
					//for some reason, the first coordinate declared is the longitude,
					//followed by the latitude.
					shop.setLatitude(Double.parseDouble(coordinates[1]));
					shop.setLongitude(Double.parseDouble(coordinates[0]));
				}
			} else {
				skip(parser);
			}
		}
		
		return shop;
	}

	public static String parseGoogleKmlPoint(XmlPullParser parser) 
			throws XmlPullParserException, IOException {
		String point = null;

		parser.require(XmlPullParser.START_TAG, null, "Point");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			
			if (name.equalsIgnoreCase("coordinates")) {
				point = parseString(parser);
			} else {
				skip(parser);
			}
		}
		
		return point;
	}
	
	public static List<Shop> parseShopXml(InputStream inputStream, Context context) 
			throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(inputStream, "UTF-8");

		try {
			parser.nextTag();
			return parseShopArray(parser, context);
		} finally {
			inputStream.close();
		}
	}
	
	public static List<Shop> parseShopArray(XmlPullParser parser, Context context) 
			throws XmlPullParserException, IOException {
		List<Shop> shopList = new ArrayList<Shop>();
		
		
		return shopList;
	}

	public static List<Nendoroid> parseNendoroidXml(InputStream inputStream, Context context) 
			throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(inputStream, "UTF-8");

		try {
			parser.nextTag();
			return parseNendoroidArray(parser, context);
		} finally {
			inputStream.close();
		}
	}
	
	public static List<Nendoroid> parseNendoroidArray(XmlPullParser parser, Context context) 
			throws XmlPullParserException, IOException {
		List<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();

		parser.require(XmlPullParser.START_TAG, null, Nendoroid.TABLE_NAME + "Array");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			
			if (name.equalsIgnoreCase(Nendoroid.TABLE_NAME)) {
				nendoroidList.add(parseNendoroid(parser, context));
			} else  {
				skip(parser);
			}
		}
				
		return nendoroidList;
	}
	
	public static Nendoroid parseNendoroid(XmlPullParser parser, Context context) 
			throws XmlPullParserException, IOException {
		Nendoroid nendoroid = new Nendoroid(context);

		parser.require(XmlPullParser.START_TAG, null, Nendoroid.TABLE_NAME);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			
			if (name.equalsIgnoreCase(Nendoroid.FIELD_ID)) {
				nendoroid.setId(parseInt(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_NUMBER)) {
				nendoroid.setNumber(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_NAME)) {
				nendoroid.setName(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_STATUS)) {
				nendoroid.setStatus(parseInt(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_LOOKING_FOR_PRIORITY)) {
				nendoroid.setLookingForPriority(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_ACQUISITION_ORDER)) {
				nendoroid.setAcquisitionOrder(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_MAIN_IMAGE)) {
				parser.nextTag();
				nendoroid.setMainImage(parseImage(parser, context, nendoroid));
				parser.nextTag();
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_THUMB_IMAGE)) {
				parser.nextTag();
				nendoroid.setThumbImage(parseImage(parser, context, nendoroid));
				parser.nextTag();
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_GSC_LINK)) {
				nendoroid.setGscLink(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_GSC_PRICE)) {
				nendoroid.setGscPrice(parseDouble(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_RELEASE_DATE)) {
				nendoroid.setReleaseDate(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_NICKNAME)) {
				nendoroid.setNickname(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_JAPANESE_NAME)) {
				nendoroid.setJapaneseName(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_OFFICIAL_NAME)) {
				nendoroid.setOfficialName(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_SERIES)) {
				nendoroid.setSeries(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_MANUFACTURER)) {
				nendoroid.setManufacturer(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_CATEGORY)) {
				nendoroid.setCategory(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_SPECIFICATIONS)) {
				nendoroid.setSpecifications(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_SCULPTOR)) {
				nendoroid.setSculptor(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_COPYRIGHT)) {
				nendoroid.setCopyright(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_DESCRIPTION)) {
				nendoroid.setDescription(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_NOTES)) {
				nendoroid.setNotes(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_OTHERS)) {
				nendoroid.setOthers(parseString(parser));
			} else if (name.equalsIgnoreCase(Nendoroid.FIELD_IMAGE_LIST)) {
				parser.nextTag();
				nendoroid.setImageList(parseImageArray(parser, context, nendoroid));
				parser.nextTag();
			} else {
				skip(parser);
			}
		}
		
		return nendoroid;
	}
	
	public static List<Image> parseImageArray(XmlPullParser parser, Context context, Nendoroid nendoroid) 
			throws XmlPullParserException, IOException {
		List<Image> imageList = new ArrayList<Image>();

		parser.require(XmlPullParser.START_TAG, null, Image.TABLE_NAME + "Array");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			
			if (name.equalsIgnoreCase(Image.TABLE_NAME)) {
				imageList.add(parseImage(parser, context, nendoroid));
			} else  {
				skip(parser);
			}
		}
		
		return imageList;
	}
	
	//TODO parse & serialize caption too
	public static Image parseImage(XmlPullParser parser, Context context, Nendoroid nendoroid) 
			throws XmlPullParserException, IOException {
		Image image = new Image(context);

		parser.require(XmlPullParser.START_TAG, null, Image.TABLE_NAME);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			
			if (name.equalsIgnoreCase(Image.FIELD_ID)) {
				image.setId(parseInt(parser));
			} else if (name.equalsIgnoreCase(Image.FIELD_NENDOROID)) {
				if (nendoroid.getId() == parseInt(parser)) {
					image.setNendoroid(nendoroid);
				}
			} else if (name.equalsIgnoreCase(Image.FIELD_FILEPATH)) {
				image.setFilepath(parseString(parser));
			} else if (name.equalsIgnoreCase(Image.FIELD_WEBPATH)) {
				image.setWebpath(parseString(parser));
			} else {
				skip(parser);
			}
		}
		
		return image;
	}
	
	private static String parseString(XmlPullParser parser) 
			throws XmlPullParserException, IOException {
		String text = null;
		if (parser.next() == XmlPullParser.TEXT) {
			text = parser.getText();
			parser.nextTag();
		} else {
		}
		
		return text;
	}
	
	private static int parseInt(XmlPullParser parser) 
			throws XmlPullParserException, IOException {
		int number = 0;
		if (parser.next() == XmlPullParser.TEXT) {
			try {
				number = Integer.parseInt(parser.getText());
			} catch (Exception e) {
				e.printStackTrace();
			}
			parser.nextTag();
		}
		
		return number;
	}
	
	private static double parseDouble(XmlPullParser parser) 
			throws XmlPullParserException, IOException {
		double number = 0;
		if (parser.next() == XmlPullParser.TEXT) {
			try {
				number = Double.parseDouble(parser.getText());
			} catch (Exception e) {
				e.printStackTrace();
			}
			parser.nextTag();
		}
		
		return number;
	}
	
	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		
		int depth = 1;
		
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	 }
	
	//FIXME for some reason this throws a null exception
	public static void serializeNendoroidXml(OutputStream outputStream, List<Nendoroid> nendoroidList) 
			throws IllegalArgumentException, IllegalStateException, IOException {
		
		XmlSerializer serializer = Xml.newSerializer();
		serializer.setOutput(outputStream, "UTF-8");
		serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
		
		serializer.startDocument("UTF-8", Boolean.TRUE);
		serializeNendoroidArray(serializer, nendoroidList);
		serializer.endDocument();
		
		outputStream.close();
	}
	
	public static void serializeNendoroidArray(XmlSerializer serializer, List<Nendoroid> nendoroidList) 
			throws IllegalArgumentException, IllegalStateException, IOException {
		
		serializer.startTag("", Nendoroid.TABLE_NAME + "Array");
		for (Nendoroid nendoroid : nendoroidList) {
			serializeNendoroid(serializer, nendoroid);
		}
		serializer.endTag("", Nendoroid.TABLE_NAME + "Array");
	}
	
	public static void serializeNendoroid(XmlSerializer serializer, Nendoroid nendoroid) 
			throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.startTag("", Nendoroid.TABLE_NAME);
		
		serializeInt(serializer, Nendoroid.FIELD_ID, nendoroid.getId());
		serializeString(serializer, Nendoroid.FIELD_NUMBER, nendoroid.getNumber());
		serializeString(serializer, Nendoroid.FIELD_NAME, nendoroid.getName());
		serializeInt(serializer, Nendoroid.FIELD_STATUS, nendoroid.getStatus());
		if (nendoroid.getLookingForPriority() != null) {
			serializeString(serializer, Nendoroid.FIELD_LOOKING_FOR_PRIORITY, nendoroid.getLookingForPriority());
		}
		if (nendoroid.getAcquisitionOrder() != null) {
			serializeString(serializer, Nendoroid.FIELD_ACQUISITION_ORDER, nendoroid.getAcquisitionOrder());
		}
		if (nendoroid.getMainImage() != null) {
			serializer.startTag("", Nendoroid.FIELD_MAIN_IMAGE);
			serializeImage(serializer, nendoroid.getMainImage());
			serializer.endTag("", Nendoroid.FIELD_MAIN_IMAGE);
		}
		if (nendoroid.getThumbImage() != null) {
			serializer.startTag("", Nendoroid.FIELD_THUMB_IMAGE);
			serializeImage(serializer, nendoroid.getThumbImage());
			serializer.endTag("", Nendoroid.FIELD_THUMB_IMAGE);
		}
		if (nendoroid.getGscLink() != null) {
			serializeString(serializer, Nendoroid.FIELD_GSC_LINK, nendoroid.getGscLink());
		}
		serializeDouble(serializer, Nendoroid.FIELD_GSC_PRICE, nendoroid.getGscPrice());
		if (nendoroid.getReleaseDate() != null) {
			serializeString(serializer, Nendoroid.FIELD_RELEASE_DATE, nendoroid.getReleaseDate());
		}
		if (nendoroid.getNickname() != null) {
			serializeString(serializer, Nendoroid.FIELD_NICKNAME, nendoroid.getNickname());
		}
		if (nendoroid.getJapaneseName() != null) {
			serializeString(serializer, Nendoroid.FIELD_JAPANESE_NAME, nendoroid.getJapaneseName());
		}
		if (nendoroid.getOfficialName() != null) {
			serializeString(serializer, Nendoroid.FIELD_OFFICIAL_NAME, nendoroid.getOfficialName());
		}
		if (nendoroid.getSeries() != null) {
			serializeString(serializer, Nendoroid.FIELD_SERIES, nendoroid.getSeries());
		}
		if (nendoroid.getManufacturer() != null) {
			serializeString(serializer, Nendoroid.FIELD_MANUFACTURER, nendoroid.getManufacturer());
		}
		if (nendoroid.getCategory() != null) {
			serializeString(serializer, Nendoroid.FIELD_CATEGORY, nendoroid.getCategory());
		}
		if (nendoroid.getSpecifications() != null) {
			serializeString(serializer, Nendoroid.FIELD_SPECIFICATIONS, nendoroid.getSpecifications());
		}
		if (nendoroid.getSculptor() != null) {
			serializeString(serializer, Nendoroid.FIELD_SCULPTOR, nendoroid.getSculptor());
		}
		if (nendoroid.getCopyright() != null) {
			serializeString(serializer, Nendoroid.FIELD_COPYRIGHT, nendoroid.getCopyright());
		}
		if (nendoroid.getDescription() != null) {
			serializeString(serializer, Nendoroid.FIELD_DESCRIPTION, nendoroid.getDescription());
		}
		if (nendoroid.getNotes() != null) {
			serializeString(serializer, Nendoroid.FIELD_NOTES, nendoroid.getNotes());
		}
		if (nendoroid.getOthers() != null) {
			serializeString(serializer, Nendoroid.FIELD_OTHERS, nendoroid.getOthers());
		}
		
		if (nendoroid.getImageList() != null) {
			serializer.startTag("", Nendoroid.FIELD_IMAGE_LIST);
			serializeImageArray(serializer, nendoroid.getImageList());
			serializer.endTag("", Nendoroid.FIELD_IMAGE_LIST);
		}
		
		serializer.endTag("", Nendoroid.TABLE_NAME);
	}
	public static void serializeImageArray(XmlSerializer serializer, List<Image> imageList) 
			throws IllegalArgumentException, IllegalStateException, IOException {
		
		serializer.startTag("", Image.TABLE_NAME + "Array");
		for (Image image : imageList) {
			serializeImage(serializer, image);
		}
		serializer.endTag("", Image.TABLE_NAME + "Array");
	}
	
	public static void serializeImage(XmlSerializer serializer, Image image) 
			throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.startTag("", Image.TABLE_NAME);
		
		//TODO remove export of filepath
		serializeInt(serializer, Image.FIELD_ID, image.getId());
		serializeInt(serializer, Image.FIELD_NENDOROID, image.getNendoroid().getId());
		serializeString(serializer, Image.FIELD_FILEPATH, image.getFilepath());
		if (image.getWebpath() != null) {
			serializeString(serializer, Image.FIELD_WEBPATH, image.getWebpath());
		}

		serializer.endTag("", Image.TABLE_NAME);
	}
	
	private static void serializeString(XmlSerializer serializer, String name, String text) 
			throws IllegalArgumentException, IllegalStateException, IOException {

		serializer.startTag("", name);
		serializer.text(text);
		serializer.endTag("", name);
	}
	
	private static void serializeInt(XmlSerializer serializer, String name, int number) 
			throws IllegalArgumentException, IllegalStateException, IOException {

		serializer.startTag("", name);
		serializer.text(String.valueOf(number));
		serializer.endTag("", name);
	}
	
	private static void serializeDouble(XmlSerializer serializer, String name, double number) 
			throws IllegalArgumentException, IllegalStateException, IOException {

		serializer.startTag("", name);
		serializer.text(String.valueOf(number));
		serializer.endTag("", name);
	}
}