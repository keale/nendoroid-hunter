package com.jankiel.nendoroidhunter.fragments;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jankiel.genlibs.utils.StringTransformations;
import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.genlibs.utils.android.DialogUtilities.NumericEditTextWatcher;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.activities.PreferencesActivity;
import com.jankiel.nendoroidhunter.activities.ShopListActivity;
import com.jankiel.nendoroidhunter.adapters.ImagePagerAdapter;
import com.jankiel.nendoroidhunter.adapters.ImagePagerAdapter.OnImageViewClickListener;
import com.jankiel.nendoroidhunter.adapters.ImagePagerAdapter.OnImageViewLongClickListener;
import com.jankiel.nendoroidhunter.adapters.NendoroidListAdapater;
import com.jankiel.nendoroidhunter.asynctasks.DownloadNendoroidImagesIntentService;
import com.jankiel.nendoroidhunter.asynctasks.DownloadNendoroidImagesResultReceiver;
import com.jankiel.nendoroidhunter.asynctasks.DownloadNendoroidImagesResultReceiver.Receiver;
import com.jankiel.nendoroidhunter.asynctasks.UpdateNendoroidStatusTask;
import com.jankiel.nendoroidhunter.asynctasks.UpdateNendoroidStatusTask.OnUpdateNendoroidStatusCompleteListener;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.models.NendoroidInShop;
import com.jankiel.nendoroidhunter.models.Shop;
import com.jankiel.nendoroidhunter.utils.AppConstant;

public class NendoroidDetailFragment extends Fragment implements Receiver, 
		OnUpdateNendoroidStatusCompleteListener {

	public static final String EXTRA_KEY_NENDOROID_ID = "com.jankiel.nendoroidhunter.EXTRA_KEY_NENDOROID_ID";
	public static final String EXTRA_KEY_NENDOROID_LIST_TYPE = "com.jankiel.nendoroidhunter.EXTRA_KEY_NENDOROID_LIST_TYPE";
	
	public static final int REQUEST_ADD_SHOP = 1;
	
	private int mNendoroidListType;
	private boolean mWasNendoroidUpdated;
	
	private String mOldExactPrice; //XXX

	private LinearLayout mOtherDetailsContainer;
	private TextView mShowHideOtherDetailsTextView;
	
	private ViewPager mImagesViewPager;
	private ImagePagerAdapter mImagePagerAdapter;
	
	private LinearLayout mShopsContainer;
	private TextView mShowHideShopsTextView;
	
	private List<TextView> mPriceTextViewList;
	private List<TextView> mConditionTextViewList;
	
	private TextView mStatusTextView;
	
	private Nendoroid mNendoroid;
	private List<NendoroidInShop> mShopList;
	
	private Menu mMenu;
	
	private DatabaseManager mDbManager;
	private SharedPreferences mSharedPreferences;
	
	private DownloadNendoroidImagesResultReceiver mResultReceiver;
	private ProgressDialog mDownloadImagesProgressDialog;
	
	public NendoroidDetailFragment(int nendoroidListType) {
		this.mNendoroidListType = nendoroidListType;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		mNendoroid = new Nendoroid(getActivity());
		mDbManager = DatabaseManager.getInstance(getActivity());
		
		mResultReceiver = DownloadNendoroidImagesResultReceiver.getInstance(new Handler());
		
		//FIXME should I get on constructor instead?
		if (getArguments().containsKey(EXTRA_KEY_NENDOROID_ID)) {
			int nendoroidId = getArguments().getInt(EXTRA_KEY_NENDOROID_ID);

			mNendoroid = mDbManager.getNendoroid(nendoroidId, null);
			
			getActivity().setTitle(mNendoroid.getDescriptiveName());
			
			String whereClause = NendoroidInShop.FIELD_NENDOROID + " = " + nendoroidId;
			mShopList = mDbManager.getAllNendoroidInShopsWithConstraint(whereClause, NendoroidInShop.FIELD_SHOP);
		}
		
		mDownloadImagesProgressDialog = new ProgressDialog(getActivity());
		mDownloadImagesProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		
		int max = 1;
		if (mNendoroid.getImageList() != null) {
			for (Image image : mNendoroid.getImageList()) {
				if (image != null && image.exists()) {
					max++;
				}
			}
		}
		
		mDownloadImagesProgressDialog.setMax(max);
		mDownloadImagesProgressDialog.setCancelable(true); //TODO maybe add a stop/cancel dialog
		mDownloadImagesProgressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, 
				getActivity().getString(R.string.dialog_button_run_in_background), 
				new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mDownloadImagesProgressDialog.dismiss();
				DialogUtilities.showToast(getActivity(), getActivity()
						.getString((R.string.toast_message_running_in_background)));
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_ADD_SHOP && resultCode == ShopListActivity.RESULT_OK) {
			showNewShopWizardDialog();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_nendoroid_detail, container, false);

		if (mNendoroid != null) {
			mImagesViewPager = (ViewPager) rootView.findViewById(R.id.nendoroid_images_view_pager);
			
			TextView numberTextView = (TextView) rootView.findViewById(R.id.nendoroid_number);
			TextView nameTextView = (TextView) rootView.findViewById(R.id.nendoroid_name);
			mStatusTextView = (TextView) rootView.findViewById(R.id.nendoroid_status);
			
			mOtherDetailsContainer = (LinearLayout) rootView.findViewById(R.id.other_details_root_container);
			mShowHideOtherDetailsTextView = (TextView) rootView.findViewById(R.id.show_label_hide);
			
			TextView nicknamePrefixTextView = (TextView) rootView.findViewById(R.id.prefix_nickname);
			TextView nicknameTextView = (TextView) rootView.findViewById(R.id.nendoroid_nickname);
			
			TextView officialNamePrefixTextView = (TextView) rootView.findViewById(R.id.prefix_official_name);
			TextView officialNameTextView = (TextView) rootView.findViewById(R.id.nendoroid_official_name);
			
			TextView japaneseNamePrefixTextView = (TextView) rootView.findViewById(R.id.prefix_japanese_name);
			TextView japaneseNameTextView = (TextView) rootView.findViewById(R.id.nendoroid_japanese_name);
			
			TextView seriesPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_series);
			TextView seriesTextView = (TextView) rootView.findViewById(R.id.nendoroid_series);
			
			TextView manufacturerPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_manufacturer);
			TextView manufacturerTextView = (TextView) rootView.findViewById(R.id.nendoroid_manufacturer);
			
			TextView categoryPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_category);
			TextView categoryTextView = (TextView) rootView.findViewById(R.id.nendoroid_category);
			
			TextView gscPricePrefixTextView = (TextView) rootView.findViewById(R.id.prefix_gsc_price);
			TextView gscPriceTextView = (TextView) rootView.findViewById(R.id.nendoroid_gsc_price);
			
			TextView releaseDatePrefixTextView = (TextView) rootView.findViewById(R.id.prefix_release_date);
			TextView releaseDateTextView = (TextView) rootView.findViewById(R.id.nendoroid_release_date);
			
			TextView specificationsPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_specifications);
			TextView specificationsTextView = (TextView) rootView.findViewById(R.id.nendoroid_specifications);
			
			TextView sculptorPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_sculptor);
			TextView sculptorTextView = (TextView) rootView.findViewById(R.id.nendoroid_sculptor);
			
			TextView othersPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_others);
			TextView othersTextView = (TextView) rootView.findViewById(R.id.nendoroid_others);
			
			TextView descriptionPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_description);
			TextView descriptionTextView = (TextView) rootView.findViewById(R.id.nendoroid_description);
			
			TextView gscLinkPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_gsc_link);
			TextView gscLinkTextView = (TextView) rootView.findViewById(R.id.nendoroid_gsc_link);
			
			TextView notesPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_notes);
			TextView notesTextView = (TextView) rootView.findViewById(R.id.nendoroid_notes);
			
			TextView copyrightPrefixTextView = (TextView) rootView.findViewById(R.id.prefix_copyright);
			TextView copyrightTextView = (TextView) rootView.findViewById(R.id.nendoroid_copyright);
			
			mShopsContainer = (LinearLayout) rootView.findViewById(R.id.shop_container);
			mShowHideShopsTextView = (TextView) rootView.findViewById(R.id.show_hide_shops);
			
			mImagePagerAdapter = new ImagePagerAdapter(getActivity(), mNendoroid);
			mImagePagerAdapter.setOnImageViewClickListener(new OnImageViewClickListener() {
				
				@Override
				public void onImageViewClick(View view, int position) {
					//the image list on the adapter is prefixed with the main image
					Image image = mNendoroid.getMainImage();
					if (position > 0) {
						image = mNendoroid.getImageList().get(position - 1);
					}
					if (image != null && image.getCaption() != null
							&& !image.getCaption().equals("")) {
						DialogUtilities.showToast(getActivity(), image.getCaption());
					} else {
//						String ordinal = "main";
//						if (position > 0) {
//							ordinal = StringTransformations.convertNumberToOrdinal(position + 1);
//						}
//						
//						DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_nendoroid_other_image,
//								mNendoroid.getDescriptiveName(), ordinal));
					}
				}
			});
			mImagePagerAdapter.setOnImageViewLongClickListener(new OnImageViewLongClickListener() {
				
				@Override
				public boolean onImageViewLongClick(View view, int position) {
					//the image list on the adapter is prefixed with the main image
					Image image = mNendoroid.getMainImage();
					if (position > 0) {
						image = mNendoroid.getImageList().get(position - 1);
					}
					
					if (image != null && image.exists()) {
						Intent imageIntent = new Intent();
						
						imageIntent.setAction(Intent.ACTION_VIEW);
						imageIntent.setDataAndType(Uri.parse("file://" + image.getFilepath()), "image/*");
						
						startActivity(imageIntent);
					}
					return false;
				}
			});
			
			mImagesViewPager.setAdapter(mImagePagerAdapter);
			mImagesViewPager.setOffscreenPageLimit(mImagePagerAdapter.getCount());
			mImagesViewPager.setPageMargin(15);
			mImagesViewPager.setClipChildren(false);
						
			numberTextView.setText(mNendoroid.getNumber());
			nameTextView.setText(mNendoroid.getName());
			
			int mainStatusReference = Nendoroid.STATUS_INTERESTED;
			if (mNendoroidListType == NendoroidListAdapater.TYPE_LOOKING_FOR) {
				mainStatusReference = Nendoroid.STATUS_LOOKING_FOR;
			} else if (mNendoroidListType == NendoroidListAdapater.TYPE_ACQUIRED) {
				mainStatusReference = Nendoroid.STATUS_ACQUIRED;
			} 
			
			String statusDetails = "<b>" + getString(R.string.prefix_status) + "</b>: " 
					+ mNendoroid.getDescriptiveStatus(mainStatusReference);
			mStatusTextView.setText(Html.fromHtml(statusDetails));
			
			mShowHideOtherDetailsTextView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (mOtherDetailsContainer.getVisibility() == View.GONE) {
						showOtherDetails(true);
					} else {
						hideOtherDetails(true);
					}
				}
			});
			
			if (mNendoroid.getNickname() != null && mNendoroid.getNickname() != ""
					&& mNendoroid.getNickname().trim().length() > 0) {
				nicknameTextView.setText(mNendoroid.getNickname());
			} else {
				nicknamePrefixTextView.setVisibility(View.GONE);
				nicknameTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getOfficialName() != null && mNendoroid.getOfficialName() != ""
					&& mNendoroid.getOfficialName().trim().length() > 0) {
				officialNameTextView.setText(mNendoroid.getOfficialName());
			} else {
				officialNamePrefixTextView.setVisibility(View.GONE);
				officialNameTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getJapaneseName() != null && mNendoroid.getJapaneseName() != ""
					&& mNendoroid.getJapaneseName().trim().length() > 0) {
				japaneseNameTextView.setText(mNendoroid.getJapaneseName());
			} else {
				japaneseNamePrefixTextView.setVisibility(View.GONE);
				japaneseNameTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getSeries() != null && mNendoroid.getSeries() != ""
					&& mNendoroid.getSeries().trim().length() > 0) {
				seriesTextView.setText(mNendoroid.getSeries());
			} else {
				seriesPrefixTextView.setVisibility(View.GONE);
				seriesTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getManufacturer() != null && mNendoroid.getManufacturer() != ""
					&& mNendoroid.getManufacturer().trim().length() > 0) {
				manufacturerTextView.setText(mNendoroid.getManufacturer());
			} else {
				manufacturerPrefixTextView.setVisibility(View.GONE);
				manufacturerTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getCategory() != null && mNendoroid.getCategory() != ""
					&& mNendoroid.getCategory().trim().length() > 0) {
				categoryTextView.setText(mNendoroid.getCategory());
			} else {
				categoryPrefixTextView.setVisibility(View.GONE);
				categoryTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getGscPrice() != 0.0) {
				DecimalFormat decimalFormat = new DecimalFormat(getActivity().getString(R.string.decimal_format_pattern_yen));
				
				gscPriceTextView.setText(decimalFormat.format(mNendoroid.getGscPrice()));
			} else {
				gscPricePrefixTextView.setVisibility(View.GONE);
				gscPriceTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getReleaseDate() != null && mNendoroid.getReleaseDate() != ""
					&& mNendoroid.getReleaseDate().trim().length() > 0) {
				releaseDateTextView.setText(mNendoroid.getReleaseDate());
			} else {
				releaseDatePrefixTextView.setVisibility(View.GONE);
				releaseDateTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getSpecifications() != null && mNendoroid.getSpecifications() != ""
					&& mNendoroid.getSpecifications().trim().length() > 0) {
				specificationsTextView.setText(mNendoroid.getSpecifications());
			} else {
				specificationsPrefixTextView.setVisibility(View.GONE);
				specificationsTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getSculptor() != null && mNendoroid.getSculptor() != ""
					&& mNendoroid.getSculptor().trim().length() > 0) {
				sculptorTextView.setText(mNendoroid.getSculptor());
			} else {
				sculptorPrefixTextView.setVisibility(View.GONE);
				sculptorTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getOthers() != null && mNendoroid.getOthers() != ""
					&& mNendoroid.getOthers().trim().length() > 0) {
				othersTextView.setText(Html.fromHtml(mNendoroid.getOthers()));
			} else {
				othersPrefixTextView.setVisibility(View.GONE);
				othersTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getDescription() != null && mNendoroid.getDescription() != ""
					&& mNendoroid.getDescription().trim().length() > 0) {
				descriptionTextView.setText(Html.fromHtml(mNendoroid.getDescription()));
			} else {
				descriptionPrefixTextView.setVisibility(View.GONE);
				descriptionTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getGscLink() != null && mNendoroid.getGscLink() != ""
					&& mNendoroid.getGscLink().trim().length() > 0) {
				if (!mNendoroid.getGscLink().startsWith("http://www.goodsmile.info/")) {
					gscLinkPrefixTextView.setText(R.string.prefix_ref_link);
				}
				
				gscLinkTextView.setText(mNendoroid.getGscLink());
				gscLinkTextView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mNendoroid.getGscLink()));
						startActivity(browserIntent);
					}
				});
			} else {
				gscLinkPrefixTextView.setVisibility(View.GONE);
				gscLinkTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getNotes() != null && mNendoroid.getNotes() != ""
					&& mNendoroid.getNotes().trim().length() > 0) {
				notesTextView.setText(Html.fromHtml(mNendoroid.getNotes()));
			} else {
				notesPrefixTextView.setVisibility(View.GONE);
				notesTextView.setVisibility(View.GONE);
			}

			if (mNendoroid.getCopyright() != null && mNendoroid.getCopyright() != "" 
					&& mNendoroid.getCopyright().trim().length() > 0) {
				copyrightTextView.setText(Html.fromHtml(mNendoroid.getCopyright()));
			} else {
				copyrightPrefixTextView.setVisibility(View.GONE);
				copyrightTextView.setVisibility(View.GONE);
			}
			
			buildShopsContainer();
			
			mShowHideShopsTextView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (mShopsContainer.getVisibility() == View.GONE) {
						showShops(true);
					} else {
						hideShops(true);
					}
				}
			});
			
			if (mNendoroidListType == NendoroidListAdapater.TYPE_LOOKING_FOR) {
				hideOtherDetails(false);
			} else if (mNendoroidListType == NendoroidListAdapater.TYPE_ACQUIRED
					|| mShopList == null || mShopList.size() == 0) {
				hideShops(false);
			}
		}

		return rootView;
	}
	
	private void buildShopsContainer() {
		TypedValue typedValue = new TypedValue();
		getActivity().getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, typedValue, true); 
		
		DisplayMetrics metrics = new android.util.DisplayMetrics(); 
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics); 
		
		int listItemPreferredHeight = (int) typedValue.getDimension(metrics);
		
		mShopsContainer.removeAllViews();
		mPriceTextViewList = new ArrayList<TextView>();
		mConditionTextViewList = new ArrayList<TextView>();
		
		for (int i = 0; i < mShopList.size(); i++) {
			final NendoroidInShop nendoroidInShop = mShopList.get(i);
			
			LinearLayout shopViewItem = new LinearLayout(getActivity());
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT, 
					LinearLayout.LayoutParams.WRAP_CONTENT);
			int oneDp = 
					(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, 
							getResources().getDisplayMetrics());
			int twoDp = 
					(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, 
							getResources().getDisplayMetrics());
			
			shopViewItem.setOrientation(LinearLayout.VERTICAL);
			shopViewItem.setLayoutParams(layoutParams);
			
			shopViewItem.setTag(i);
			shopViewItem.setPadding(twoDp, oneDp, twoDp, oneDp);
			
			TextView nameTextView = new TextView(getActivity());
			
			nameTextView.setLayoutParams(layoutParams);
			nameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
			nameTextView.setText(nendoroidInShop.getDescriptiveName());
			
			shopViewItem.addView(nameTextView);
			
			TextView priceTextView = new TextView(getActivity());
			
			priceTextView.setLayoutParams(layoutParams);
			priceTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			
			if (nendoroidInShop.getExactPrice() == 0.0 && (nendoroidInShop.getPriceRange() != null 
					|| nendoroidInShop.getPriceRange().equals(""))) {
				priceTextView.setText(nendoroidInShop.getPriceRange());
			} else {
				String userCurrency = mSharedPreferences
						.getString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, getActivity().getString(R.string.prefix_currency_yen));
				String decimalFormatPattern = userCurrency + " " + getActivity().getString(R.string.decimal_format_pattern_basic);
				
				DecimalFormat decimalFormat = new DecimalFormat(decimalFormatPattern);
				priceTextView.setText(decimalFormat.format(nendoroidInShop.getExactPrice()));
			}
			
			shopViewItem.addView(priceTextView);
			mPriceTextViewList.add(priceTextView);
			
			TextView conditionTextView = new TextView(getActivity());
			
			conditionTextView.setLayoutParams(layoutParams);
			conditionTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			conditionTextView.setText(NendoroidInShop.getConditionName(getActivity(), nendoroidInShop.getCondition()));
			
			shopViewItem.addView(conditionTextView);
			mConditionTextViewList.add(conditionTextView);
			
			shopViewItem.setMinimumHeight(listItemPreferredHeight);
			shopViewItem.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					if (nendoroidInShop.getShop().hasValidCoordinates()) {
						double latitude = nendoroidInShop.getShop().getLatitude();
						double longitude = nendoroidInShop.getShop().getLongitude();
						Uri shopLatLngUri = Uri.parse("geo:" + latitude + "," + longitude + "");
						
						Intent mapIntent = new Intent(Intent.ACTION_VIEW, shopLatLngUri);
						if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
							startActivity(mapIntent);
						} else {
							DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_no_map_app,
									nendoroidInShop.getDescriptiveName()));
						}
					} else {
						String website = nendoroidInShop.getShop().getWebsiteOrFacebook();
						if (website != null && !website.equals("")) {
							Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
							startActivity(websiteIntent);
						} else {
							DialogUtilities.showToast(getActivity(), 
									getActivity().getString(R.string.toast_message_no_valid_coordinates_website_or_facebook,
									nendoroidInShop.getDescriptiveName()));
						}
					}
				}
			});
			
			shopViewItem.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View view) {
					showNendoroidInShopActionsDialog(nendoroidInShop, view);
					return false;
				}
			});
			
			mShopsContainer.addView(shopViewItem);

			View horizontalBar = new View(getActivity());
			LinearLayout.LayoutParams barLayoutParams = 
					new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, oneDp);
			barLayoutParams.setMargins(0, 0, 0, 0);
			
			horizontalBar.setLayoutParams(barLayoutParams);
			horizontalBar.setBackgroundColor(getResources().getColor(R.color.gray));
			
			mShopsContainer.addView(horizontalBar);
		}
		mShopsContainer.invalidate();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		boolean hasSeenTip = mSharedPreferences
				.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_HAS_SEEN_NENDOROID_DETAIL_TIP, false);
		
		if (!hasSeenTip) {
			new Timer().schedule(new TimerTask() {
				
				@Override
				public void run() {
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							DialogUtilities.showBasicInfoDialog(getActivity(), 
									getActivity().getString(R.string.dialog_message_nendoroid_detail_tip));
							
							mSharedPreferences.edit()
									.putBoolean(AppConstant.SHARED_PREFERENCES_KEY_HAS_SEEN_NENDOROID_DETAIL_TIP, true)
									.commit();
						}
					});
				}
			}, 400); 
		}

		
		mResultReceiver.setReceiver(this);
		
		//TODO change text only if the current nendo is this one
		if (DownloadNendoroidImagesIntentService.isRunning(getActivity())) {
			if (mMenu != null) { //TODO if null, do this at a later time; also consider doing more tasks at once
				MenuItem downloadOtherImagesMenuItem = mMenu.findItem(R.id.menu_download_other_images);
				downloadOtherImagesMenuItem.setTitle(getActivity()
						.getString(R.string.menu_downloading_nendoroid_list));
			}
		}
		
		checkAndHideDownloadOtherImagesMenuItem();
	}
	
	@Override
	public void onStop() {		
		mResultReceiver.setReceiver(null);
		
		super.onStop();
	};
	
	public boolean wasNendoroidUpdated() {
		return mWasNendoroidUpdated;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_nendoroid_detail, menu);
		mMenu = menu;
		
		super.onCreateOptionsMenu(menu,inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_add_to_shop:
			showNewShopWizardDialog();
			return true;
		case R.id.menu_update_status:
			showUpdateStatusDialog();
			return true;
		case R.id.menu_download_other_images:
			//TODO maybe disable this when all images are present
			//TODO disable if downloading basic?
			if (DownloadNendoroidImagesIntentService.isRunning(getActivity())) {
				if (mDownloadImagesProgressDialog != null && !mDownloadImagesProgressDialog.isShowing()) {
					mDownloadImagesProgressDialog.show();
				}
			} else {
				startDownloadNendoroidImagesIntentService();
			}
			return true;
		case R.id.menu_preferences:
			Intent preferencesIntent = new Intent(getActivity(), PreferencesActivity.class);
			getActivity().startActivityForResult(preferencesIntent, 0);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public boolean handleKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_MENU:
			if (mMenu != null) {
				mMenu.performIdentifierAction(R.id.menu_overflow, 0);
			}
			break;
		}
		return true;
	}
	
	//FIXME for some reason this is not working properly?
	private void checkAndHideDownloadOtherImagesMenuItem() {
//		boolean hasDownloadedAllImages = true;
//		
//		if (mNendoroid.getImageList() != null) {
//			for (Image image : mNendoroid.getImageList()) {
//				if (image == null || !image.exists()) {
//					hasDownloadedAllImages = false;
//					break;
//				}
//			}
//		}
//		
//		if (hasDownloadedAllImages) {
//			if (mMenu != null) {
//				MenuItem downloadOtherImagesMenuItem = mMenu.findItem(R.id.menu_download_other_images);
//				downloadOtherImagesMenuItem.setVisible(false);
//			}
//		}
	}
	
	public void executeUpdateNendoroidStatusTask(int oldStatus, int newStatus) {
		UpdateNendoroidStatusTask updateNendoroidStatusTask = 
				new UpdateNendoroidStatusTask(getActivity(), mDbManager, 
						mNendoroid, oldStatus, newStatus);
		
		updateNendoroidStatusTask.setOnUpdateNendoroidStatusCompleteListener(this);
		updateNendoroidStatusTask.execute();
	}
	
	public void startDownloadNendoroidImagesIntentService() {
		Intent downloadNendoroidImagesIntent = 
				new Intent(Intent.ACTION_SYNC, null, getActivity(), DownloadNendoroidImagesIntentService.class);
		
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_DOWNLOAD_NENDOROID_IMAGES_RESULT_RECEIVER, mResultReceiver);
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_DESTINATION_DIRECTORY, Environment.getExternalStorageDirectory().getAbsolutePath());
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_IMAGES_TO_DOWNLOAD, DownloadNendoroidImagesIntentService.OTHER_IMAGES);
		
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_GET_ALL_NENDOROIDS, false);
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_NENDOROID_ID_ARRAY, new int[] {mNendoroid.getId()});
		
		getActivity().startService(downloadNendoroidImagesIntent);
		
		mDownloadImagesProgressDialog.setMessage(getActivity().getString(R.string.progress_message_starting_download));
		mDownloadImagesProgressDialog.show();
	}
	
	private void showUpdateStatusDialog() {
		final String[][] statusTable = Nendoroid.getStatusNameTable(getActivity());
		
		DialogUtilities.showDialogWithItems(getActivity(), 
				getActivity().getString(R.string.dialog_title_status_selection), 
				statusTable[1], new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						int newStatus = Integer.parseInt(statusTable[0][which]);
						int oldStatus = mNendoroid.getStatus();
						
						executeUpdateNendoroidStatusTask(oldStatus, newStatus);
					}
		}, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	private void showUpdateNendoroidInShopPriceRangeDialog(DialogInterface.OnClickListener posButtonListener) {
		String userCurrency = mSharedPreferences
				.getString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, getActivity().getString(R.string.prefix_currency_yen));
		DialogUtilities.showDialogWithItems(getActivity(), getActivity().getString(R.string.dialog_title_price_range_select), 
				NendoroidInShop.getPriceRangeArray(getActivity(), userCurrency), posButtonListener, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	private void showUpdateNendoroidInShopExactPriceDialog(final NendoroidInShop nendoroidInShop, final int index) {
		final String userCurrency = mSharedPreferences
				.getString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, getActivity().getString(R.string.prefix_currency_yen));
		final String decimalFormatterPattern = userCurrency + " " + getActivity().getString(R.string.decimal_format_pattern_basic);
		
		
		double exactPrice = nendoroidInShop.getExactPrice();
		final DecimalFormat decimalFormat = new DecimalFormat(decimalFormatterPattern);
		
		if (exactPrice == 0.0) {
			exactPrice = nendoroidInShop.getExactPriceFromPriceRange(userCurrency);
		}
		
		DialogUtilities.showDialogWithNumericEditText(getActivity(), 
				getActivity().getString(R.string.dialog_title_exact_price_entry), 
				decimalFormat.format(exactPrice), new NumericEditTextWatcher() {
					
					@Override
					public void onTextChanged(CharSequence charSequence, int start, int before, int count, 
							EditText editText, TextWatcher textWatcher) {
						
						if (mOldExactPrice != null) {
							editText.removeTextChangedListener(textWatcher);
							editText.setText(mOldExactPrice);
							editText.addTextChangedListener(textWatcher);
							
							editText.setSelection(mOldExactPrice.length());
							
							mOldExactPrice = null;
						}
						
						String number = charSequence.toString();
						
						if (number.endsWith(".")) {
							number += "0";
							//FIXME fix decimal
						}
						
						try {
							String[] decimalFormatterPatternComponents = decimalFormatterPattern.split(" ");
							String currencyPrefix = decimalFormatterPatternComponents[0] + " ";
							
							if (number.startsWith(currencyPrefix)) {
								number = number.substring(currencyPrefix.length());
							}
							number = number.replaceAll(",", "");
							
							String formattedNumber = decimalFormat.format(Double.parseDouble(number));
							
							editText.removeTextChangedListener(textWatcher);
							editText.setText(formattedNumber);
							editText.addTextChangedListener(textWatcher);
							
							int changeInLength = formattedNumber.length() - charSequence.toString().length();
							
							editText.setSelection(start + count + changeInLength);
						} catch (Exception e) {
							e.printStackTrace();
							//FIXME breaks if prefix is edited 
						}
					}
					
					@Override
					public void beforeTextChanged(CharSequence charSequence, int start, int count,
							int after, EditText editText, TextWatcher textWatcher) {
						String[] decimalFormatterPatternComponents = decimalFormatterPattern.split(" ");
						String currencyPrefix = decimalFormatterPatternComponents[0] + " ";
						
						if (start <= currencyPrefix.length()) {
							mOldExactPrice = charSequence.toString();
						} else {
							mOldExactPrice = null;
						}
					}
					
					@Override
					public void afterTextChanged(Editable string, EditText editText, TextWatcher textWatcher) {
						
					}
				},
				getActivity().getString(R.string.dialog_button_update), new DialogUtilities.OnEditTextDialogClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, EditText editText) {
						
						nendoroidInShop.setExactPrice(editText.getText().toString(), userCurrency + " ");
						double enteredExactPrice = nendoroidInShop.getExactPrice();
						try {
							boolean isSuccessful = mDbManager.updateNendoroidInShop(nendoroidInShop);
							if (isSuccessful) {
								mPriceTextViewList.get(index).setText(decimalFormat.format(enteredExactPrice));
								
								DialogUtilities.showToast(getActivity(), getActivity().getString(
										R.string.toast_message_exact_price_update_success, 
										mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
							} else {
								DialogUtilities.showBasicErrorDialog(getActivity(), getString(
										R.string.dialog_message_error_updating_exact_price, 
										mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
							}
							
						} catch (Exception e) {
							DialogUtilities.showBasicErrorDialog(getActivity(), getString(
									R.string.dialog_message_error_updating_exact_price_exception_message, 
									mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName(),
									e.getMessage()));
							e.printStackTrace();
						}
					}
				}, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	private void showUpdateNendoroidInShopConditionDialog(DialogInterface.OnClickListener posButtonListener) {
		final String[][] conditionTable = NendoroidInShop.getConditionNameTable(getActivity());
		
		DialogUtilities.showDialogWithItems(getActivity(), getActivity().getString(R.string.dialog_title_condition_selection), 
				conditionTable[1], posButtonListener, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	private void showNewShopWizardDialog() {
		final String userCurrency = mSharedPreferences
				.getString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, getActivity().getString(R.string.prefix_currency_yen));
		final List<Shop> allShopsList = mDbManager.getAllShopsWithConstraint(null, Shop.FIELD_NAME);
		
		if (allShopsList == null || allShopsList.size() == 0) {
			Intent shopListIntent = new Intent(getActivity(), ShopListActivity.class);
			shopListIntent.putExtra(ShopListFragment.EXTRA_KEY_SHOULD_ADD_SHOP, true);
			
			getActivity().startActivityForResult(shopListIntent, REQUEST_ADD_SHOP);
			return;
		}
		
		final String[] shopNames = new String[allShopsList.size()];
		for (int i = 0; i < shopNames.length; i++) {
			shopNames[i] = allShopsList.get(i).getName();
		}
		
		DialogUtilities.showDialogWithItems(getActivity(), getActivity().getString(R.string.dialog_title_shop_selection), 
				shopNames, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int shopIndex) {
				final Shop selectedShop = allShopsList.get(shopIndex);
				final String[] priceRangeArray = NendoroidInShop.getPriceRangeArray(getActivity(), userCurrency);
				
				showUpdateNendoroidInShopPriceRangeDialog(new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int priceIndex) {
								final String selectedPriceRange = priceRangeArray[priceIndex];
								showUpdateNendoroidInShopConditionDialog(new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int conditionIndex) {
												final String[][] conditionTable = NendoroidInShop.getConditionNameTable(getActivity());
												final int selectedCondition = Integer.parseInt(conditionTable[0][conditionIndex]);
												
												NendoroidInShop nendoroidInShop = 
														new NendoroidInShop(getActivity(), mNendoroid, selectedShop, selectedPriceRange, selectedCondition);

												try {
													int nendoroidInShopId = (int) mDbManager.insertNendoroidInShop(nendoroidInShop);
													nendoroidInShop.setId(nendoroidInShopId);
													
													if (nendoroidInShopId != -1) {
														mShopList.add(nendoroidInShop);
														buildShopsContainer();
														
														DialogUtilities.showToast(getActivity(),
																getActivity().getString(R.string.toast_message_nendoroid_added_to_shop_success, 
																mNendoroid.getDescriptiveName(), selectedShop.getName()));
													} else {
														DialogUtilities.showBasicErrorDialog(getActivity(), 
															getActivity().getString(R.string.dialog_message_error_adding_nendoroid_to_shop,
															mNendoroid.getDescriptiveName(), selectedShop.getName()));
													}
													
												} catch (Exception e) {
														DialogUtilities.showBasicErrorDialog(getActivity(), 
															getActivity().getString(R.string.dialog_message_error_adding_nendoroid_to_shop_exception_message,
															mNendoroid.getDescriptiveName(), selectedShop.getName(),
															e.getMessage()));
													e.printStackTrace();
												}
											}
								});
							}
				});
			}
		}, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	public void showNendoroidInShopActionsDialog(final NendoroidInShop nendoroidInShop, final View view) {
		final String userCurrency = mSharedPreferences
				.getString(AppConstant.SHARED_PREFERENCES_KEY_USER_CURRENCY, getActivity().getString(R.string.prefix_currency_yen));
		
		final String[] actionsArray = {
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_open_map),
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_open_website),
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_open_facebook),
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_update_price_range),
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_update_exact_price),
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_update_condition),
				getActivity().getString(R.string.dialog_item_nendoroid_in_shop_action_remove_shop)
		};
		
		DialogUtilities.showDialogWithItems(getActivity(), getActivity().getString(R.string.dialog_title_shop_action_selection), 
				actionsArray, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == 0) {
							if (nendoroidInShop.getShop().hasValidCoordinates()) {
								double latitude = nendoroidInShop.getShop().getLatitude();
								double longitude = nendoroidInShop.getShop().getLongitude();
								Uri shopLatLngUri = Uri.parse("geo:" + latitude + "," + longitude + "");
								
								Intent mapIntent = new Intent(Intent.ACTION_VIEW, shopLatLngUri);
								if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
									startActivity(mapIntent);
								} else {
									DialogUtilities.showToast(getActivity(), getActivity().getString(
											R.string.toast_message_no_map_app, mNendoroid.getDescriptiveName()));
								}
							} else {
								DialogUtilities.showToast(getActivity(), 
										getActivity().getString(R.string.toast_message_no_valid_coordinates,
												nendoroidInShop.getDescriptiveName()));
							}
						} else if (which == 1) {
							String website = nendoroidInShop.getShop().getWebsite();
							if (website != null && !website.equals("")) {
								Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
								startActivity(websiteIntent);
							} else {
								DialogUtilities.showToast(getActivity(), 
										getActivity().getString(R.string.toast_message_no_website,
												nendoroidInShop.getDescriptiveName()));
							}
						} else if (which == 2) {
							String facebook = nendoroidInShop.getShop().getFacebook();
							if (facebook != null && !facebook.equals("")) {
								Intent facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebook));
								startActivity(facebookIntent);
							} else {
								DialogUtilities.showToast(getActivity(), 
										getActivity().getString(R.string.toast_message_no_facebook,
												nendoroidInShop.getDescriptiveName()));
							}
						} else if (which == 3) {
							final String[] priceRangeArray = NendoroidInShop.getPriceRangeArray(getActivity(), userCurrency);
							
							showUpdateNendoroidInShopPriceRangeDialog(new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									String selectedPriceRange = priceRangeArray[which];
									nendoroidInShop.setPriceRange(selectedPriceRange);
									nendoroidInShop.setExactPrice(0.0);
									
									try {
										boolean isSuccessful = mDbManager.updateNendoroidInShop(nendoroidInShop);
										if (isSuccessful) {
											int index = Integer.parseInt(view.getTag().toString());
											mPriceTextViewList.get(index).setText(selectedPriceRange);
											
											DialogUtilities.showToast(getActivity(), getActivity().getString(
													R.string.toast_message_price_range_update_success, 
													mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
										} else {
											DialogUtilities.showBasicErrorDialog(getActivity(), getString(
													R.string.dialog_message_error_updating_price_range, 
													mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
										}
										
									} catch (Exception e) {
										DialogUtilities.showBasicErrorDialog(getActivity(), getString(
												R.string.dialog_message_error_updating_price_range_exception_message, 
												mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName(),
												e.getMessage()));
										e.printStackTrace();
									}
								}
							});
						} else if (which == 4) {
							int index = Integer.parseInt(view.getTag().toString());
							showUpdateNendoroidInShopExactPriceDialog(nendoroidInShop, index);
						} else if (which == 5) {
							showUpdateNendoroidInShopConditionDialog(new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									String[][] conditionTable = NendoroidInShop.getConditionNameTable(getActivity());
									
									int selectedCondition = Integer.parseInt(conditionTable[0][which]);
									nendoroidInShop.setCondition(selectedCondition);
									
									try {
										boolean isSuccessful = mDbManager.updateNendoroidInShop(nendoroidInShop);
										if (isSuccessful) {
											int index = Integer.parseInt(view.getTag().toString());
											mConditionTextViewList.get(index).setText(NendoroidInShop.getConditionName(getActivity(), selectedCondition));
											
											DialogUtilities.showToast(getActivity(), getActivity().getString(
													R.string.toast_message_condition_update_success, 
													mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
										} else {
											DialogUtilities.showBasicErrorDialog(getActivity(), getString(
													R.string.dialog_message_error_updating_condition, 
													mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
										}
										
									} catch (Exception e) {
										DialogUtilities.showBasicErrorDialog(getActivity(), getString(
												R.string.dialog_message_error_updating_condition_exception_message, 
												mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName(),
												e.getMessage()));
										e.printStackTrace();
									}
								}
							});
						} else if (which == 6) {
							DialogUtilities.showDialog(getActivity(), getActivity().getString(R.string.dialog_title_confirmation), 
									getActivity().getString(R.string.dialog_message_remove_nendoroid_from_shop_confirmation, 
									mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()), 
									getActivity().getString(R.string.dialog_button_remove), new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											try {
												boolean isSuccesful = mDbManager.hardDeleteNendoroidInShop(nendoroidInShop);
												
												if (isSuccesful) {
													mShopList.remove(nendoroidInShop);
													buildShopsContainer();
													
													DialogUtilities.showToast(getActivity(),
															getActivity().getString(R.string.toast_message_nendoroid_removed_from_shop_success, 
															mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
												} else {
													DialogUtilities.showBasicErrorDialog(getActivity(), 
														getActivity().getString(R.string.dialog_message_error_removing_nendoroid_from_shop,
														mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName()));
												}
												
											} catch (Exception e) {
													DialogUtilities.showBasicErrorDialog(getActivity(), 
														getActivity().getString(R.string.dialog_message_error_removing_nendoroid_from_shop_exception_message,
														mNendoroid.getDescriptiveName(), nendoroidInShop.getDescriptiveName(),
														e.getMessage()));
												e.printStackTrace();
											}
										}
									}, getActivity().getString(R.string.dialog_button_cancel));
						}
					}
				}, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	private void showOtherDetails(boolean shouldAnimate) {
		if (mOtherDetailsContainer.getVisibility() == View.GONE) {
			mOtherDetailsContainer.setVisibility(View.VISIBLE);
			
			if (shouldAnimate) {
				mOtherDetailsContainer.animate()
					.alpha(1.0f)
					.setDuration(400)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							super.onAnimationEnd(animation);
							mOtherDetailsContainer.setVisibility(View.VISIBLE);
						}
					});
			}
			mShowHideOtherDetailsTextView.setText(R.string.label_hide);
		}
	}
	
	private void hideOtherDetails(boolean shouldAnimate) {
		if (mOtherDetailsContainer.getVisibility() == View.VISIBLE) {
			if (shouldAnimate) {
				mOtherDetailsContainer.animate()
					.alpha(0.0f)
					.setDuration(400)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							super.onAnimationEnd(animation);
							mOtherDetailsContainer.setVisibility(View.GONE);
						}
					});
			} else {
				mOtherDetailsContainer.setVisibility(View.GONE);
			}
			mShowHideOtherDetailsTextView.setText(R.string.label_show);
		}
	}
	
	private void showShops(boolean shouldAnimate) {
		if (mShopsContainer.getVisibility() == View.GONE) {
			mShopsContainer.setVisibility(View.VISIBLE);
			
			if (shouldAnimate) {
				mShopsContainer.animate()
					.alpha(1.0f)
					.setDuration(400)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							super.onAnimationEnd(animation);
							mShopsContainer.setVisibility(View.VISIBLE);
						}
					});
			}
			mShowHideShopsTextView.setText(R.string.label_hide);
		}
	}
	
	private void hideShops(boolean shouldAnimate) {
		if (mShopsContainer.getVisibility() == View.VISIBLE) {
			if (shouldAnimate) {
				mShopsContainer.animate()
					.alpha(0.0f)
					.setDuration(400)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							super.onAnimationEnd(animation);
							mShopsContainer.setVisibility(View.GONE);
						}
					});
			} else {
				mShopsContainer.setVisibility(View.GONE);
			}
			mShowHideShopsTextView.setText(R.string.label_show);
		}
	}
	
	@Override
	public void onUpdateNendoroidStatusComplete(boolean isSuccessful) {
		 //TODO add to try catch to other updates
		if (isSuccessful) {
			mWasNendoroidUpdated = true;
			mNendoroid.refreshFromDatabase();
			
			int mainStatusReference = Nendoroid.STATUS_INTERESTED;
			if (mNendoroidListType == NendoroidListAdapater.TYPE_LOOKING_FOR) {
				mainStatusReference = Nendoroid.STATUS_LOOKING_FOR;
			} else if (mNendoroidListType == NendoroidListAdapater.TYPE_ACQUIRED) {
				mainStatusReference = Nendoroid.STATUS_ACQUIRED;
			} 
			
			String statusDetails = "<b>" + getString(R.string.prefix_status) + "</b>: " 
					+ mNendoroid.getDescriptiveStatus(mainStatusReference);
			mStatusTextView.setText(Html.fromHtml(statusDetails));
			
			DialogUtilities.showToast(getActivity(), getActivity().getString(
					R.string.toast_message_nendoroid_update_success, 
					mNendoroid.getDescriptiveName()));
		}
		
		//TODO pop the exception message from the task
		//DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(
				//R.string.dialog_message_error_updating_nendoroid_exception_message, 
				//mNendoroid.getDescriptiveName(), e.getMessage()));
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		if (resultCode == DownloadNendoroidImagesIntentService.STATUS_RUNNING) {
			String progressMessage = resultData.getString(DownloadNendoroidImagesIntentService.BUNDLE_KEY_MESSAGE);
			if (progressMessage == null || progressMessage.equals("")) {
				progressMessage = getActivity().getString(R.string.progress_message_starting_download);
			}
			int progress = resultData.getInt(DownloadNendoroidImagesIntentService.BUNDLE_KEY_PROGRESS);
			int max = resultData.getInt(DownloadNendoroidImagesIntentService.BUNDLE_KEY_MAX);
			
			if (mDownloadImagesProgressDialog != null) {
				mDownloadImagesProgressDialog.setMessage(progressMessage);
				mDownloadImagesProgressDialog.setProgress(progress);
				mDownloadImagesProgressDialog.setMax(max);
			}
			
			if (mMenu != null) {
				MenuItem downloadOtherImagesMenuItem = mMenu.findItem(R.id.menu_download_other_images);
				downloadOtherImagesMenuItem.setTitle(getActivity()
						.getString(R.string.menu_downloading_other_image, StringTransformations.convertNumberToOrdinal(progress)));
			}
			
		} else if (resultCode == DownloadNendoroidImagesIntentService.STATUS_FINISHED
				|| resultCode == DownloadNendoroidImagesIntentService.STATUS_ERROR) {
			if (mDownloadImagesProgressDialog != null && mDownloadImagesProgressDialog.isShowing()) {
				mDownloadImagesProgressDialog.dismiss();
			}
			
			if (mMenu != null) { //TODO if null, do this at a later time; also consider doing more tasks at once
				MenuItem downloadOtherImagesMenuItem = mMenu.findItem(R.id.menu_download_other_images);
				downloadOtherImagesMenuItem.setTitle(R.string.menu_download_other_images);
			}
			
			if (resultCode == DownloadNendoroidImagesIntentService.STATUS_FINISHED) {
				DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_nendoroid_downloaded_all_images));
				checkAndHideDownloadOtherImagesMenuItem();
				
				mNendoroid.refreshFromDatabase();
				mImagePagerAdapter.setNendoroid(mNendoroid);
				
				mImagePagerAdapter.buildImageList();
				mImagePagerAdapter.notifyDataSetChanged();
			} else if (resultCode == DownloadNendoroidImagesIntentService.STATUS_ERROR) {
				DialogUtilities.showBasicErrorDialog(getActivity(), 
						getActivity().getString(R.string.dialog_message_error_occurred_exception_message, 
						resultData.getString(DownloadNendoroidImagesIntentService.BUNDLE_KEY_EXCEPTION)));
			}
		}
	}
}
