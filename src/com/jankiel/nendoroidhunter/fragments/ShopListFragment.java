package com.jankiel.nendoroidhunter.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.activities.MainActivity;
import com.jankiel.nendoroidhunter.activities.PreferencesActivity;
import com.jankiel.nendoroidhunter.adapters.ShopListAdapater;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Shop;
import com.jankiel.nendoroidhunter.utils.Utils;

public class ShopListFragment extends Fragment {
	
	public static final String EXTRA_KEY_SHOULD_ADD_SHOP = "com.jankiel.nendoroidhunter.EXTRA_KEY_SHOULD_ADD_SHOP";

	private boolean mShouldAddShop;
	
	private DatabaseManager mDbManager;

	private List<Shop> mOriginalShopList;
	private List<Shop> mShopList;
	
	private Menu mMenu;
	
	private ListView mShopListView;
	private TextView mEmptyTextView;

	@SuppressWarnings("unused")
	private SharedPreferences mSharedPreferences;
	private ShopListAdapater mShopListAdapter;
	
	public ShopListFragment(boolean shouldAddShop) {
		this.mShouldAddShop = shouldAddShop;
		mOriginalShopList = new ArrayList<Shop>();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		mShopListAdapter = new ShopListAdapater(getActivity(), mOriginalShopList);

		mDbManager = DatabaseManager.getInstance(getActivity());
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		
		getActivity().setTitle(getActivity().getString(R.string.title_shop_list)); //TODO move to strings
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_shop_list, container, false);
		
		mEmptyTextView = (TextView) rootView.findViewById(R.id.text_view_empty);
		mEmptyTextView.setText(R.string.list_view_loading);
		
		mShopListView = (ListView) rootView.findViewById(R.id.list_view_shop);
		mShopListView.setAdapter(mShopListAdapter);
		
		//TODO implement long click/selection
		//TODO implement default action preference: open map, open details, open website, open facebook, dial
		mShopListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				Shop shop = mShopList.get(position);
				
				if (shop.hasValidCoordinates()) {
					double latitude = shop.getLatitude();
					double longitude = shop.getLongitude();
					Uri shopLatLngUri = Uri.parse("geo:" + latitude + "," + longitude + "");
					
					Intent mapIntent = new Intent(Intent.ACTION_VIEW, shopLatLngUri);
					if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
						startActivity(mapIntent);
					} else {
						DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_no_map_app,
								shop.getName()));
					}
				} else {
					String website = shop.getWebsiteOrFacebook();
					if (website != null && !website.equals("")) {
						Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
						startActivity(websiteIntent);
					} else {
						DialogUtilities.showToast(getActivity(), 
								getActivity().getString(R.string.toast_message_no_valid_coordinates_website_or_facebook,
								shop.getName()));
					}
				}
			}
		});
		
		mShopListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				Shop shop = mShopList.get(position);
				showShopActionsDialog(shop, position);
				return false;
			}
		});
		
		//TODO move to async task if seems slow
		mEmptyTextView.setText(R.string.list_view_loading);
		mOriginalShopList = mDbManager.getAllShopsWithConstraint(null, Shop.FIELD_NAME);
		
		mShopList = mOriginalShopList;
		refreshShopListAdapater();
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (mShouldAddShop && mOriginalShopList.size() == 0) {
			Dialog dialog = DialogUtilities.buildDialog(getActivity(), 
					getActivity().getString(R.string.dialog_title_empty_shop_list), 
					getActivity().getString(R.string.dialog_message_shop_list_empty_create_or_import), 
					getActivity().getString(R.string.dialog_button_create), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							showCreateShopWizardDialog();
						}
					}, 
					getActivity().getString(R.string.dialog_button_import), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent preferencesIntent = new Intent(getActivity(), PreferencesActivity.class);
							
							getActivity().startActivityForResult(preferencesIntent, NendoroidDetailFragment.REQUEST_ADD_SHOP);
						}
					});
			
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_shop_list, menu);
		
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
		
		searchView.setOnQueryTextListener(new OnQueryTextListener() { 

			@Override 
			public boolean onQueryTextChange(String query) {
				filter(query);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				filter(query);
				return false;
			}
		});
		
		mMenu = menu;

	    super.onCreateOptionsMenu(menu, inflater);
	}
	
	public boolean handleKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_MENU:
			if (mMenu != null) {
				mMenu.performIdentifierAction(R.id.menu_overflow, 0);
			}
			break;
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_create_shop:
			showCreateShopWizardDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void filter(String query) {
		if (query == null || query.equals("")) {
			mShopList = mOriginalShopList;
			refreshShopListAdapater();
			return;
		}
		
		mShopList = new ArrayList<Shop>();
		
		query = query.toLowerCase();

		for (Shop shop : mOriginalShopList) {
			String shopName = shop.getName().toLowerCase();
			if (shopName.contains(query)) {
				mShopList.add(shop);
			}
		}
		
		refreshShopListAdapater();
	}
	
	private void refreshShopListAdapater() {
		if (mShopList != null && mShopList.size() > 0) {
			mShopListView.setVisibility(View.VISIBLE);
		} else {
			mShopListView.setVisibility(View.GONE);
			mEmptyTextView.setText(R.string.list_view_empty);
		}
		
		mShopListAdapter.setList(mShopList);
		mShopListAdapter.notifyDataSetChanged();
	}
	
	public void showCreateShopWizardDialog() {
		DialogUtilities.showDialogWithEditText(getActivity(), getActivity().getString(R.string.dialog_title_shop_name_entry), 
				"Shop", 
				getActivity().getString(R.string.dialog_button_create), new DialogUtilities.OnEditTextDialogClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, EditText editText) {
						
						//TODO get lat and long somewhere
						Shop shop = new Shop(getActivity(), editText.getText().toString());
						try {
							boolean isSuccessful = mDbManager.insertShop(shop) != -1;

							if (isSuccessful) {
								//FIXME move to async
								mOriginalShopList = mDbManager.getAllShopsWithConstraint(null, Shop.FIELD_NAME);
								mShopList = mOriginalShopList;
								refreshShopListAdapater();
								
								DialogUtilities.showToast(getActivity(), getActivity().getString(
										R.string.toast_message_shop_created_success, 
										shop.getName()));
							} else {
								DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(
										R.string.dialog_message_error_creating_shop, 
										shop.getName()));
							}
						} catch (Exception e) {
							DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(
									R.string.dialog_message_error_creating_shop_exception_message, 
									shop.getName(), e.getMessage()));
							e.printStackTrace();
						}
					}
				}, getActivity().getString(R.string.dialog_button_cancel));
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.e("jankieldev", "inResult");
		Log.e("jankieldev", "requestCode: " + requestCode);
		Log.e("jankieldev", "resultCode: " + resultCode);
		if (requestCode == NendoroidDetailFragment.REQUEST_ADD_SHOP
				|| resultCode == MainActivity.RESULT_DB_UPDATED) {
			mOriginalShopList = mDbManager.getAllShopsWithConstraint(null, Shop.FIELD_NAME);
			mShopList = mOriginalShopList;
			refreshShopListAdapater();
		}
	}
	
	private void showShopActionsDialog(final Shop shop, int index) {

		final String[] actions = {
			getActivity().getString(R.string.dialog_item_shop_action_open_map),
			getActivity().getString(R.string.dialog_item_shop_action_open_website),
			getActivity().getString(R.string.dialog_item_shop_action_open_facebook),
			//getActivity().getString(R.string.dialog_item_shop_action_edit_shop),
			getActivity().getString(R.string.dialog_item_shop_action_edit_latlng),
			getActivity().getString(R.string.dialog_item_shop_action_edit_website),
			getActivity().getString(R.string.dialog_item_shop_action_edit_facebook),
			getActivity().getString(R.string.dialog_item_shop_action_delete_shop)
		};
		
		if (!shop.hasValidCoordinates()) {
			actions[3] = getActivity().getString(R.string.dialog_item_shop_action_add_latlng);
		}
		if (shop.getWebsite() == null || shop.getWebsite().equals("")) {
			actions[4] = getActivity().getString(R.string.dialog_item_shop_action_add_website);
		}
		if (shop.getFacebook() == null || shop.getFacebook().equals("")) {
			actions[5] = getActivity().getString(R.string.dialog_item_shop_action_add_facebook);
		}
		
		DialogUtilities.showDialogWithItems(getActivity(), getActivity().getString(R.string.dialog_title_shop_action_selection), 
				actions, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == 0) {
							if (shop.hasValidCoordinates()) {
								double latitude = shop.getLatitude();
								double longitude = shop.getLongitude();
								Uri shopLatLngUri = Uri.parse("geo:" + latitude + "," + longitude + "");
								
								Intent mapIntent = new Intent(Intent.ACTION_VIEW, shopLatLngUri);
								if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
									startActivity(mapIntent);
								} else {
									DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_no_map_app,
											shop.getName()));
								}
							} else {
								DialogUtilities.showToast(getActivity(), 
										getActivity().getString(R.string.toast_message_no_valid_coordinates,
												shop.getName()));
							}
						} else if (which == 1) {
							String website = shop.getWebsite();
							if (website != null && !website.equals("")) {
								Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
								startActivity(websiteIntent);
							} else {
								DialogUtilities.showToast(getActivity(), 
										getActivity().getString(R.string.toast_message_no_website,
												shop.getName()));
							}
						} else if (which == 2) {
							String facebook = shop.getFacebook();
							if (facebook != null && !facebook.equals("")) {
								Intent facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebook));
								startActivity(facebookIntent);
							} else {
								DialogUtilities.showToast(getActivity(), 
										getActivity().getString(R.string.toast_message_no_facebook,
												shop.getName()));
							}
						} else if (which == 3) {
							DialogUtilities.showDialogWithEditText(getActivity(), 
									getActivity().getString(R.string.dialog_title_latlng_entry), 
									shop.getLatitude() + "," + shop.getLongitude(), 
									getActivity().getString(R.string.dialog_button_save), 
									new DialogUtilities.OnEditTextDialogClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which, EditText editText) {
											String latLngString = editText.getText().toString();
											
											String[] latLngStringComponents = latLngString.split(",");
											if (latLngStringComponents.length != 2) {
												DialogUtilities.showBasicErrorDialog(getActivity(), 
														getActivity().getString(R.string.toast_message_latlng_invalid));
											} else {
												double latitude = 0.0;
												double longitude = 0.0;
												
												try {
													latitude = Double.parseDouble(latLngStringComponents[0].trim());
													longitude = Double.parseDouble(latLngStringComponents[1].trim());
												} catch (Exception e) {
													DialogUtilities.showBasicErrorDialog(getActivity(), getString(
															R.string.toast_message_latlng_invalid));
													e.printStackTrace();
													return; //XXX
												}
												
												if (latitude == 0.0 || longitude == 0.0) {
													DialogUtilities.showBasicErrorDialog(getActivity(), getString(
															R.string.toast_message_latlng_invalid));
												} else {
													shop.setLatitude(latitude);
													shop.setLongitude(longitude);
													
													try {
														boolean isSuccessful = mDbManager.updateShop(shop);
														if (isSuccessful) {
															DialogUtilities.showToast(getActivity(), getActivity().getString(
																	R.string.toast_message_shop_update_success, shop.getName()));
														} else {
															DialogUtilities.showBasicErrorDialog(getActivity(), getString(
																	R.string.dialog_message_error_updating_shop, shop.getName()));
														}
														
													} catch (Exception e) {
														DialogUtilities.showBasicErrorDialog(getActivity(), getString(
																R.string.dialog_message_error_updating_shop_exception_message, shop.getName(),
																e.getMessage()));
														e.printStackTrace();
													}
												}
												
												
											}
										}
									}, getActivity().getString(R.string.dialog_button_cancel));
						} else if (which == 4) {
							DialogUtilities.showDialogWithEditText(getActivity(), 
									getActivity().getString(R.string.dialog_title_website_entry), shop.getWebsite(), 
									getActivity().getString(R.string.dialog_button_save), 
									new DialogUtilities.OnEditTextDialogClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which, EditText editText) {
											String websiteUrl = editText.getText().toString();
											websiteUrl = websiteUrl.trim();
											if (!websiteUrl.startsWith("http://") || !websiteUrl.startsWith("https://")) {
												websiteUrl = "http://" + websiteUrl;
											}
												
											if (!Utils.isValidURL(websiteUrl)) {
												DialogUtilities.showBasicErrorDialog(getActivity(), getString(
														R.string.toast_message_website_invalid));
											} else {
												shop.setWebsite(websiteUrl);
												
												try {
													boolean isSuccessful = mDbManager.updateShop(shop);
													if (isSuccessful) {
														DialogUtilities.showToast(getActivity(), getActivity().getString(
																R.string.toast_message_shop_update_success, shop.getName()));
													} else {
														DialogUtilities.showBasicErrorDialog(getActivity(), getString(
																R.string.dialog_message_error_updating_shop, shop.getName()));
													}
													
												} catch (Exception e) {
													DialogUtilities.showBasicErrorDialog(getActivity(), getString(
															R.string.dialog_message_error_updating_shop_exception_message, shop.getName(),
															e.getMessage()));
													e.printStackTrace();
												}
											}
										}
									}, getActivity().getString(R.string.dialog_button_cancel));
						} else if (which == 5) {
							DialogUtilities.showDialogWithEditText(getActivity(), 
									getActivity().getString(R.string.dialog_title_facebook_entry), shop.getFacebook(), 
									getActivity().getString(R.string.dialog_button_save), 
									new DialogUtilities.OnEditTextDialogClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which, EditText editText) {
											String facebookUrl = editText.getText().toString();
											facebookUrl = facebookUrl.trim();
											if (!facebookUrl.startsWith("http://") || !facebookUrl.startsWith("https://")) {
												facebookUrl = "http://" + facebookUrl;
											}
												
											if (!Utils.isValidFacebookURL(facebookUrl)) {
												DialogUtilities.showBasicErrorDialog(getActivity(), getString(
														R.string.toast_message_facebook_invalid));
											} else {
												shop.setFacebook(facebookUrl);
												
												try {
													boolean isSuccessful = mDbManager.updateShop(shop);
													if (isSuccessful) {
														DialogUtilities.showToast(getActivity(), getActivity().getString(
																R.string.toast_message_shop_update_success, shop.getName()));
													} else {
														DialogUtilities.showBasicErrorDialog(getActivity(), getString(
																R.string.dialog_message_error_updating_shop, shop.getName()));
													}
													
												} catch (Exception e) {
													DialogUtilities.showBasicErrorDialog(getActivity(), getString(
															R.string.dialog_message_error_updating_shop_exception_message, shop.getName(),
															e.getMessage()));
													e.printStackTrace();
												}
											}
										}
									}, getActivity().getString(R.string.dialog_button_cancel));
						} else if (which == 6) {
							DialogUtilities.showDialog(getActivity(), getActivity().getString(R.string.dialog_title_confirmation), 
									getActivity().getString(R.string.dialog_message_delete_shop_confirmation,
									shop.getName()), 
									getActivity().getString(R.string.dialog_button_delete), new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											try {
												boolean isSuccesful = mDbManager.hardDeleteShop(shop);
												
												if (isSuccesful) {
													mShopList.remove(shop);
													refreshShopListAdapater();
													
													DialogUtilities.showToast(getActivity(), getActivity().getString(
															R.string.toast_message_shop_deleted_success, 
															shop.getName()));
												} else {
													DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(
															R.string.dialog_message_error_deleting_shop, 
															shop.getName()));
												}
											} catch (Exception e) {
												DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(
														R.string.dialog_message_error_deleting_shop_exception_message, 
														shop.getName(), e.getMessage()));
												e.printStackTrace();
											}
										}
									}, getActivity().getString(R.string.dialog_button_cancel));
						}
					}
				}, getActivity().getString(R.string.dialog_button_cancel));
	}
}
