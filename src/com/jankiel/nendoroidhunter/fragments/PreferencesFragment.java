package com.jankiel.nendoroidhunter.fragments;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.EditText;

import com.jankiel.genlibs.utils.FileOperations;
import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.activities.AboutActivity;
import com.jankiel.nendoroidhunter.activities.HelpActivity;
import com.jankiel.nendoroidhunter.activities.LegalStuffsActivity;
import com.jankiel.nendoroidhunter.activities.MainActivity;
import com.jankiel.nendoroidhunter.activities.ShopListActivity;
import com.jankiel.nendoroidhunter.asynctasks.DownloadNendoroidImagesIntentService;
import com.jankiel.nendoroidhunter.asynctasks.DownloadNendoroidImagesResultReceiver;
import com.jankiel.nendoroidhunter.asynctasks.DownloadNendoroidImagesResultReceiver.Receiver;
import com.jankiel.nendoroidhunter.asynctasks.ExportToFileTask;
import com.jankiel.nendoroidhunter.asynctasks.GetAllNendoroidsFromDatabaseTask;
import com.jankiel.nendoroidhunter.asynctasks.ImportFromFileTask;
import com.jankiel.nendoroidhunter.asynctasks.InsertIntoDatabaseTask;
import com.jankiel.nendoroidhunter.asynctasks.LoadInitialDatabaseTask;
import com.jankiel.nendoroidhunter.asynctasks.LoadInitialDatabaseTask.OnInitialDatabaseLoadCompleteListener;
import com.jankiel.nendoroidhunter.asynctasks.ScrapeNendoroidDataTask;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.utils.AppConstant;
import com.jankiel.nendoroidhunter.utils.Utils;

public class PreferencesFragment extends PreferenceFragment implements 
				ScrapeNendoroidDataTask.OnNendoroidDataScrapeCompleteListener,
				InsertIntoDatabaseTask.OnDatabaseInsertCompleteListener,
				ExportToFileTask.OnExportToFileCompleteListener, 
				ImportFromFileTask.OnImportFromFileCompleteListener, 
				OnInitialDatabaseLoadCompleteListener, Receiver {
	
	private boolean mDbWasUpdated;
	
	private final int NO_REQUEST = -100;
	private int mRequestCode;

	private DatabaseManager mDbManager;
	
	private SharedPreferences mSharedPreferences;
	
	private PreferenceCategory mNendoroidListPreferenceCategory;
	
	private Preference mImportNendoroidListPreference;
	private Preference mExportNendoroidListPreference;
	private Preference mUpdateNendoroidListPreference;
	private Preference mDownloadImagesPreference;
	
	private PreferenceCategory mShopListPreferenceCategory;
	
	private Preference mOpenShopListActivityPreference;
	private Preference mImportShopListPreference;
	private Preference mExportShopListPreference;
	
	private PreferenceCategory mMiscellaneousPreferenceCategory;
	
	private CheckBoxPreference mIsJankielPreference;
	private Preference mOpenHelpActivityPreference;
	private Preference mOpenAboutActivityPreference;
	private Preference mOpenLegalStuffsActivityPreference;
	
	private DownloadNendoroidImagesResultReceiver mResultReceiver;
	private ProgressDialog mDownloadImagesProgressDialog;
	
	private final String JANKIEL_UNLOCKER_KEY = "100100110110";
	private String mJankielUnlockerKey;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.fragment_preferences);
		
		mDbManager = DatabaseManager.getInstance(getActivity());
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		
		mRequestCode = NO_REQUEST;
		mJankielUnlockerKey = "";
		
		mResultReceiver = DownloadNendoroidImagesResultReceiver.getInstance(new Handler());
		
		mNendoroidListPreferenceCategory = (PreferenceCategory) findPreference(AppConstant.PREFERENCE_CATEGORY_KEY_NENDOROID_LIST);
		
		mImportNendoroidListPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_IMPORT_NENDOROID_LIST);
		mImportNendoroidListPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				DialogUtilities.showDialog(getActivity(), 
						getActivity().getString(R.string.dialog_title_confirmation), 
						getActivity().getString(R.string.dialog_message_reset_db_and_import_confirmation), 
						getActivity().getString(R.string.dialog_button_reset_and_import), new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//TODO call db delete nendos, nendos in shop and images
								showImportFileSelectionDialog(AppConstant.EXPECTATION_NENDOROID);
							}
						}, getActivity().getString(R.string.dialog_button_cancel));
				return false;
			}
		});
		
		mExportNendoroidListPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_EXPORT_NENDOROID_LIST);
		mExportNendoroidListPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				showExportFileWizardDialog(AppConstant.EXPECTATION_NENDOROID);
				return false;
			}
		});
		
		mUpdateNendoroidListPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_UPDATE_NENDOROID_LIST);
		mUpdateNendoroidListPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				executeScrapeNendoroidDataTask(true);
				return false;
			}
		});
		
		//FIXME welcome dialog gets popped when back while dling
		//steps
		//initial install
		//while dling images, update list,
		//dunno if related, open shop list
		//back back to main
		mDownloadImagesPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_DOWNLOAD_IMAGES);
		mDownloadImagesPreference.setOnPreferenceClickListener(mDownloadImagesPreferenceClickListener);
		
		mShopListPreferenceCategory = (PreferenceCategory) findPreference(AppConstant.PREFERENCE_CATEGORY_KEY_SHOP_LIST);

		//FIXME add change user currency only if isJankiel
		//TODO add option to automatically convert gsc price to peso
		mOpenShopListActivityPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_OPEN_SHOP_LIST_ACTIVITY);
		mOpenShopListActivityPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				getActivity().startActivity(new Intent(getActivity(), ShopListActivity.class));
				return false;
			}
		});
		
		mImportShopListPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_IMPORT_SHOP_LIST);
		mImportShopListPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// TODO Auto-generated method stub
				showImportFileSelectionDialog(AppConstant.EXPECTATION_SHOP);
				return false;
			}
		});
		
		mExportShopListPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_EXPORT_SHOP_LIST);
		mExportShopListPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// TODO Auto-generated method stub
				showExportFileWizardDialog(AppConstant.EXPECTATION_SHOP);
				return false;
			}
		});
		
		mMiscellaneousPreferenceCategory = (PreferenceCategory) findPreference(AppConstant.PREFERENCE_CATEGORY_KEY_MISCELLANEOUS);
		
		mIsJankielPreference = (CheckBoxPreference) findPreference(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL);
		mIsJankielPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				showJankielAuthenticationDialog();
				return false;
			}
		});
		
		mOpenHelpActivityPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_OPEN_HELP_ACTIVITY);
		mOpenHelpActivityPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				mJankielUnlockerKey += "0";
				if (mJankielUnlockerKey.startsWith(JANKIEL_UNLOCKER_KEY)) {
					mMiscellaneousPreferenceCategory.addPreference(mIsJankielPreference);
				}
				getActivity().startActivity(new Intent(getActivity(), HelpActivity.class));
				return false;
			}
		});
		
		mOpenAboutActivityPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_OPEN_ABOUT_ACTIVITY);
		mOpenAboutActivityPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				mJankielUnlockerKey += "1";
				if (mJankielUnlockerKey.startsWith(JANKIEL_UNLOCKER_KEY)) {
					mMiscellaneousPreferenceCategory.addPreference(mIsJankielPreference);
				}
				getActivity().startActivity(new Intent(getActivity(), AboutActivity.class));				
				return false;
			}
		});
		
		mOpenLegalStuffsActivityPreference = findPreference(AppConstant.SHARED_PREFERENCES_KEY_OPEN_LEGAL_STUFFS_ACTIVITY);
		mOpenLegalStuffsActivityPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				getActivity().startActivity(new Intent(getActivity(), LegalStuffsActivity.class));
				return false;
			}
		});
		
		if (getActivity().getCallingActivity() != null) {
			ComponentName componentName = getActivity().getCallingActivity();
			
			//FIXME a bit kludgy but...
			if (componentName.getShortClassName().equals(".activities.NendoroidDetailActivity")) {
				mImportNendoroidListPreference.setEnabled(false);
			}
		}
		
		mDownloadImagesProgressDialog = new ProgressDialog(getActivity());
		mDownloadImagesProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mDownloadImagesProgressDialog.setMax(600);
		mDownloadImagesProgressDialog.setCancelable(true); //TODO maybe add a stop/cancel dialog
		mDownloadImagesProgressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, 
				getActivity().getString(R.string.dialog_button_run_in_background), 
				new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mDownloadImagesProgressDialog.dismiss();
				DialogUtilities.showToast(getActivity(), getActivity()
						.getString((R.string.toast_message_running_in_background)));
			}
		});
		
		showHidePreferencesIfJankiel();
	}
	
	@Override
	public void onStart() {
		super.onStart();
				
		if (mRequestCode  == NO_REQUEST && getActivity().getCallingActivity() != null) {
			//ComponentName component = getActivity().getCallingActivity();
			Intent intent = getActivity().getIntent();
			
			mRequestCode = intent.getIntExtra(MainActivity.EXTRA_KEY_REQUEST_CODE, NO_REQUEST);
			
			if (mRequestCode != NO_REQUEST) {
				
				if (mRequestCode == MainActivity.REQUEST_SCRAPE_NENDOROID_LIST) {
					executeScrapeNendoroidDataTask(false);
				} else if (mRequestCode == MainActivity.REQUEST_IMPORT_NENDOROID_LIST) {
					showImportFileSelectionDialog(AppConstant.EXPECTATION_NENDOROID);
				} else if (mRequestCode == NendoroidDetailFragment.REQUEST_ADD_SHOP) {
					showImportFileSelectionDialog(AppConstant.EXPECTATION_SHOP);
				} else if (mRequestCode == MainActivity.REQUEST_LOAD_INITIAL_DATABASE) {
					executeLoadInitialDatabaseTask();
				}
			}
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		mResultReceiver.setReceiver(this);
		
		if (DownloadNendoroidImagesIntentService.isRunning(getActivity())) {
			mDownloadImagesPreference.setTitle(R.string.menu_downloading_nendoroid_list);
			mDownloadImagesPreference.setOnPreferenceClickListener(mCurrentlyDownloadingImagesPreferenceClickListener);
		}
	}
	
	@Override
	public void onStop() {
		mDownloadImagesPreference.setTitle(R.string.menu_download_nendoroid_list); //TODO put to a method maybe?
		mDownloadImagesPreference.setSummary(R.string.menu_summary_download_nendoroid_list);
		mDownloadImagesPreference.setOnPreferenceClickListener(mDownloadImagesPreferenceClickListener);
		
		mResultReceiver.setReceiver(null);
		
		super.onStop();
	};
	
	public boolean wasDbUpdated() {
		return mDbWasUpdated;
	}
	
	public void showImportFileSelectionDialog(final int listToExpect) {
		DialogUtilities.showFileListDialog(getActivity(), 
				getActivity().getString(R.string.dialog_title_file_selection), 
				Environment.getExternalStorageDirectory().getAbsolutePath(), 
				new FilenameFilter() {
					
					@Override
					public boolean accept(File directory, String filename) {
						File file = new File(directory.getAbsolutePath() + File.separator + filename);
						
						if (file.isDirectory()) {
							return true;
						}
						String fileformat = FileOperations.getFileFormat(file);
						if (fileformat.equals(AppConstant.FILE_FORMAT_XML)
								|| fileformat.equals(AppConstant.FILE_FORMAT_JSON)
								/*|| fileformat.equals(AppConstant.FILE_FORMAT_CSV)*/
								|| fileformat.equals(AppConstant.FILE_FORMAT_KML)) {
							return true;
						}
						return false;
					}
				}, new DialogUtilities.OnFileListDialogClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, File file) {
						executeImportFromFileTask(file, listToExpect);
					}
				}, null, null, null);
	}
	
	private void showExportFileWizardDialog(final int listToExpect) {
		final String[] fileTypes = {
				AppConstant.FILE_FORMAT_XML, 
				AppConstant.FILE_FORMAT_JSON, 
				AppConstant.FILE_FORMAT_CSV
		};
		
		DialogUtilities.showDialogWithItems(getActivity(), 
				getActivity().getString(R.string.dialog_title_file_type_selection), 
				fileTypes, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int index) {
						final String fileFormat = fileTypes[index];
						
						DialogUtilities.showFileListDialog(getActivity(), getActivity().getString(R.string.dialog_title_directory_selection), 
								Environment.getExternalStorageDirectory().getAbsolutePath(), 
								new FilenameFilter() {
									public boolean accept(File directory, String filename) {
										File file = new File(directory.getAbsolutePath() + File.separator + filename);
										return file.isDirectory();
									}
								}, null, null, getActivity().getString(R.string.dialog_button_select),
								new DialogUtilities.OnFileListDialogClickListener() {
							
									@Override
									public void onClick(DialogInterface dialog, int which, final File currentDirectory) {
										
										DialogUtilities.showDialogWithEditText(getActivity(), 
												getActivity().getString(R.string.dialog_title_filename_entry), "Filename", 
												getActivity().getString(R.string.dialog_button_export), new DialogUtilities.OnEditTextDialogClickListener() {
													
													@Override
													public void onClick(DialogInterface dialog, int which, EditText editText) {
														String filename = editText.getText().toString();
														if (filename.endsWith("." + fileFormat)) {
															filename = filename.substring(0, filename.length() - ("." + fileFormat).length());
														}
														File file = new File(currentDirectory.getAbsolutePath() 
																		+ File.separator + filename + "." + fileFormat);

														executeExportToFileTask(null, file, listToExpect);
													}
												}, getActivity().getString(R.string.dialog_button_cancel));
										
									}
								});
					}
		}, getActivity().getString(R.string.dialog_button_cancel)); //TODO < Back instead of cancel 
	}
	
	private void showJankielAuthenticationDialog() {		
		//because this method is called after the pref change,
		//we negate the saved prefs here to get the previous value
		boolean isJankiel = !mSharedPreferences.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL, false);
		mIsJankielPreference.setChecked(isJankiel);
		
		if (!isJankiel) {
			DialogUtilities.showDialogWithEditText(getActivity(), 
					getActivity().getString(R.string.dialog_title_is_jankiel_password_entry), null, 
					getActivity().getString(R.string.dialog_button_authenticate), new DialogUtilities.OnEditTextDialogClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which, EditText editText) {
							String password = editText.getText().toString();
							
							try {
								password = Utils.hashString(password);
								if (password.equalsIgnoreCase(AppConstant.ADVANCED_USER_PASSWORD)) {
									DialogUtilities.showToast(getActivity(), 
											getActivity().getString(R.string.toast_message_is_jankiel_authenticated));
									
									mSharedPreferences.edit().putBoolean(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL, true)
										.commit();
									mIsJankielPreference.setChecked(true);
									showHidePreferencesIfJankiel();
									
								} else {
									DialogUtilities.showBasicErrorDialog(getActivity(), 
											getActivity().getString(R.string.dialog_message_error_incorrect_password));
								}
							} catch (Exception e) {
								DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(R.string.dialog_message_error_occurred_exception_message, 
										e.getMessage()));
							}
							
						}
					}, getActivity().getString(R.string.dialog_button_cancel));
		} else {
			mSharedPreferences.edit().putBoolean(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL, false)
				.commit();
			mIsJankielPreference.setChecked(false);
			showHidePreferencesIfJankiel();
		}
	}
	
	private void showHidePreferencesIfJankiel() {
		boolean isJankiel = 
				mSharedPreferences.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL, false);
		
		if (isJankiel) {
			mNendoroidListPreferenceCategory.addPreference(mImportNendoroidListPreference);
			mNendoroidListPreferenceCategory.addPreference(mExportNendoroidListPreference);
			
			mShopListPreferenceCategory.addPreference(mExportShopListPreference);
		} else {
			mNendoroidListPreferenceCategory.removePreference(mImportNendoroidListPreference);
			mNendoroidListPreferenceCategory.removePreference(mExportNendoroidListPreference);
			
			mShopListPreferenceCategory.removePreference(mExportShopListPreference);
			
			mMiscellaneousPreferenceCategory.removePreference(mIsJankielPreference);
		}
	}
	
	public void executeImportFromFileTask(File file, int listToExpect) {
		ImportFromFileTask importFromFileTask = new ImportFromFileTask(getActivity(), file, listToExpect);
		importFromFileTask.setOnImportFromFileCompleteListener(this);
			
		importFromFileTask.execute();
	}
	
	@SuppressWarnings("rawtypes")
	public void executeExportToFileTask(List itemList, File file, int listToExpect) {
		ExportToFileTask exportToFileTask = 
				new ExportToFileTask(getActivity(), itemList, file, listToExpect);
		exportToFileTask.setOnExportToFileCompleteListener(this);
		
		if (itemList == null) {
			if (listToExpect == AppConstant.EXPECTATION_NENDOROID) {
				GetAllNendoroidsFromDatabaseTask getAllNendoroidsFromDatabaseTask =
						new GetAllNendoroidsFromDatabaseTask(getActivity(), mDbManager, exportToFileTask);
				getAllNendoroidsFromDatabaseTask.execute();
			} else if (listToExpect == AppConstant.EXPECTATION_SHOP) {
				itemList = mDbManager.getAllShops();
				
				exportToFileTask.setItemList(itemList);
				exportToFileTask.execute();
			}
		} else {
			exportToFileTask.execute();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void executeInsertIntoDatabaseTask(List itemList) {
		InsertIntoDatabaseTask insertIntoDatabaseTask = 
			new InsertIntoDatabaseTask(getActivity(), mDbManager, itemList);
		insertIntoDatabaseTask.setOnDatabaseInsertCompleteListener(this);
		
		insertIntoDatabaseTask.execute();
	}
	
	public void startDownloadNendoroidImagesIntentService(List<Nendoroid> nendoroidList, int imagesToDownload) {
		Intent downloadNendoroidImagesIntent = 
				new Intent(Intent.ACTION_SYNC, null, getActivity(), DownloadNendoroidImagesIntentService.class);
		
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_DOWNLOAD_NENDOROID_IMAGES_RESULT_RECEIVER, mResultReceiver);
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_DESTINATION_DIRECTORY, Environment.getExternalStorageDirectory().getAbsolutePath());
		downloadNendoroidImagesIntent
			.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_IMAGES_TO_DOWNLOAD, imagesToDownload);
		
		if (nendoroidList == null) {
			downloadNendoroidImagesIntent
				.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_GET_ALL_NENDOROIDS, true);
		} else {
			downloadNendoroidImagesIntent
				.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_GET_ALL_NENDOROIDS, false);
			
			int[] nendoroidIdArray = new int[nendoroidList.size()];
			
			for (int i = 0; i < nendoroidList.size(); i++) {
				nendoroidIdArray[i] = nendoroidList.get(i).getId();
			}
			
			downloadNendoroidImagesIntent
				.putExtra(DownloadNendoroidImagesIntentService.EXTRA_KEY_NENDOROID_ID_ARRAY, nendoroidIdArray);
		}
		
		getActivity().startService(downloadNendoroidImagesIntent);
		
		mDownloadImagesProgressDialog.setMessage(getActivity().getString(R.string.progress_message_starting_download));
		mDownloadImagesProgressDialog.show();
	}
	
	//TODO detect internet connection
	public void executeScrapeNendoroidDataTask(boolean isForUpdate) {
		ScrapeNendoroidDataTask scrapeNendoroidDataTask = new ScrapeNendoroidDataTask(getActivity());
		scrapeNendoroidDataTask.setOnNendoroidDataScrapeCompleteListener(this);
		
		if (isForUpdate) {
			GetAllNendoroidsFromDatabaseTask getAllNendoroidsFromDatabaseTask =
					new GetAllNendoroidsFromDatabaseTask(getActivity(), mDbManager, scrapeNendoroidDataTask);
			getAllNendoroidsFromDatabaseTask.execute();
		} else {
			scrapeNendoroidDataTask.execute();
		}
	}
	
	private void executeLoadInitialDatabaseTask() {
		LoadInitialDatabaseTask loadInitialDatabaseTask = 
				new LoadInitialDatabaseTask(getActivity(), mDbManager);
		loadInitialDatabaseTask.setOnInitialDatabaseLoadCompleteListener(this);
		
		loadInitialDatabaseTask.execute();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void onImportFromFileComplete(List itemList, boolean isSuccessful) {
		if (isSuccessful) {
			if (itemList != null && itemList.size() > 0) {
				DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_import_from_file_success, 
						itemList.size()));
				if (Nendoroid.isNendoroidList(itemList)) {
					mDbManager.emptyDatabase();
				}
				executeInsertIntoDatabaseTask(itemList);
			} else {
				DialogUtilities.showBasicErrorDialog(getActivity(), getActivity().getString(R.string.dialog_message_error_no_usable_data));
			}
		}
	}

	@Override
	public void onExportToFileComplete(File file, boolean isSuccessful) {
		if (isSuccessful) {
			DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_export_to_file_success, 
					file.getAbsolutePath()));
		}
	}

	//FIXME might crash here coz of Nendo <- Object cast
	@Override
	@SuppressWarnings("rawtypes")
	public void onDatabaseInsertComplete(final List itemList, boolean isSuccessful) {
		if (isSuccessful) {
			boolean hasLoadedInitialDatabase = 
					mSharedPreferences.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_HAS_LOADED_INITIAL_DATABASE, false);
			if (!hasLoadedInitialDatabase) {
				mSharedPreferences.edit().putBoolean(AppConstant.SHARED_PREFERENCES_KEY_HAS_LOADED_INITIAL_DATABASE, true)
					.commit();
			}
			
			mDbWasUpdated = true;
			
			DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_insert_to_database_success, 
					itemList.size()));
			
			if (Nendoroid.isNendoroidList(itemList) && !DownloadNendoroidImagesIntentService.isRunning(getActivity())) {
				final List<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();
				for (Object item : itemList) {
					nendoroidList.add((Nendoroid) item);
				}
				
				DialogUtilities.showDialog(getActivity(), 
						getActivity().getString(R.string.dialog_title_confirmation), 
						getActivity().getString(R.string.dialog_message_download_basic_images_confirmation),
						getActivity().getString(R.string.dialog_button_download), new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//FIXME didn't work on update
								startDownloadNendoroidImagesIntentService(nendoroidList, DownloadNendoroidImagesIntentService.BASIC_IMAGES);
							}
						}, 
						getActivity().getString(R.string.dialog_button_cancel));
			}
		}
	}

	@Override
	public void onNendoroidDataScrapeComplete(List<Nendoroid> nendoroidList, boolean isSuccessful) {
		if (isSuccessful) {
			if (nendoroidList != null && nendoroidList.size() > 0) {
				executeInsertIntoDatabaseTask(nendoroidList);
			} else {
				DialogUtilities.showBasicInfoDialog(getActivity(), 
						getActivity().getString(R.string.dialog_message_list_is_up_to_date));
			}
		}
	}

	@Override
	public void onInitialDatabaseLoadComplete(boolean isSuccessful) {
		if (isSuccessful) {
			mSharedPreferences.edit().putBoolean(AppConstant.SHARED_PREFERENCES_KEY_HAS_LOADED_INITIAL_DATABASE, true)
				.commit();
			
			mDbWasUpdated = true;
			
			DialogUtilities.showToast(getActivity(), getActivity().getString(R.string.toast_message_initial_database_load_complete));

			if (!DownloadNendoroidImagesIntentService.isRunning(getActivity())) {
				DialogUtilities.showDialog(getActivity(), 
						getActivity().getString(R.string.dialog_title_confirmation), 
						getActivity().getString(R.string.dialog_message_download_basic_images_confirmation),
						getActivity().getString(R.string.dialog_button_download), new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								startDownloadNendoroidImagesIntentService(null, DownloadNendoroidImagesIntentService.BASIC_IMAGES);
							}
						}, 
						getActivity().getString(R.string.dialog_button_cancel));
			}
		}
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		if (resultCode == DownloadNendoroidImagesIntentService.STATUS_RUNNING) {
			String progressMessage = resultData.getString(DownloadNendoroidImagesIntentService.BUNDLE_KEY_MESSAGE);
			if (progressMessage == null || progressMessage.equals("")) {
				progressMessage = getActivity().getString(R.string.progress_message_starting_download);
			}
			int progress = resultData.getInt(DownloadNendoroidImagesIntentService.BUNDLE_KEY_PROGRESS);
			int max = resultData.getInt(DownloadNendoroidImagesIntentService.BUNDLE_KEY_MAX);
			
			if (mDownloadImagesProgressDialog != null) {
				mDownloadImagesProgressDialog.setMessage(progressMessage);
				mDownloadImagesProgressDialog.setProgress(progress);
				mDownloadImagesProgressDialog.setMax(max);
			}
			
			mDownloadImagesPreference.setTitle(R.string.menu_downloading_nendoroid_list);
			mDownloadImagesPreference.setSummary(getActivity()
					.getString(R.string.menu_summary_downloading_nendoroid_list, 
							progress, max));
			mDownloadImagesPreference.setOnPreferenceClickListener(mCurrentlyDownloadingImagesPreferenceClickListener);
			
		} else if (resultCode == DownloadNendoroidImagesIntentService.STATUS_FINISHED
				|| resultCode == DownloadNendoroidImagesIntentService.STATUS_ERROR) {
			if (mDownloadImagesProgressDialog != null && mDownloadImagesProgressDialog.isShowing()) {
				mDownloadImagesProgressDialog.dismiss();
			}
			
			mDownloadImagesPreference.setTitle(R.string.menu_download_nendoroid_list);
			mDownloadImagesPreference.setSummary(R.string.menu_summary_download_nendoroid_list);
			mDownloadImagesPreference.setOnPreferenceClickListener(mDownloadImagesPreferenceClickListener);
			
			if (resultCode == DownloadNendoroidImagesIntentService.STATUS_FINISHED) {
				mDbWasUpdated = true;
				int imagesDownloaded = resultData.getInt(DownloadNendoroidImagesIntentService.BUNDLE_KEY_IMAGES_TO_DOWNLOAD, -1); 
				
				if (imagesDownloaded != -1) {
					switch(imagesDownloaded) {
					case DownloadNendoroidImagesIntentService.BASIC_IMAGES:
						DialogUtilities.showBasicInfoDialog(getActivity(), 
								getActivity().getString(R.string.dialog_message_basic_images_download_success));
						break;
					case DownloadNendoroidImagesIntentService.THUMB_IMAGE:
						DialogUtilities.showBasicInfoDialog(getActivity(), 
								getActivity().getString(R.string.dialog_message_thumb_image_download_success));
						break;
					case DownloadNendoroidImagesIntentService.MAIN_IMAGE:
						DialogUtilities.showBasicInfoDialog(getActivity(), 
								getActivity().getString(R.string.dialog_message_main_image_download_success));
						break;
					case DownloadNendoroidImagesIntentService.OTHER_IMAGES:
						DialogUtilities.showBasicInfoDialog(getActivity(), 
								getActivity().getString(R.string.dialog_message_other_images_download_success));
						break;
					default:
						//
						break;
					}
				}
				
			} else if (resultCode == DownloadNendoroidImagesIntentService.STATUS_ERROR) {
				DialogUtilities.showBasicErrorDialog(getActivity(), 
						getActivity().getString(R.string.dialog_message_error_occurred_exception_message, 
						resultData.getString(DownloadNendoroidImagesIntentService.BUNDLE_KEY_EXCEPTION)));
			}
		}
	}
	
	private OnPreferenceClickListener mCurrentlyDownloadingImagesPreferenceClickListener = new OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			if (mDownloadImagesProgressDialog != null && !mDownloadImagesProgressDialog.isShowing()) {
				mDownloadImagesProgressDialog.show();
			}
			return false;
		}
	};
	
	private OnPreferenceClickListener mDownloadImagesPreferenceClickListener = new OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			boolean isJankiel = 
					mSharedPreferences.getBoolean(AppConstant.SHARED_PREFERENCES_KEY_IS_JANKIEL, false);
			
			if (isJankiel) {
				String[] imagesToDownload = {
						getActivity().getString(R.string.dialog_item_download_basic_images),
						getActivity().getString(R.string.dialog_item_download_other_images)
				};
				
				DialogUtilities.showDialogWithItems(getActivity(), 
						getActivity().getString(R.string.dialog_title_images_to_download_selection), 
						imagesToDownload, new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which == 0) {
									startDownloadNendoroidImagesIntentService(null, DownloadNendoroidImagesIntentService.BASIC_IMAGES);
								} else if (which == 1) {
									startDownloadNendoroidImagesIntentService(null, DownloadNendoroidImagesIntentService.OTHER_IMAGES);
								}
								//TODO add maybe thumbs or main?
							}
						}, getActivity().getString(R.string.dialog_button_cancel));
			} else {
				startDownloadNendoroidImagesIntentService(null, DownloadNendoroidImagesIntentService.BASIC_IMAGES);
			}
			
			return false;
		}
	};
}
