package com.jankiel.nendoroidhunter.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.jankiel.genlibs.utils.android.DialogUtilities;
import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.activities.MainActivity;
import com.jankiel.nendoroidhunter.activities.NendoroidDetailActivity;
import com.jankiel.nendoroidhunter.activities.PreferencesActivity;
import com.jankiel.nendoroidhunter.adapters.NendoroidListAdapater;
import com.jankiel.nendoroidhunter.asynctasks.NendoroidListLoader;
import com.jankiel.nendoroidhunter.models.DatabaseManager;
import com.jankiel.nendoroidhunter.models.Nendoroid;
import com.jankiel.nendoroidhunter.utils.AppConstant;

public class NendoroidListFragment extends Fragment 
				implements LoaderCallbacks<List<Nendoroid>> {
	
	private int mNendoroidListType;
	//private boolean mShouldBeTwoPane;
	private boolean mIsListLoading;

	private List<Nendoroid> mOriginalNendoroidList;
	private DatabaseManager mDbManager;
	
	private Menu mMenu;
	
	private ListView mNendoroidListView;
	private TextView mEmptyTextView;

	private SharedPreferences mSharedPreferences;
	private NendoroidListAdapater mNendoroidListAdapater;
	
	public NendoroidListFragment(int nendoroidListType) {
		this.mNendoroidListType = nendoroidListType;
		mOriginalNendoroidList = new ArrayList<Nendoroid>();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		mNendoroidListAdapater = new NendoroidListAdapater(getActivity(), mOriginalNendoroidList, mNendoroidListType);

		mDbManager = DatabaseManager.getInstance(getActivity());
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_nendoroid_list, container, false);
		
		mEmptyTextView = (TextView) rootView.findViewById(R.id.text_view_empty);
		mEmptyTextView.setText(R.string.list_view_loading);
		
		mNendoroidListView = (ListView) rootView.findViewById(R.id.list_view_nendoroid);
		mNendoroidListView.setAdapter(mNendoroidListAdapater);
		
		//TODO implement long click/selection
		mNendoroidListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				
				Intent nendoroidDetailIntent = new Intent(getActivity(), NendoroidDetailActivity.class);
				nendoroidDetailIntent.putExtra(NendoroidDetailFragment.EXTRA_KEY_NENDOROID_ID, (int) id);
				nendoroidDetailIntent.putExtra(NendoroidDetailFragment.EXTRA_KEY_NENDOROID_LIST_TYPE, mNendoroidListType);
			
				getActivity().startActivityForResult(nendoroidDetailIntent, 0);
			}
		});
		
		hideListView(R.string.list_view_loading);
		loadNendoroidList();
		
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_nendoroid_list, menu);
		
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
		
		searchView.setOnQueryTextListener(new OnQueryTextListener() { 

			@Override 
			public boolean onQueryTextChange(String query) {
				filter(query);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				filter(query);
				return false;
			}
		});
		
		mMenu = menu;

	    super.onCreateOptionsMenu(menu, inflater);
	}
	
	public boolean handleKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_MENU:
			if (mMenu != null) {
				mMenu.performIdentifierAction(R.id.menu_overflow, 0);
			}
			break;
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_search_by:
			showSearchBySelectionDialog();
			return true;
		case R.id.menu_refresh_nendoroid_list:
			hideListView(R.string.list_view_loading);
			loadNendoroidList();
			return true;
		case R.id.menu_preferences:
			getActivity().startActivity(new Intent(getActivity(), PreferencesActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void hideListView(int emptyTextViewTextResourceId) {
		if (mEmptyTextView != null && mNendoroidListView != null) {
			mEmptyTextView.setVisibility(View.VISIBLE);
			mEmptyTextView.setText(emptyTextViewTextResourceId);
			
			mNendoroidListView.setVisibility(View.GONE);
		}
	}
	
	public void showListView() {
		if (mEmptyTextView != null && mNendoroidListView != null) {
			mEmptyTextView.setVisibility(View.GONE);
			
			mNendoroidListView.setVisibility(View.VISIBLE);
		}
	}
	
	public void loadNendoroidList() {
		if (mEmptyTextView != null && mNendoroidListView != null) { //sometimes the fragment's onCreateView is not yet called from ViewPager
			LoaderManager loaderManager = getActivity().getSupportLoaderManager();
			
			if (loaderManager.getLoader(mNendoroidListType) != null
					&& loaderManager.getLoader(mNendoroidListType).isStarted()) {
				loaderManager.restartLoader(mNendoroidListType, null, this).forceLoad();
			} else {
				loaderManager.initLoader(mNendoroidListType, null, this).forceLoad();
			}
		}
	}
	
	public void filter(String query) {
		if (mIsListLoading) {
			return;
		}
		
		if (query == null || query.equals("")) {
			refreshNendoroidListAdapater(mOriginalNendoroidList);
			return;
		}
		
		List<Nendoroid> nendoroidList = new ArrayList<Nendoroid>();
		String defaultSearchField = mSharedPreferences
				.getString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, "");

		for (Nendoroid nendoroid : mOriginalNendoroidList) {
			if (defaultSearchField.equals(Nendoroid.FIELD_NUMBER)) {
				if (queryMatchesString(query, nendoroid.getNumber())) {
					nendoroidList.add(nendoroid);
				}
			} else if (defaultSearchField.equals(Nendoroid.FIELD_NAME)) {
				if (queryMatchesString(query, nendoroid.getName())) {
					nendoroidList.add(nendoroid);
				}
			} else if (defaultSearchField.equals(Nendoroid.FIELD_SERIES)) {
				if (queryMatchesString(query, nendoroid.getSeries())) {
					nendoroidList.add(nendoroid);
				}
			} else if (defaultSearchField.equals(Nendoroid.FIELD_CATEGORY)) {
				if (queryMatchesString(query, nendoroid.getCategory())) {
					nendoroidList.add(nendoroid);
				}
			} else {
				if (queryMatchesString(query, nendoroid.getNumber()) 
						|| queryMatchesString(query, nendoroid.getName())) {
					nendoroidList.add(nendoroid);
				}
			}
		}
		
		refreshNendoroidListAdapater(nendoroidList);
	}
	
	private boolean queryMatchesString(String query, String string) {
		if (query == null || string == null) {
			return false;
		}
		
		query = query.toLowerCase();
		string = string.toLowerCase();
		
		boolean spaceIsDelimiter = false;
		
		if (spaceIsDelimiter) {
			String[] componentArray = query.split(" ");
			
			for (String component : componentArray) {
				if (string.contains(component)) {
					return true;
				}
			}
			
			return false;
		} else {
			return string.contains(query);
		}
		
	}
	
	private void refreshNendoroidListAdapater(List<Nendoroid> nendoroidList) {
		if (mIsListLoading) {
			return;
		}
		
		if (nendoroidList != null && nendoroidList.size() > 0) {
			showListView();
		} else {
			hideListView(R.string.list_view_empty);
		}
		
		mNendoroidListAdapater.setList(nendoroidList);
		mNendoroidListAdapater.notifyDataSetChanged();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//not reloading
		if (resultCode == MainActivity.RESULT_DB_UPDATED) {
			hideListView(R.string.list_view_loading);
			loadNendoroidList();
		}
	}

	@Override
	public Loader<List<Nendoroid>> onCreateLoader(int id, Bundle arguments) {
		mOriginalNendoroidList = new ArrayList<Nendoroid>();
		mIsListLoading = true;
		refreshNendoroidListAdapater(mOriginalNendoroidList);
		return new NendoroidListLoader(getActivity(), mDbManager, mNendoroidListType);
	}

	@Override
	public void onLoadFinished(Loader<List<Nendoroid>> loader, List<Nendoroid> nendoroidList) {
		mOriginalNendoroidList = nendoroidList;
		mIsListLoading = false;
		refreshNendoroidListAdapater(mOriginalNendoroidList);
	}

	@Override
	public void onLoaderReset(Loader<List<Nendoroid>> nendoroidList) {
		mOriginalNendoroidList = new ArrayList<Nendoroid>();
		mIsListLoading = false;
		refreshNendoroidListAdapater(mOriginalNendoroidList);
	}
	
	private void showSearchBySelectionDialog() {
		DialogUtilities.showDialogWithItems(getActivity(), 
				getActivity().getString(R.string.dialog_title_search_by), 
				new String[] {
					getActivity().getString(R.string.dialog_item_search_by_number_or_name),
					getActivity().getString(R.string.dialog_item_search_by_number_only),
					getActivity().getString(R.string.dialog_item_search_by_name_only),
					getActivity().getString(R.string.dialog_item_search_by_series),
					getActivity().getString(R.string.dialog_item_search_by_category),
				}, 
				new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 1: //Number only
							mSharedPreferences.edit()
								.putString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, Nendoroid.FIELD_NUMBER)
								.commit();
							break;
						case 2: //Name only
							mSharedPreferences.edit()
								.putString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, Nendoroid.FIELD_NAME)
								.commit();
							break;
						case 3: //Series
							mSharedPreferences.edit()
								.putString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, Nendoroid.FIELD_SERIES)
								.commit();
							break;
						case 4: //Category
							mSharedPreferences.edit()
								.putString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, Nendoroid.FIELD_CATEGORY)
								.commit();
							break;
						default:
							mSharedPreferences.edit()
								.putString(AppConstant.SHARED_PREFERENCES_KEY_DEFAULT_SEARCH_FIELD, "")
								.commit();
							break;
						}
					}
				});
	}
	
}
