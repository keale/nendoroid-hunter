package com.jankiel.nendoroidhunter.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.jankiel.nendoroidhunter.R;

public class HelpFragment extends Fragment {
	
	private WebView mWebView;
	private Menu mMenu;
	
	public HelpFragment() {
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		getActivity().setTitle(getActivity().getString(R.string.title_help));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_help, container, false);
		
		mWebView = (WebView) rootView.findViewById(R.id.web_view_help);
		mWebView.loadUrl("file:///android_asset/help.html");
		
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		//inflater.inflate(R.menu.menu_help, menu);
		//mMenu = menu;

	    super.onCreateOptionsMenu(menu, inflater);
	}
	
	public boolean handleKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_MENU:
			if (mMenu != null) {
				mMenu.performIdentifierAction(R.id.menu_overflow, 0);
			}
			break;
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
