package com.jankiel.nendoroidhunter.adapters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;

public class ImagePagerAdapter extends PagerAdapter {
	
	private Context mContext;
	
	private Nendoroid mNendoroid;
	private List<Image> mImageList;
	
	private OnImageViewClickListener mClickListener;
	
	private OnImageViewLongClickListener mLongClickListener;

	public ImagePagerAdapter(Context context, Nendoroid nendoroid) {
		this.mContext = context;
		this.mNendoroid = nendoroid;
		
		buildImageList();
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rootView = inflater.inflate(R.layout.pager_item_image, container, false);
		ImageView imageView = (ImageView) rootView.findViewById(R.id.nendoroid_image);
		
		Image image = this.mImageList.get(position);
		
		if (image != null && image.exists()) {
			imageView.setImageURI(Uri.fromFile(new File(image.getFilepath())));
		} else {
			imageView.setImageResource(R.drawable.no_main_img);
		}
		
		if (mClickListener != null) {
			imageView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					mClickListener.onImageViewClick(view, position);
				}
			});
		}
		if (mLongClickListener != null) {
			imageView.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View view) {
					return mLongClickListener.onImageViewLongClick(view, position);
				}
			});
		}
		
		container.addView(rootView);
		return rootView;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
	
	@Override
	public int getCount() {
		return this.mImageList.size();
	}
	
	@Override
	public int getItemPosition(Object object){
	     return POSITION_NONE;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}
	
	public void buildImageList() {
		this.mImageList = new ArrayList<Image>();
		
		this.mImageList.add(mNendoroid.getMainImage());
		
		if (mNendoroid.getImageList() != null) {
			for (Image image : mNendoroid.getImageList()) {
				if (image != null && image.exists()) {
					this.mImageList.add(image);
				}
			}
		}
	}
	
	public void setNendoroid(Nendoroid nendoroid) {
		this.mNendoroid = nendoroid;
	}
	
	public void setOnImageViewClickListener(OnImageViewClickListener listener) {
		mClickListener = listener;
	}
	
	public void setOnImageViewLongClickListener(OnImageViewLongClickListener listener) {
		mLongClickListener = listener;
	}
	
	public interface OnImageViewClickListener {
		public void onImageViewClick(View view, int position);
	}
	
	public interface OnImageViewLongClickListener {
		public boolean onImageViewLongClick(View view, int position);
	}

}
