package com.jankiel.nendoroidhunter.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.Shop;

public class ShopListAdapater extends BaseAdapter {
	
	private Context mContext;
	
	private List<Shop> mShopList;

	public ShopListAdapater(Context context, List<Shop> shopList) {
		this.mContext = context;
		this.mShopList = shopList;
	}
	
	@Override
	public View getView(int position, View row, ViewGroup parent) {
		  
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		
		if(row == null) {
			row = inflater.inflate(R.layout.list_item_shop, parent, false);
		}

		TextView nameTextView = (TextView) row.findViewById(R.id.shop_name);
		TextView typeTextView = (TextView) row.findViewById(R.id.shop_type);

		Shop shop = mShopList.get(position);
				
		nameTextView.setText(shop.getName());
		typeTextView.setText(shop.getTypeName());
		
		return row;
	  }

	@Override
	public int getCount() {
		if (mShopList != null) {
			return mShopList.size();
		} else {
			return 0;
		}
	}

	@Override
	public Shop getItem(int position) {
		return mShopList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mShopList.get(position).getId();
	}
	
	public List<Shop> getList(){
		return mShopList;
	}
	
	public void setList(List<Shop> shopList){
		this.mShopList = shopList;
	}
}
