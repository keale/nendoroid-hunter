package com.jankiel.nendoroidhunter.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.fragments.NendoroidListFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {
	
	public static final int TAB_LOOOKING_FOR = 0;
	public static final int TAB_ACQUIRED = 1;
	public static final int TAB_INTERESTED = 2;
	public static final int TAB_ALL = 3;

	private Context mContext;

	private Fragment[] mFragmentArray;

	public TabPagerAdapter(FragmentManager fragmentManager, Context context) {
		super(fragmentManager);

		this.mContext = context;
		
		mFragmentArray = new Fragment[4];

		mFragmentArray[TAB_LOOOKING_FOR] = new NendoroidListFragment(NendoroidListAdapater.TYPE_LOOKING_FOR);
		mFragmentArray[TAB_ACQUIRED] = new NendoroidListFragment(NendoroidListAdapater.TYPE_ACQUIRED);
		mFragmentArray[TAB_INTERESTED] = new NendoroidListFragment(NendoroidListAdapater.TYPE_INTERESTED);
		mFragmentArray[TAB_ALL] = new NendoroidListFragment(NendoroidListAdapater.TYPE_ALL);
	}

	@Override
	public Fragment getItem(int position) {
		return mFragmentArray[position];
	}

	@Override
	public int getCount() {
		return 4;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return mContext.getString(R.string.title_looking_for);
		case 1:
			return mContext.getString(R.string.title_acquired);
		case 2:
			return mContext.getString(R.string.title_interested);
		case 3:
			return mContext.getString(R.string.title_all);
		}
		return null;
	}
}