package com.jankiel.nendoroidhunter.adapters;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jankiel.nendoroidhunter.R;
import com.jankiel.nendoroidhunter.models.Image;
import com.jankiel.nendoroidhunter.models.Nendoroid;

public class NendoroidListAdapater extends BaseAdapter {

	public static final int TYPE_ALL = 0;
	public static final int TYPE_LOOKING_FOR = 1;
	public static final int TYPE_ACQUIRED = 2;
	public static final int TYPE_INTERESTED = 3;
	
	private Context mContext;
	
	private List<Nendoroid> mNendoroidList;
	private int mNendoroidListType;

	public NendoroidListAdapater(Context context, List<Nendoroid> nendoroidList, int nendoroidListType) {
		this.mContext = context;
		this.mNendoroidList = nendoroidList;
		this.mNendoroidListType = nendoroidListType;
	}
	
	@Override
	public View getView(int position, View row, ViewGroup parent) {
		  
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		
		if(row == null) {
			row = inflater.inflate(R.layout.list_item_nendoroid, parent, false);
		}

		ImageView thumbImageView = (ImageView) row.findViewById(R.id.nendoroid_image);
		
		TextView numberTextView = (TextView) row.findViewById(R.id.nendoroid_number);
		TextView nameTextView = (TextView) row.findViewById(R.id.nendoroid_name);
		TextView statusTextView = (TextView) row.findViewById(R.id.nendoroid_status);

		Nendoroid nendoroid = mNendoroidList.get(position);
		
		if (nendoroid.getThumbImage() != null) {
			Image thumbImage = nendoroid.getThumbImage();
			
			//check if filepath exists catch warning
			if (thumbImage.exists()) {
				thumbImageView.setImageURI(Uri.fromFile(new File(thumbImage.getFilepath())));
			} else {
				thumbImageView.setImageResource(R.drawable.no_thumb_img);
			}
		} else {
			thumbImageView.setImageResource(R.drawable.no_thumb_img);
		}
		
		numberTextView.setText(nendoroid.getNumber());
		nameTextView.setText(nendoroid.getName());
		
		int mainStatusReference = Nendoroid.STATUS_INTERESTED;
		if (mNendoroidListType == TYPE_LOOKING_FOR) {
			mainStatusReference = Nendoroid.STATUS_LOOKING_FOR;
		} else if (mNendoroidListType == TYPE_ACQUIRED) {
			mainStatusReference = Nendoroid.STATUS_ACQUIRED;
		} 
		
		statusTextView.setText(nendoroid.getDescriptiveStatus(mainStatusReference));
		
		return row;
	  }

	@Override
	public int getCount() {
		if (mNendoroidList != null) {
			return mNendoroidList.size();
		} else {
			return 0;
		}
	}

	@Override
	public Nendoroid getItem(int position) {
		return mNendoroidList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mNendoroidList.get(position).getId();
	}
	
	public List<Nendoroid> getList(){
		return mNendoroidList;
	}
	
	public void setList(List<Nendoroid> nendoroidList){
		this.mNendoroidList = nendoroidList;
	}
}
