package com.jankiel.genlibs.utils.android;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jankiel.nendoroidhunter.R;

public class DialogUtilities {
	
	/*
	 * Build dialog methods
	 */
	
	/***
	 * Build a basic information dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static Dialog buildBasicInfoDialog(Context context, String message){
		return buildDialog(context, context.getString(R.string.dialog_title_information), message, null, null,  
				context.getString(R.string.dialog_button_ok), null, null, null);
	}
	
	/***
	 * Build a basic warning dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static Dialog buildBasicWarningDialog(Context context, String message){
		return buildDialog(context, context.getString(R.string.dialog_title_warning), message, null, null,  
				context.getString(R.string.dialog_button_ok), null, null, null);
	}
	
	/***
	 * Build a basic error dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static Dialog buildBasicErrorDialog(Context context, String message){
		return buildDialog(context, context.getString(R.string.dialog_title_error), message, null, null,  
				context.getString(R.string.dialog_button_ok), null, null, null);
	}
	
	/***
	 * Build a dialog with title, message and positive and negative
	 * buttons.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the dialog's positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the dialog's negative button
	 * @param negButtonClickListener onClickListener for the negative button
	 * @return the dialog built
	 */
	public static Dialog buildDialog(Context context, String title, String message, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener){
		
		return buildDialog(context, title, message, null, null, 
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
	}
	
	/***
	 * Build a dialog with a title, message and an OK button. The OK button's
	 * onClickListener does nothing but dismiss the dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static Dialog buildDialog(Context context, String title, String message){
		return buildDialog(context, title, message, null, null,  
			context.getString(R.string.dialog_button_ok), null, null, null);
	}
	
	/***
	 * Build a dialog with a title, message and a positive button. The positive button's
	 * onClickListener does nothing but dismiss the dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the positive button
	 * @return the dialog built
	 */
	public static Dialog buildDialog(Context context, String title, String message, String posButtonText){
		return buildDialog(context, title, message, null, null,  
			posButtonText, null, null, null);
	}
	
	/***
	 * Build a dialog with a title, message and a positive button. The positive button's
	 * onClickListener is passed as a parameter.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @return the dialog built
	 */
	public static Dialog buildDialog(Context context, String title, String message,
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener){
		return buildDialog(context, title, message, null, null,  
			posButtonText, posButtonClickListener, null, null);
	}
	
	/***
	 * Build a dialog with a title, message a positive button and a negative
	 * button. The positive button's onClickListener is passed as a parameter.
	 * The negative button's click just dismisses the dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the negative button
	 * @return the dialog built
	 */
	public static Dialog buildDialog(Context context, String title, String message, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText) {
		
		return buildDialog(context, title, message, null, null,
				posButtonText, posButtonClickListener, negButtonText, null);
	}
	
	/***
	 * Build a dialog with a title, item list, positive and negative buttons. The item
	 * onClickListener and the positive button and negative button's onClickListener
	 * are passed as parameter.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the negative button
	 * @param negButtonClickListener onClickListener for the positive button
	 * @return the dialog built
	 */
	public static Dialog buildDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener) {
		
		return buildDialog(context, title, null, items, itemClickListener,
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
	}
	
	/***
	 * 
	 * Build a dialog with a title, item list and a positive button. The item
	 * onClickListener and the negative button button's onClickListener
	 * are passed as parameter.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param negButtonText the text on the negative button
	 * @param negButtonClickListener onClickListener for the negative button
	 * @return the dialog built
	 */
	public static Dialog buildDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener) {
		
		return buildDialog(context, title, null, items, itemClickListener,
				negButtonText, negButtonClickListener, null, null);
	}
	
	/***
	 * Build a dialog with a title, item list and a negative button. The item
	 * onClickListener is passed as a parameter. The negative button just
	 * dismisses the dialog.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param negButtonText the text on the negative button
	 * @return the dialog built
	 */
	public static Dialog buildDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String posButtonText) {
		
		return buildDialog(context, title, null, items, itemClickListener,
				posButtonText, null, null, null);
	}
	
	/***
	 * Build a dialog with a title and an item list. The item
	 * onClickListener is passed as a parameter.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @return the dialog built
	 */
	public static Dialog buildDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener) {
		
		return buildDialog(context, title, null, items, itemClickListener,
				null, null, null, null);
	}
	
	/***
	 * Build a dialog with all the options for building a dialog supplied
	 * as parameters. This method is the used by all the other "build" 
	 * methods of this class to build their respective dialogs, passing only
	 * the parameters needed and setting the others to null. If a parameter
	 * is set to null, it means that it is not needed and is not displayed
	 * on the constructed dialog i.e. its "set" method is not called. 
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the negative button
	 * @param negButtonClickListener onClickListener for the positive button
	 * @return the dialog built
	 */
	public static Dialog buildDialog(Context context, String title, String message, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener){

		AlertDialog.Builder builder = new Builder(context);
		
		if(title != null)
			builder.setTitle(title);
		else builder.setTitle(context.getString(R.string.dialog_title_default_title));
		
		if(message != null)
			builder.setMessage(message);
		
		if(items != null && itemClickListener != null)
			builder.setItems(items, itemClickListener);
		
		if(posButtonClickListener != null){
			if(posButtonText == null)
				builder.setPositiveButton(context.getString(R.string.dialog_button_ok), posButtonClickListener);
			else 
				builder.setPositiveButton(posButtonText, posButtonClickListener);
		} else if(posButtonText != null) {
			//posButtonText is provided, meaning a simple dismiss function is needed.
			builder.setPositiveButton(posButtonText, dismissOnClickListener());
		}
		
		if(negButtonClickListener != null){
			if(negButtonText == null)
				builder.setNegativeButton(context.getString(R.string.dialog_button_cancel), negButtonClickListener);
			else 
				builder.setNegativeButton(negButtonText, negButtonClickListener);
		} else if(negButtonText != null) {
			//negButtonText is provided, meaning a simple dismiss function is needed.
			builder.setNegativeButton(negButtonText, dismissOnClickListener());
		}
		
		return builder.create();
	}
	
	public static Dialog buildDialogWithEditText(Context context, String title, String defaultText){
		return buildDialogWithEditText(context, title, defaultText, 
			context.getString(R.string.dialog_button_ok), null, null, null);
	}
	
	public static Dialog buildDialogWithEditText(Context context, String title, String defaultText, String posButtonText){
		return buildDialogWithEditText(context, title, defaultText,
			posButtonText, null, null, null);
	}
	
	public static Dialog buildDialogWithEditText(Context context, String title, String defaultText,
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener){
		return buildDialogWithEditText(context, title, defaultText, 
			posButtonText, posButtonClickListener, null, null);
	}
	
	public static Dialog buildDialogWithEditText(Context context, String title, String defaultText, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener, 
			String negButtonText) {
		
		return buildDialogWithEditText(context, title, defaultText,
				posButtonText, posButtonClickListener, negButtonText, null);
	}

	public static Dialog buildDialogWithEditText(Context context, String title, String defaultText,
			String posButtonText, final OnEditTextDialogClickListener posButtonClickListener, 
			String negButtonText, final OnEditTextDialogClickListener negButtonClickListener) {

		//XXX dirty workaround here, source:http://stackoverflow.com/q/19741872/2431281
		context.setTheme(android.R.style.Theme_Holo_Light);
		AlertDialog.Builder builder = new Builder(context);
		
		if(title != null)
			builder.setTitle(title);
		else builder.setTitle(context.getString(R.string.dialog_title_default_title));
		
		final EditText editText = new EditText(context);

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.WRAP_CONTENT);
		editText.setLayoutParams(layoutParams);
		
		Resources resources = context.getResources();
		int topPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, resources.getDisplayMetrics());
		int bottomPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, resources.getDisplayMetrics());
		int leftPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, resources.getDisplayMetrics());
		int rightPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, resources.getDisplayMetrics());
		editText.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
		
		if (defaultText != null)
			editText.setText(defaultText);
		
		builder.setView(editText);
		
		if(posButtonClickListener != null){
			if(posButtonText == null) {
				builder.setPositiveButton(context.getString(R.string.dialog_button_ok), new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						posButtonClickListener.onClick(dialog, which, editText);
					}
				});
			} else {
				builder.setPositiveButton(posButtonText, new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						posButtonClickListener.onClick(dialog, which, editText);
					}
				});
			}
		} else if(posButtonText != null) {
			//posButtonText is provided, meaning a simple dismiss function is needed.
			builder.setPositiveButton(posButtonText, dismissOnClickListener());
		}
		
		if(negButtonClickListener != null){
			if(negButtonText == null) {
				builder.setNegativeButton(context.getString(R.string.dialog_button_cancel), new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						negButtonClickListener.onClick(dialog, which, editText);
					}
				});
			} else {
				builder.setNegativeButton(negButtonText, new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						negButtonClickListener.onClick(dialog, which, editText);
					}
				});
			}
		} else if(negButtonText != null) {
			//negButtonText is provided, meaning a simple dismiss function is needed.
			builder.setNegativeButton(negButtonText, dismissOnClickListener());
		}
		
		return builder.create();
	}
	
	public static Dialog buildDialogWithNumericEditText(Context context, String title, String defaultText){
		return buildDialogWithNumericEditText(context, title, defaultText, null,  
			context.getString(R.string.dialog_button_ok), null, null, null);
	}

	public static Dialog buildDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher){
		return buildDialogWithNumericEditText(context, title, defaultText, numericEditTextWatcher,  
			context.getString(R.string.dialog_button_ok), null, null, null);
	}

	
	public static Dialog buildDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, String posButtonText){
		return buildDialogWithNumericEditText(context, title, defaultText, numericEditTextWatcher,  
			posButtonText, null, null, null);
	}
	
	public static Dialog buildDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener){
		return buildDialogWithNumericEditText(context, title, defaultText, numericEditTextWatcher,  
			posButtonText, posButtonClickListener, null, null);
	}
	
	public static Dialog buildDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener, 
			String negButtonText) {
		
		return buildDialogWithNumericEditText(context, title, defaultText, numericEditTextWatcher,
				posButtonText, posButtonClickListener, negButtonText, null);
	}

	public static Dialog buildDialogWithNumericEditText(Context context, String title, 
			String defaultText, final NumericEditTextWatcher numericEditTextWatcher, 
			String posButtonText, final OnEditTextDialogClickListener posButtonClickListener, 
			String negButtonText, final OnEditTextDialogClickListener negButtonClickListener) {

		//XXX dirty workaround here, source:http://stackoverflow.com/q/19741872/2431281
		context.setTheme(android.R.style.Theme_Holo_Light);
		AlertDialog.Builder builder = new Builder(context);
		
		if(title != null)
			builder.setTitle(title);
		else builder.setTitle(context.getString(R.string.dialog_title_default_title));
		
		final EditText editText = new EditText(context);

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.WRAP_CONTENT);
		editText.setLayoutParams(layoutParams);
		
		if (numericEditTextWatcher != null) {
			editText.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
					numericEditTextWatcher.onTextChanged(charSequence, start, before, count, 
							editText, this);
				}
				
				@Override
				public void beforeTextChanged(CharSequence charSequence, int start, int count,
						int after) {
					numericEditTextWatcher.beforeTextChanged(charSequence, start, count, after, editText, this);
				}
				
				@Override
				public void afterTextChanged(Editable string) {
					numericEditTextWatcher.afterTextChanged(string, editText, this);
				}
			});
		}
		
		editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		
		Resources resources = context.getResources();
		int topPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, resources.getDisplayMetrics());
		int bottomPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, resources.getDisplayMetrics());
		int leftPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, resources.getDisplayMetrics());
		int rightPadding = 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, resources.getDisplayMetrics());
		editText.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
		
		if (defaultText != null)
			editText.setText(defaultText);
		
		builder.setView(editText);
		
		if(posButtonClickListener != null){
			if(posButtonText == null) {
				builder.setPositiveButton(context.getString(R.string.dialog_button_ok), new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						posButtonClickListener.onClick(dialog, which, editText);
					}
				});
			} else {
				builder.setPositiveButton(posButtonText, new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						posButtonClickListener.onClick(dialog, which, editText);
					}
				});
			}
		} else if(posButtonText != null) {
			//posButtonText is provided, meaning a simple dismiss function is needed.
			builder.setPositiveButton(posButtonText, dismissOnClickListener());
		}
		
		if(negButtonClickListener != null){
			if(negButtonText == null) {
				builder.setNegativeButton(context.getString(R.string.dialog_button_cancel), new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						negButtonClickListener.onClick(dialog, which, editText);
					}
				});
			} else {
				builder.setNegativeButton(negButtonText, new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						negButtonClickListener.onClick(dialog, which, editText);
					}
				});
			}
		} else if(negButtonText != null) {
			//negButtonText is provided, meaning a simple dismiss function is needed.
			builder.setNegativeButton(negButtonText, dismissOnClickListener());
		}
		
		return builder.create();
	}
	
	public static Dialog buildDialogWithView(Context context, String title, View view, 
			String posButtonText, OnViewDialogClickListener posButtonClickListener, 
			String negButtonText, OnViewDialogClickListener negButtonClickListener){
		
		return buildDialogWithView(context, title, view, null, null, 
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
	}
	
	public static Dialog buildDialogWithView(Context context, String title, View view){
		return buildDialogWithView(context, title, view, null, null,  
			context.getString(R.string.dialog_button_ok), null, null, null);
	}
	
	public static Dialog buildDialogWithView(Context context, String title, View view, String posButtonText){
		return buildDialogWithView(context, title, view, null, null,  
			posButtonText, null, null, null);
	}
	
	public static Dialog buildDialogWithView(Context context, String title, View view,
			String posButtonText, OnViewDialogClickListener posButtonClickListener){
		return buildDialogWithView(context, title, view, null, null,  
			posButtonText, posButtonClickListener, null, null);
	}
	
	public static Dialog buildDialogWithView(Context context, String title, View view, 
			String posButtonText, OnViewDialogClickListener posButtonClickListener, 
			String negButtonText) {
		
		return buildDialogWithView(context, title, view, null, null,
				posButtonText, posButtonClickListener, negButtonText, null);
	}

	public static Dialog buildDialogWithView(Context context, String title, final View view, 
			String[] items, OnViewDialogClickListener itemClickListener, 
			String posButtonText, final OnViewDialogClickListener posButtonClickListener, 
			String negButtonText, final OnViewDialogClickListener negButtonClickListener) {

		AlertDialog.Builder builder = new Builder(context);
		
		if(title != null)
			builder.setTitle(title);
		else builder.setTitle(context.getString(R.string.dialog_title_default_title));
				
		builder.setView(view);
		
		if(posButtonClickListener != null){
			if(posButtonText == null) {
				builder.setPositiveButton(context.getString(R.string.dialog_button_ok), new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						posButtonClickListener.onClick(dialog, which, view);
					}
				});
			} else {
				builder.setPositiveButton(posButtonText, new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						posButtonClickListener.onClick(dialog, which, view);
					}
				});
			}
		} else if(posButtonText != null) {
			//posButtonText is provided, meaning a simple dismiss function is needed.
			builder.setPositiveButton(posButtonText, dismissOnClickListener());
		}
		
		if(negButtonClickListener != null){
			if(negButtonText == null) {
				builder.setNegativeButton(context.getString(R.string.dialog_button_cancel), new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						negButtonClickListener.onClick(dialog, which, view);
					}
				});
			} else {
				builder.setNegativeButton(negButtonText, new OnClickListener() {
		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						negButtonClickListener.onClick(dialog, which, view);
					}
				});
			}
		} else if(negButtonText != null) {
			//negButtonText is provided, meaning a simple dismiss function is needed.
			builder.setNegativeButton(negButtonText, dismissOnClickListener());
		}
		
		return builder.create();
	}
	
	/*
	 * Show dialog methods
	 */
	
	/***
	 * Show a basic warning dialog. Builds the dialog using the
	 * same build dialog method with the same parameters and calls
	 * the dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static void showBasicWarningDialog(Context context, String message){
		Dialog dialog = buildBasicWarningDialog(context, message);
		
		dialog.show();
	}
	
	/***
	 * Show a basic error dialog. Builds the dialog using the
	 * same build dialog method with the same parameters and calls
	 * the dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static void showBasicErrorDialog(Context context, String message){
		Dialog dialog = buildBasicErrorDialog(context, message);
		
		dialog.show();
	}
	
	/***
	 * Show a basic information dialog. Builds the dialog using the
	 * same build dialog method with the same parameters and calls
	 * the dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static void showBasicInfoDialog(Context context, String message){
		Dialog dialog = buildBasicInfoDialog(context, message);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with title, message and positive and negative
	 * buttons. Builds the dialog using the build dialog method
	 * with the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the dialog's positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the dialog's negative button
	 * @param negButtonClickListener onClickListener for the negative button
	 * @return the dialog built
	 */
	public static void showDialog(Context context, String title, String message, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener){
		
		Dialog dialog = buildDialog(context, title, message, 
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
		dialog.show();
		
	}
	
	/***
	 * Show a dialog with a title, message and an OK button. The OK button's
	 * onClickListener does nothing but dismiss the dialog. Builds the dialog
	 * using the build dialog method with the same parameters and calls
	 * the dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @return the dialog built
	 */
	public static void showDialog(Context context, String title, String message){
		Dialog dialog = buildDialog(context, title, message);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with a title, message and a positive button. The positive button's
	 * onClickListener does nothing but dismiss the dialog. Builds the dialog using the
	 * same build dialog method with the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the positive button
	 * @return the dialog built
	 */
	public static void showDialog(Context context, String title, String message, String posButtonText){
		Dialog dialog = buildDialog(context, title, message,  
			posButtonText);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with a title, message and a positive button. The positive button's
	 * onClickListener is passed as a parameter.  Builds the dialog using the build
	 * dialog method with the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @return the dialog built
	 */
	public static void showDialog(Context context, String title, String message,
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener){
		
		Dialog dialog = buildDialog(context, title, message,  
			posButtonText, posButtonClickListener);
		
		dialog.show();
	}

	/***
	 * Show a dialog with a title, message a positive button and a negative
	 * button. The positive button's onClickListener is passed as a parameter.
	 * The negative button's click just dismisses the dialog.  Builds the dialog
	 * using the build dialog method with the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the negative button
	 * @return the dialog built
	 */
	public static void showDialog(Context context, String title, String message,
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener,
			String negButtonText){
		
		Dialog dialog = buildDialog(context, title, message,  
			posButtonText, posButtonClickListener, negButtonText);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with a title, item list, positive and negative buttons. The item
	 * onClickListener and the positive button and negative button's onClickListener
	 * are passed as parameter. Builds the dialog using the build dialog method with
	 * the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the negative button
	 * @param negButtonClickListener onClickListener for the positive button
	 * @return the dialog built
	 */
	public static void showDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener) {
		
		Dialog dialog = buildDialogWithItems(context, title, items, itemClickListener,
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
		dialog.show();
	}

	/***
	 * 
	 * Show a dialog with a title, item list and a positive button. The item
	 * onClickListener and the negative button button's onClickListener
	 * are passed as parameter. Builds the dialog using the build dialog
	 * method with the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param negButtonText the text on the negative button
	 * @param negButtonClickListener onClickListener for the negative button
	 * @return the dialog built
	 */
	public static void showDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener) {
		
		Dialog dialog = buildDialogWithItems(context, title, items, itemClickListener,
				negButtonText, negButtonClickListener);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with a title, item list and a negative button. The item
	 * onClickListener is passed as a parameter. The negative button just
	 * dismisses the dialog. Builds the dialog using the build dialog method
	 * with the same parameters and calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param negButtonText the text on the negative button
	 * @return the dialog built
	 */
	public static void showDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String negButtonText) {
		
		Dialog dialog = buildDialogWithItems(context, title, items, itemClickListener,
				negButtonText);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with a title and an item list. The item
	 * onClickListener is passed as a parameter. Builds the dialog
	 * using the build dialog method with the same parameters and
	 * calls that dialog's show method.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @return the dialog built
	 */
	public static void showDialogWithItems(Context context, String title, 
			String[] items, DialogInterface.OnClickListener itemClickListener) {
		
		Dialog dialog = buildDialogWithItems(context, title, items, itemClickListener);
		
		dialog.show();
	}
	
	/***
	 * Show a dialog with all the options for building a dialog supplied
	 * as parameters. Builds the dialog using the build dialog method with
	 * the same parameters and calls that dialog's show method. 
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param title the title of the dialog to display
	 * @param message the message to display
	 * @param items the list of items to display
	 * @param itemClickListener the onClickListener for an item click
	 * @param posButtonText the text on the positive button
	 * @param posButtonClickListener onClickListener for the positive button
	 * @param negButtonText the text on the negative button
	 * @param negButtonClickListener onClickListener for the positive button
	 * @return the dialog built
	 */
	public static void showDialog(Context context, String title, String message, 
			String[] items, DialogInterface.OnClickListener itemClickListener, 
			String posButtonText, DialogInterface.OnClickListener posButtonClickListener, 
			String negButtonText, DialogInterface.OnClickListener negButtonClickListener){
		
		Dialog dialog = buildDialog(context, title, message, items, itemClickListener, 
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
		dialog.show();
	}
	
	public static void showDialogWithEditText(Context context, String title, String defaultText, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener, 
			String negButtonText, OnEditTextDialogClickListener negButtonClickListener){
		
		Dialog dialog = buildDialogWithEditText(context, title, defaultText, 
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
		dialog.show();
		
	}
	
	public static void showDialogWithEditText(Context context, String title, String defaultText){
		Dialog dialog = buildDialogWithEditText(context, title, defaultText);
		
		dialog.show();
	}
	
	public static void showDialogWithEditText(Context context, String title, String defaultText, String posButtonText){
		Dialog dialog = buildDialogWithEditText(context, title, defaultText,  
			posButtonText);
		
		dialog.show();
	}
	
	public static void showDialogWithEditText(Context context, String title, String defaultText,
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener){
		
		Dialog dialog = buildDialogWithEditText(context, title, defaultText,  
			posButtonText, posButtonClickListener);
		
		dialog.show();
	}
	
	public static void showDialogWithEditText(Context context, String title, String defaultText,
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener,
			String negButtonText){
		
		Dialog dialog = buildDialogWithEditText(context, title, defaultText,  
			posButtonText, posButtonClickListener, negButtonText);
		
		dialog.show();
	}
	
	public static void showDialogWithNumericEditText(Context context, String title, 
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener, 
			String negButtonText, OnEditTextDialogClickListener negButtonClickListener){
		
		Dialog dialog = buildDialogWithNumericEditText(context, title, defaultText, numericEditTextWatcher,
				posButtonText, posButtonClickListener, negButtonText, negButtonClickListener);
		
		dialog.show();
		
	}
	
	public static void showDialogWithNumericEditText(Context context, String title, String defaultText){
		Dialog dialog = buildDialogWithNumericEditText(context, title, defaultText);
		
		dialog.show();
	}
	
	public static void showDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher){
		Dialog dialog = buildDialogWithNumericEditText(context, title, defaultText);
		
		dialog.show();
	}

	public static void showDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, String posButtonText){
		Dialog dialog = buildDialogWithNumericEditText(context, title, defaultText, 
			numericEditTextWatcher, posButtonText);
		
		dialog.show();
	}
	
	public static void showDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener){
		
		Dialog dialog = buildDialogWithNumericEditText(context, title, defaultText,  
			numericEditTextWatcher, posButtonText, posButtonClickListener);
		
		dialog.show();
	}
	
	public static void showDialogWithNumericEditText(Context context, String title,
			String defaultText, NumericEditTextWatcher numericEditTextWatcher, 
			String posButtonText, OnEditTextDialogClickListener posButtonClickListener,
			String negButtonText){
		
		Dialog dialog = buildDialogWithNumericEditText(context, title, defaultText,  
			numericEditTextWatcher, posButtonText, posButtonClickListener, negButtonText);
		
		dialog.show();
	}
	
	public static void showDialogWithView(Context context, String title, View view, 
			String posButtonText, OnViewDialogClickListener posButtonClickListener, 
			String negButtonText, OnViewDialogClickListener negButtonClickListener){
		
		Dialog dialog = buildDialogWithView(context, title, view, posButtonText, posButtonClickListener, 
				negButtonText, negButtonClickListener);
		
		dialog.show();
		
	}
	
	public static void showDialogWithView(Context context, String title, View view){
		Dialog dialog = buildDialogWithView(context, title, view);
		
		dialog.show();
	}
	
	public static void showDialogWithView(Context context, String title, View view, String posButtonText){
		Dialog dialog = buildDialogWithView(context, title, view, posButtonText);
		
		dialog.show();
	}
	
	public static void showDialogWithView(Context context, String title, View view,
			String posButtonText, OnViewDialogClickListener posButtonClickListener){
		
		Dialog dialog = buildDialogWithView(context, title, view,  
			posButtonText, posButtonClickListener);
		
		dialog.show();
	}
	
	public static void showDialogWithView(Context context, String title, View view,
			String posButtonText, OnViewDialogClickListener posButtonClickListener,
			String negButtonText){
		
		Dialog dialog = buildDialogWithView(context, title, view,  
			posButtonText, posButtonClickListener, negButtonText);
		
		dialog.show();
	}
	
	/***
	 * Show a file list dialog that acts as a mini file explorer. The user
	 * can navigate through the android's file system and do one of the provided
	 * runnable interfaces depending on whether it is a file or a directory. If
	 * the runnable interface for the directory is null, then selecting a directory
	 * means going inside that directory.
	 * <p>
	 * To add the present working directory as a title to the dialog. Use "\{dir}" as
	 * placeholder.
	 * <p>
	 * 
	 * @param context the context in which this dialog will be created
	 * @param directory the directory to be loaded and shown.
	 * @param filter the filename filter to be used for filtering shown items
	 * @param onFileClickListener the onFileClickListener if the selected item is a file
	 * @param onDirectoryClickListener the onFileClickListener if the selected item is a directory
	 */
	public static void showFileListDialog(final Context context, final String titleGiven,
			final String directory, final FilenameFilter filter,
			final OnFileListDialogClickListener onFileClickListener, 
			final OnFileListDialogClickListener onDirectoryClickListener,
			final String posButtonText, //TODO make more methods
			final OnFileListDialogClickListener posButtonClickListener){
		
		File[] tempFileList = loadFileList(directory, filter);
		
		//sort the files before checking for up dir option applicability
		Arrays.sort(tempFileList, new Comparator<File>() {

			@Override
			public int compare(File leftFile, File rightFile) {
				String leftFilename = leftFile.getName().toLowerCase();
				String rightFilename = rightFile.getName().toLowerCase();
				return leftFilename.compareTo(rightFilename);
			}
			
		});
		
		final File[] fileList = addAnUpDirectoryOptionIfApplicable(tempFileList, directory);
		final String[] filenameList = getFilenames(fileList);
		
		//means that an up dir option was added
		if(fileList.length > tempFileList.length) {
			filenameList[0] = "..";
		}
		
		final String title = titleGiven == null ? directory : addDirectoryOnTitle(titleGiven, directory);
		
		DialogInterface.OnClickListener posButtonListener = null;
		
		if (posButtonClickListener != null) {
			posButtonListener = new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					posButtonClickListener.onClick(dialog, which, new File(directory));
				}
			};
		}
		
		showDialog(context, title, null, filenameList, 
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

							File chosenFile = fileList[which];
							
							if(chosenFile.isDirectory()) {
								if(onDirectoryClickListener != null) {
									onDirectoryClickListener.onClick(dialog, which, chosenFile);
								} else {
									showFileListDialog(context, titleGiven, chosenFile.getAbsolutePath(), 
											filter, onFileClickListener, onDirectoryClickListener, 
											posButtonText, posButtonClickListener);
								}
							} else {
								onFileClickListener.onClick(dialog, which, chosenFile);
							}
						}
					}, 
					posButtonText, posButtonListener, context.getString(R.string.dialog_button_cancel), dismissOnClickListener());
		
	}
	
	/***
	 * Show a toast. Calls the showToastShort method to show a toast
	 * of short duration.
	 * <p>
	 * 
	 * @param context the context in which the toast will be created
	 * @param text the text to display
	 */
	public static void showToast(Context context, String text){
		showToastShort(context, text);
	}
	
	/***
	 * Show a short duration toast.
	 * <p>
	 * 
	 * @param context the context in which the toast will be created
	 * @param text the text to display
	 */
	public static void showToastShort(Context context, String text){
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	/***
	 * Show a long duration toast.
	 * <p>
	 * 
	 * @param context the context in which the toast will be created
	 * @param text the text to display
	 */
	public static void showToastLong(Context context, String text){
		Toast.makeText(context, text, Toast.LENGTH_LONG).show();
	}
	
	/***
	 * Returns a dialog button onClickListener that dismisses that dialog.
	 * <p>
	 * 
	 * @return an onClickListener that dismisses the dialog it is attached to
	 */
	public static DialogInterface.OnClickListener dismissOnClickListener(){
		return new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		};
	}
	
	/***
	 * 
	 * @author jacatbay
	 *
	 */
	public interface OnEditTextDialogClickListener {
		
		/***
		 * 
		 */
		public void onClick(DialogInterface dialog, int which, EditText editText);
	}
	
	/***
	 * 
	 * @author jacatbay
	 *
	 */
	public interface OnViewDialogClickListener {
		
		/***
		 * 
		 */
		public void onClick(DialogInterface dialog, int which, View view);
	}
	
	/***
	 * 
	 * @author jacatbay
	 *
	 */
	public interface OnFileListDialogClickListener {
		
		/***
		 * 
		 */
		public void onClick(DialogInterface dialog, int which, File currentFile);
	}
	
	/***
	 * 
	 * @author jacatbay
	 *
	 */
	//XXX think of something better
	public interface NumericEditTextWatcher {
		
		/***
		 * 
		 */
		public void onTextChanged(CharSequence charSequence, int start, int before, int count,
				EditText editText, TextWatcher textWatcher);
		
		/***
		 * 
		 */
		public void beforeTextChanged(CharSequence charSequence, int start, int count,
				int after, EditText editText, TextWatcher textWatcher);
		
		/***
		 * 
		 */
		public void afterTextChanged(Editable string, EditText editText, TextWatcher textWatcher);
	}
	
	/*
	 * Dialog specific methods. Methods that some special dialogs use to be built
	 */
	
	/***
	 * Loads the file list of the given directory as an array of File objects.
	 * Used by the fileListDialog methods. Never returns null but returns an
	 * array of length 0.
	 * <p>
	 * 
	 * @param directory the directory to load
	 * @param filter the filter used to filter the items loaded
	 * @return an array of File objects inside the directory filtered by the filter
	 */
	private static File[] loadFileList(String directory, FilenameFilter filter) {
		File path = new File(directory);
		
	    if(path.exists()) {
	        //if null return an empty array instead
	        File[] list = path.listFiles(filter); 
	        return list == null? new File[0] : list;
	        
	    } else return new File[0];
	}
	
	/***
	 * Checks if the given directory can have an "up" option. If the directory
	 * given is not the root, adds the directory's parent as the first item
	 * and then appends the other files objects. Else does nothing.
	 * <p>
	 * 
	 * The up option is an option whether to go back to the previous directory,
	 * normally the directory above it if the file structure is visualizes as 
	 * an n-ary tree with the root at the top.
	 * 
	 * @param files the File array to be processed
	 * @param directory the directory that will be used to determin whether "up" option is applicable
	 * @return the File array object processed
	 */
	private static File[] addAnUpDirectoryOptionIfApplicable(File[] files, String directory) {
		//if directory is root, no need to up one 
		if(directory.equals("/"))
			return files;
		else {
			File[] newFiles = new File[files.length+1];
			
			//add an "up" option
			newFiles[0] = new File(upOneDirectory(directory));
			
			for(int i = 0; i < files.length; i++){
				newFiles[i+1] = files[i];
			}
			return newFiles;
		}
	}
	
	/***
	 * Transforms the File array to a String array and returns it. Uses
	 * the File objects filename.
	 * <p>
	 * 
	 * @param files the File array to get the filenames from
	 * @return a String array of file name based on the given File array
	 */
	private static String[] getFilenames(File[] files) {
		String[] filenames = new String[files.length];
		
		for(int i = 0; i < files.length; i++){
			if(files[i].isDirectory())
				filenames[i] = files[i].getName() + File.separator;
			else filenames[i] = files[i].getName();
		}
		return filenames;
	}
	
	/***
	 * Returns the given directory's parent.
	 * <p>
	 * 
	 * @param directory the directory to process
	 * @return the parent directory
	 */
	private static String upOneDirectory(String directory){
		String[] dirs = directory.split(Pattern.quote(File.separator));
		StringBuilder stringBuilder = new StringBuilder("");
		
		for(int i = 0; i < dirs.length-1; i++)
			stringBuilder.append(dirs[i]).append(File.separator);
		
		return stringBuilder.toString();
	}
	
	private static String addDirectoryOnTitle(String title, String directory){
		String placeholder = "\\{dir}";
		
		if(!title.contains(placeholder))
			return title;
		
		StringBuilder string = new StringBuilder(title);
		
		int startIndex = string.indexOf(placeholder);
		string.replace(startIndex, startIndex + placeholder.length(), directory);
		
		return string.toString();
	}
	
	/*
	 * Other useful methods
	 */
	
	
	@SuppressWarnings("deprecation")
	public static void showNotification(Context context, int iconResource, long delay, String title,
			String tickerText, PendingIntent pendingIntent) {
		
		if(context == null) return;
		
	    NotificationManager notificationManager = (NotificationManager) 
	    		context.getSystemService(Context.NOTIFICATION_SERVICE);
	    
	    iconResource = iconResource == -1 ? R.drawable.ic_launcher : iconResource;
	    delay = delay < 0 ? System.currentTimeMillis() + 1000 : delay;
	    
	    title = title == null  || title.equals("") ? "Notification" : title;
	    tickerText = tickerText == null  || tickerText.equals("") ?  "You have a notification from this app" : tickerText;
	    
	    pendingIntent = pendingIntent ==  null ?  PendingIntent.getActivity(context, 0, new Intent(), 0) 
	    		: pendingIntent;
	    
	    Notification notification = new Notification(iconResource, tickerText, delay);
	    notification.setLatestEventInfo(context, title, tickerText, pendingIntent);
	    
	    notificationManager.notify(0, notification);
	}
	
	/**
	 * This class cannot be instantiated.
	 */
	 private DialogUtilities(){
		//this prevents even the native class from 
		//calling this ctor as well
		throw new AssertionError();
	}
}
