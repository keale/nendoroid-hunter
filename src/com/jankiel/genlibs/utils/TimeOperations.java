package com.jankiel.genlibs.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/***
 * Class containing some useful operations for time.
 * <p>
 * Contains methods for addition, subtraction etc.
 * Also contains methods for formatting time. Time is
 * String format.
 * <p>
 * 
 * @author jacatbay
 *
 */
public class TimeOperations {

	/***
	 * Gets the difference of the two arguments and
	 * returns it in HH:MM:SS format.
	 * <p>
	 * Note the arrangement of the arguments so as
	 * not to return a negative value.
	 * <p>
	 * 
	 * @param minuend    the time that is larger
	 * @param subtrahend the time that is smaller.
	 * @return           the difference in HH:MM:SS format
	 */
	public static String timeDifferenceInHHMMSS(String minuend, String subtrahend){
		int seconds1 = HHMMSSToSeconds(minuend);
		int seconds2 = HHMMSSToSeconds(subtrahend);
		
		int difference = seconds1 - seconds2;
		
		return secondsToHHMMSS(difference);
	}

	/***
	 * Gets the difference of the two arguments and
	 * returns it in its numeric value
	 * <p>
	 * Note the arrangement of the arguments so as
	 * not to return a negative value.
	 * <p>
	 * 
	 * @param minuend    the time that is larger
	 * @param subtrahend the time that is smaller.
	 * @return           the difference in seconds
	 */
	public static int timeDifferenceInSeconds(String minuend, String subtrahend){
		int seconds1 = HHMMSSToSeconds(minuend);
		int seconds2 = HHMMSSToSeconds(subtrahend);
		
		return (seconds1 - seconds2);
	}

	/***
	 * Gets the sum of the two arguments and
	 * returns it in HH:MM:SS format.
	 * <p>
	 * 
	 * @param addend1 the first addend
	 * @param addend2 the second addend
	 * @return        the sum in HH:MM:SS format
	 */
	public static String timeSumInHHMMSS(String addend1, String addend2){
		int seconds1 = HHMMSSToSeconds(addend1);
		int seconds2 = HHMMSSToSeconds(addend2);
		
		int sum = seconds1 + seconds2;
		
		return secondsToHHMMSS(sum);
	}

	/***
	 * Gets the difference of the two arguments and
	 * returns it in its numeric value
	 * <p>
	 * Note the arrangement of the arguments so as
	 * not to return a negative value.
	 * <p>
	 * 
	 * @param addend1 the first addend
	 * @param addend2 the second addend
	 * @return        the sum in seconds
	 */
	public static int timeSumInSeconds(String addend1, String addend2){
		int seconds1 = HHMMSSToSeconds(addend1);
		int seconds2 = HHMMSSToSeconds(addend2);
		
		return (seconds1 + seconds2);
	}
	
	/***
	 * Converts a numeric value to HH:MM:SS format.
	 * <p>
	 * 
	 * @param secs the numeric value to convert
	 * @return     the converted value in HH:MM:SS format
	 */
	public static String secondsToHHMMSS(int secs) {

		int hours = secs / 3600;
		int remainder = secs % 3600;
		int minutes = remainder / 60;
		int seconds = remainder % 60;

		return ( (hours < 10 ? "0" : "") + hours
				+ ":" + (minutes < 10 ? "0" : "") + minutes
				+ ":" + (seconds< 10 ? "0" : "") + seconds );

	}
	
	/***
	 * Converts a time string of HH:MM:SS format to seconds.
	 * <p>
	 * Example:
	 * 		"00:01:30" will return 90
	 * 		"04:20:01" will return 1441
	 * <p>
	 * 
	 * @param time the time string to convert
	 * @return     the value of the time in seconds
	 */
	public static int HHMMSSToSeconds(String hhmmss){
			String[] components = hhmmss.split(":");
			
			int hours = Integer.parseInt(components[0]) * 3600;
			int minutes = Integer.parseInt(components[1]) * 60;
			int seconds = Integer.parseInt(components[2]);
	        
	        return hours+minutes+seconds;
	}
	
	/***
	 * Get current time stamp in yy.MM.dd HH:mm:ss format
	 * <p>
	 * Example: 13.07.04 07:00:00
	 * <p>
	 * 
	 * @return the current time stamp
	 */
	public static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yy.MM.dd HH:mm:ss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}

	
	/***
	 * Get current time stamp with a specified format.
	 * <p>
	 * Returns the current time stamp formatted with the
	 * pattern given as an argument. Only accepts
	 * patterns/formats that SimpleDateFormat accepts.
	 * <p>
	 * 
	 * @return the current time stamp formatted in the pattern given
	 * @see    SimpleDateFormat
	 */
	public static String getCurrentTimeStamp(String timeFormat) {
	    SimpleDateFormat sdfDate = new SimpleDateFormat(timeFormat);
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	public static String getRandomDate() {
		Random mizer = new Random();
		int mo = mizer.nextInt(12) + 1;
		int d = mizer.nextInt(30) + 1;
		
		return "2016/" + (mo < 10 ? ("0" + mo) : mo) + "/" + (d < 10 ? ("0" + d) : d);
	}
	
	/**
	 * This class cannot be instantiated.
	 */
	 private TimeOperations(){
		//this prevents even the native class from 
		//calling this ctor as well
		throw new AssertionError();
	}
}
