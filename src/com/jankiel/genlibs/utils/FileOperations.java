package com.jankiel.genlibs.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/***
 * Class containing static methods that can be
 * used for some file operations.
 * <p>
 * Contains methods for various file operations
 * like file structure listing, numbered folder
 * creation etc.
 * <p>
 * 
 * @author jacatbay
 *
 */
public class FileOperations {
	
	/***
	 * For conversion purposes
	 */
	public static final String UNIT_BYTE = "byte";
	public static final String UNIT_KILOBYTE = "kilobyte";
	public static final String UNIT_MEGABYTE = "megabyte";
	public static final String UNIT_GIGABYTE = "gigabyte";
	public static final String UNIT_TERABYTE = "terabyte";
	
	/***
	 * Lists all the files and subfolders 
	 * recursively of this path.
	 * <p>
	 * This function does a depth first search
	 * style of traversal.
	 * <p>
	 * 
	 * @param path the path that will be used as root
	 * @return a large string containing the file and folders
	 */
	public static String listFilesInPath(String path){
		return listFilesInPath(path, "", false);
	}
	
	/***
	 * Lists all the files and subfolders 
	 * recursively of this path including the
	 * file sizes for each file.
	 * <p>
	 * This function does a depth first search
	 * style of traversal.
	 * <p>
	 * 
	 * @param path the path that will be used as root
	 * @return a large string containing the file and folders
	 */
	public static String listFilesInPathWithSize(String path){
		return listFilesInPath(path, "", true);
	}
	
	/***
	 * Lists all the files and subfolders 
	 * recursively of this path.
	 * <p>
	 * This function does a depth first search
	 * style of traversal. This function is used
	 * by the same function with the same name to
	 * list the files in an indented manner.
	 * <p>
	 * 
	 * @param path the path that will be used as root
	 * @param indention the indention that will be used
	 * @return a large string containing the file and folders
	 */
	private static String listFilesInPath(String path, String indention, boolean willPrintSize){
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		StringBuilder stringBuilder = new StringBuilder("");
		
		if(listOfFiles == null)
			return "";

		for (int i = 0; i < listOfFiles.length; i++) {
			
			stringBuilder.append(indention + listOfFiles[i].getName()).append("\n");
			
			if(willPrintSize){
				//file size
				if(listOfFiles[i].isFile()){
					long fileSize = listOfFiles[i].length();
					stringBuilder.append(indention + "Size: " + convertByteToLargestUnit(fileSize)).append("\n");
				}
			}
			
			//recursive call
			if (listOfFiles[i].isDirectory()) 
				stringBuilder.append(listFilesInPath(path + "\\" + listOfFiles[i].getName(), indention + "\t", willPrintSize));
			
		}
		
		return stringBuilder.toString();
	}
	
	/***
	 * Converts a byte to the largest Unit.
	 * e.g. 79862313 will be converted to
	 * 76.16Mb.
	 * <p>
	 * 
	 * @param byteToConvert the byte to convert
	 * @return the same byte converted to largest unit
	 */
	public static String convertByteToLargestUnit(long byteToConvert){
		
		double byteConverted = byteToConvert;
		int unitBase = 0; 
		
		while(byteConverted > 1000){
			byteConverted = byteConverted/1024;
			unitBase++;
		}
		
		if(unitBase == 4)
			return StringTransformations.formatNumberIntoTwoDecimalPlaces(byteConverted) + "Tb";
		else if (unitBase == 3)
			return StringTransformations.formatNumberIntoTwoDecimalPlaces(byteConverted) + "Gb";
		else if (unitBase == 2)
			return StringTransformations.formatNumberIntoTwoDecimalPlaces(byteConverted) + "Mb";
		else if (unitBase == 1)
			return StringTransformations.formatNumberIntoTwoDecimalPlaces(byteConverted) + "Kb";
		else if (unitBase == 0)
			return StringTransformations.formatNumberIntoTwoDecimalPlaces(byteConverted) + "b";
		else return StringTransformations.formatNumberIntoTwoDecimalPlaces(byteToConvert) + "";
	}
	
	/***
	 * Creates numbered directories up to the limit given as a
	 * parameter. You can also choose to pad the names of the
	 * directories with zeroes (0) for easier sorting.
	 * <p>
	 * 
	 * @param parentDirectory the directories will be created inside it
	 * @param numOfDirs specifies how many directories will be created
	 * @param willPadWithZeroes set to true if directories will be padded
	 */
	public static void createNumberedDirectories(String parentDirectory, int numOfDirs,
		boolean willPadWithZeroes){
		
		for (int i = 1; i <= numOfDirs; i++) {;
			String filename = parentDirectory == null ? "" : parentDirectory;
			
			if(willPadWithZeroes) {
				int numOfDigits = String.valueOf(numOfDirs).length();
				
				filename = filename + StringTransformations.padInFront(String.valueOf(i), numOfDigits);
				
			} else filename = filename + i; 
			
			File directory = new File(filename);
			
			if(directory.mkdir()){
				System.out.println("Directory created.");
			} else {
				System.out.println("Directory not created.");
			}
        }
    }
	
	@SuppressWarnings("unused")
	private static String listFilesInPathFormatToXml(String path, String indention){
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		StringBuilder stringBuilder = new StringBuilder("");
		
		if(listOfFiles == null)
			return "";

		for (int i = 0; i < listOfFiles.length; i++) {
			File file = listOfFiles[i]; 
			
			if (file.isFile()) {
				stringBuilder.append(indention + "<file name=\"" + file.getName() + "\"\n");
				
				long fileSize = file.length();
				stringBuilder.append(indention + "      size=\"" + convertByteToLargestUnit(fileSize) + "\"\n");

				stringBuilder.append(indention + "      format=\"" + getFileFormat(file) + "\"\n");
				
				stringBuilder.append(indention + "      resolution=\"" + "" + "\" />\n");
			}
			
			//recursive call
			if (file.isDirectory())  {
				if (indention.equals("")) {
					stringBuilder.append(indention + "<root name=\"" + file.getName() + "\">\n");
				} else {
					stringBuilder.append(indention + "<folder name=\"" + file.getName() + "\">\n");
				}
				
				stringBuilder.append(listFilesInPathFormatToXml(path + "\\" + file.getName(), indention + "\t"));
				
				if (indention.equals("")) {
					stringBuilder.append(indention + "</root>\n");
				} else {
					stringBuilder.append(indention + "</folder>\n");
				}
			}
			
		}
		
		return stringBuilder.toString();
	}
	
	public static String getFileFormat(File file) {
		if (file.isDirectory()) {
			return "directory";
		}

		return getFileFormat(file.getName());
	}
	
	public static String getFileFormat(String filepath) {
		for (int i = filepath.length()-1; i >= 0; i--) {
			if (filepath.charAt(i) == '.') {
				return filepath.substring(i+1, filepath.length());
			}
		}
		
		return "";
	}
	
	public static void removeUrlEncodingOnFilesInside(String path) {
		File root = new File(path);
		
		File[] listOfFiles = root.listFiles();
		
		if (listOfFiles == null || listOfFiles.length == 0) {
			return;
		}
		
		for (File file : listOfFiles) {
			
			if (file.isDirectory()) {
				removeUrlEncodingOnFilesInside(file.getAbsolutePath());
			} else if (file.isFile()) {
				String fileName = file.getName();
				
				try {
					String decodedFileName = URLDecoder.decode(fileName, "UTF-8");
					
					if (!fileName.equals(decodedFileName)) {
						File parentFile = file.getParentFile();
						File fileWithDecodedFileName = new File(parentFile.getAbsolutePath() + File.separator + decodedFileName);
						
						boolean renameSuccess = file.renameTo(fileWithDecodedFileName);
						
						if (renameSuccess) {
							System.out.println("Renaming " + fileName + " to " + decodedFileName + " was successful.");
						} else {
							System.out.println("Renaming " + fileName + " to " + decodedFileName + " ended up in a failure.");
						}
					}
					
				} catch (UnsupportedEncodingException e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
	
	/**
	 * This class cannot be instantiated.
	 */
	 private FileOperations(){
		//this prevents even the native class from 
		//calling this ctor as well
		throw new AssertionError();
	}
}