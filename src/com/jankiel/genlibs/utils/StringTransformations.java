package com.jankiel.genlibs.utils;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Random;

/***
 * Class containing static methods that can be used for string transformations.
 * <p>
 * Contains methods for various String transformations, for example,
 * capitalizing first letter of the string, converting number to words etc.
 * <p>
 *
 * @author jacatbay
 *
 */
public class StringTransformations {

    /***
     * Converts a numeric value to HH:MM:SS format.
     * <p>
     *
     * @param seconds the numeric value to convert
     * @return the converted value in HH:MM:SS format
     */
    public static String convertSecondsToHHMMSS(String seconds) {
        if (seconds == null || seconds.equals("")) {
            return "00:00:00";
        }

        return TimeOperations.secondsToHHMMSS(Integer.parseInt(seconds));
    }

    /***
     * Converts a numeric value to HH:MM:SS format.
     * <p>
     *
     * @param seconds the numeric value to convert
     * @return the converted value in HH:MM:SS format
     */
    public static String convertSecondsToHHMMSS(int seconds) {
        return TimeOperations.secondsToHHMMSS(seconds);
    }

    /***
     * Converts a time string of HH:MM:SS format to seconds.
     * <p>
     * Example: "00:01:30" will return 90 "04:20:01" will return 1441
     * <p>
     *
     * @param time the time string to convert
     * @return the value of the time in seconds
     */
    public static String convertHHMMSSToSeconds(String time) {
        if (time == null || time.equals("")) {
            return "0";
        }

        return String.valueOf(TimeOperations.HHMMSSToSeconds(time));
    }

    /***
     * Converts a given numeric value to english sentential form.
     * <p>
     * This method returns the number into its english sentential form(e.g. 84
     * is EIGHTY FOUR). Always returns words in upper case. Current maximum
     * limit is the max value for int and current minimum limit is min value for
     * int + 1
     * <p>
     *
     * @param input the numeric value to convert
     * @return the numeric value in words
     * @see Integer.MAX_VALUE
     * @see Integer.MIN_VALUE
     */
    public static String convertNumberToWords(String input) {

        if (input == null || input.equals("")) {
            return "ZERO";
        }

        int number = Integer.parseInt(input);

        String convertedNumberInString = "";

        if (number < 0) {
            convertedNumberInString += "NEGATIVE ";
            number = -number;
        }

        if (number == 0) {
            convertedNumberInString += "ZERO ";
        }

        //process the billions first
        int remainder = number / 1000000000; // 1 Billion

        switch (remainder) {
            case 1:
                convertedNumberInString += "ONE ";
                break;
            case 2:
                convertedNumberInString += "TWO ";
                break;
            case 3:
                convertedNumberInString += "THREE ";
                break;
            case 4:
                convertedNumberInString += "FOUR ";
                break;
            case 5:
                convertedNumberInString += "FIVE ";
                break;
            case 6:
                convertedNumberInString += "SIX ";
                break;
            case 7:
                convertedNumberInString += "SEVEN ";
                break;
            case 8:
                convertedNumberInString += "EIGHT ";
                break;
            case 9:
                convertedNumberInString += "NINE ";
                break;
            default:
                break;
        }

        if (remainder > 0) {
            convertedNumberInString += "BILLION ";
        }

        //get the remainder,
        number %= 1000000000;
        //process the hundred millions
        remainder = number / 100000000;
        int hchk = remainder;

        switch (remainder) {
            case 1:
                convertedNumberInString += "ONE HUNDRED ";
                break;
            case 2:
                convertedNumberInString += "TWO HUNDRED ";
                break;
            case 3:
                convertedNumberInString += "THREE HUNDRED ";
                break;
            case 4:
                convertedNumberInString += "FOUR HUNDRED ";
                break;
            case 5:
                convertedNumberInString += "FIVE HUNDRED ";
                break;
            case 6:
                convertedNumberInString += "SIX HUNDRED ";
                break;
            case 7:
                convertedNumberInString += "SEVEN HUNDRED ";
                break;
            case 8:
                convertedNumberInString += "EIGHT HUNDRED ";
                break;
            case 9:
                convertedNumberInString += "NINE HUNDRED ";
                break;
            default:
                break;
        }

        //flag if remainder is 11-19 million
        boolean isTwoDigitsMillions = false;

        //get remainder
        number %= 100000000;
        //process the ten millions
        remainder = number / 10000000;
        int tchk = remainder;

        if (remainder == 1 && number % 10000000 == 0) {
            convertedNumberInString += "TEN ";
        }
        switch (remainder) {
            case 1:
                isTwoDigitsMillions = true;
                break;
            case 2:
                convertedNumberInString += "TWENTY ";
                break;
            case 3:
                convertedNumberInString += "THIRTY ";
                break;
            case 4:
                convertedNumberInString += "FORTY ";
                break;
            case 5:
                convertedNumberInString += "FIFTY ";
                break;
            case 6:
                convertedNumberInString += "SIXTY ";
                break;
            case 7:
                convertedNumberInString += "SEVENTY ";
                break;
            case 8:
                convertedNumberInString += "EIGHTY ";
                break;
            case 9:
                convertedNumberInString += "NINETY ";
                break;
            default:
                break;
        }

        //get remainder
        number %= 10000000;
        //process the millions
        remainder = number / 1000000; //1 Million

        if (isTwoDigitsMillions) {
            switch (remainder) {
                case 1:
                    convertedNumberInString += "ELEVEN ";
                    break;
                case 2:
                    convertedNumberInString += "TWELVE ";
                    break;
                case 3:
                    convertedNumberInString += "THIRTEEN ";
                    break;
                case 4:
                    convertedNumberInString += "FOURTEEN ";
                    break;
                case 5:
                    convertedNumberInString += "FIFTEEN ";
                    break;
                case 6:
                    convertedNumberInString += "SIXTEEN ";
                    break;
                case 7:
                    convertedNumberInString += "SEVENTEEN ";
                    break;
                case 8:
                    convertedNumberInString += "EIGHTEEN ";
                    break;
                case 9:
                    convertedNumberInString += "NINETEEN ";
                    break;
                default:
                    break;
            }
        } else {
            switch (remainder) {
                case 1:
                    convertedNumberInString += "ONE ";
                    break;
                case 2:
                    convertedNumberInString += "TWO ";
                    break;
                case 3:
                    convertedNumberInString += "THREE ";
                    break;
                case 4:
                    convertedNumberInString += "FOUR ";
                    break;
                case 5:
                    convertedNumberInString += "FIVE ";
                    break;
                case 6:
                    convertedNumberInString += "SIX ";
                    break;
                case 7:
                    convertedNumberInString += "SEVEN ";
                    break;
                case 8:
                    convertedNumberInString += "EIGHT ";
                    break;
                case 9:
                    convertedNumberInString += "NINE ";
                    break;
                default:
                    break;
            }
        }

        if (hchk > 0 || tchk > 0 || remainder > 0) {
            convertedNumberInString += "MILLION ";
        }

        //get remainder
        number %= 1000000;
        //process hundred thousands
        remainder = number / 100000;
        hchk = remainder;

        switch (remainder) {
            case 1:
                convertedNumberInString += "ONE HUNDRED ";
                break;
            case 2:
                convertedNumberInString += "TWO HUNDRED ";
                break;
            case 3:
                convertedNumberInString += "THREE HUNDRED ";
                break;
            case 4:
                convertedNumberInString += "FOUR HUNDRED ";
                break;
            case 5:
                convertedNumberInString += "FIVE HUNDRED ";
                break;
            case 6:
                convertedNumberInString += "SIX HUNDRED ";
                break;
            case 7:
                convertedNumberInString += "SEVEN HUNDRED ";
                break;
            case 8:
                convertedNumberInString += "EIGHT HUNDRED ";
                break;
            case 9:
                convertedNumberInString += "NINE HUNDRED ";
                break;
            default:
                break;
        }

        //flag if two digits
        boolean isTwoDigitsThousands = false;

        //get remainder
        number %= 100000;
        //process ten thousands
        remainder = number / 10000;
        tchk = remainder;

        if (remainder == 1 && number % 10000 == 0) {
            convertedNumberInString += "TEN ";
        }
        switch (remainder) {
            case 1:
                isTwoDigitsThousands = true;
                break;
            case 2:
                convertedNumberInString += "TWENTY ";
                break;
            case 3:
                convertedNumberInString += "THIRTY ";
                break;
            case 4:
                convertedNumberInString += "FORTY ";
                break;
            case 5:
                convertedNumberInString += "FIFTY ";
                break;
            case 6:
                convertedNumberInString += "SIXTY ";
                break;
            case 7:
                convertedNumberInString += "SEVENTY ";
                break;
            case 8:
                convertedNumberInString += "EIGHTY ";
                break;
            case 9:
                convertedNumberInString += "NINETY ";
                break;
            default:
                break;
        }

        //get remainder
        number %= 10000;
        //process thousands
        remainder = number / 1000; //1 Thousand

        if (isTwoDigitsThousands) {
            switch (remainder) {
                case 1:
                    convertedNumberInString += "ELEVEN ";
                    break;
                case 2:
                    convertedNumberInString += "TWELVE ";
                    break;
                case 3:
                    convertedNumberInString += "THIRTEEN ";
                    break;
                case 4:
                    convertedNumberInString += "FOURTEEN ";
                    break;
                case 5:
                    convertedNumberInString += "FIFTEEN ";
                    break;
                case 6:
                    convertedNumberInString += "SIXTEEN ";
                    break;
                case 7:
                    convertedNumberInString += "SEVENTEEN ";
                    break;
                case 8:
                    convertedNumberInString += "EIGHTEEN ";
                    break;
                case 9:
                    convertedNumberInString += "NINETEEN ";
                    break;
                default:
                    break;
            }
        } else {
            switch (remainder) {
                case 1:
                    convertedNumberInString += "ONE ";
                    break;
                case 2:
                    convertedNumberInString += "TWO ";
                    break;
                case 3:
                    convertedNumberInString += "THREE ";
                    break;
                case 4:
                    convertedNumberInString += "FOUR ";
                    break;
                case 5:
                    convertedNumberInString += "FIVE ";
                    break;
                case 6:
                    convertedNumberInString += "SIX ";
                    break;
                case 7:
                    convertedNumberInString += "SEVEN ";
                    break;
                case 8:
                    convertedNumberInString += "EIGHT ";
                    break;
                case 9:
                    convertedNumberInString += "NINE ";
                    break;
                default:
                    break;
            }
        }

        if (hchk > 0 || tchk > 0 || remainder > 0) {
            convertedNumberInString += "THOUSAND ";
        }

        //get remainder
        number %= 1000;
        //process hundreds
        remainder = number / 100;

        switch (remainder) {
            case 1:
                convertedNumberInString += "ONE HUNDRED ";
                break;
            case 2:
                convertedNumberInString += "TWO HUNDRED ";
                break;
            case 3:
                convertedNumberInString += "THREE HUNDRED ";
                break;
            case 4:
                convertedNumberInString += "FOUR HUNDRED ";
                break;
            case 5:
                convertedNumberInString += "FIVE HUNDRED ";
                break;
            case 6:
                convertedNumberInString += "SIX HUNDRED ";
                break;
            case 7:
                convertedNumberInString += "SEVEN HUNDRED ";
                break;
            case 8:
                convertedNumberInString += "EIGHT HUNDRED ";
                break;
            case 9:
                convertedNumberInString += "NINE HUNDRED ";
                break;
            default:
                break;
        }

        //if is two digits
        boolean isTwoDigitsOnes = false;

        //get remainder
        number %= 100;
        //process tens
        remainder = number / 10;

        if (remainder == 1 && number % 10 == 0) {
            convertedNumberInString += "TEN ";
        }
        switch (remainder) {
            case 1:
                isTwoDigitsOnes = true;
                break;
            case 2:
                convertedNumberInString += "TWENTY ";
                break;
            case 3:
                convertedNumberInString += "THIRTY ";
                break;
            case 4:
                convertedNumberInString += "FORTY ";
                break;
            case 5:
                convertedNumberInString += "FIFTY ";
                break;
            case 6:
                convertedNumberInString += "SIXTY ";
                break;
            case 7:
                convertedNumberInString += "SEVENTY ";
                break;
            case 8:
                convertedNumberInString += "EIGHTY ";
                break;
            case 9:
                convertedNumberInString += "NINETY ";
                break;
            default:
                break;
        }

        //get remainder
        number %= 10;
        //process ones
        remainder = number;

        if (isTwoDigitsOnes) {
            switch (remainder) {
                case 1:
                    convertedNumberInString += "ELEVEN ";
                    break;
                case 2:
                    convertedNumberInString += "TWELVE ";
                    break;
                case 3:
                    convertedNumberInString += "THIRTEEN ";
                    break;
                case 4:
                    convertedNumberInString += "FOURTEEN ";
                    break;
                case 5:
                    convertedNumberInString += "FIFTEEN ";
                    break;
                case 6:
                    convertedNumberInString += "SIXTEEN ";
                    break;
                case 7:
                    convertedNumberInString += "SEVENTEEN ";
                    break;
                case 8:
                    convertedNumberInString += "EIGHTEEN ";
                    break;
                case 9:
                    convertedNumberInString += "NINETEEN ";
                    break;
                default:
                    break;
            }
        } else {
            switch (remainder) {
                case 1:
                    convertedNumberInString += "ONE ";
                    break;
                case 2:
                    convertedNumberInString += "TWO ";
                    break;
                case 3:
                    convertedNumberInString += "THREE ";
                    break;
                case 4:
                    convertedNumberInString += "FOUR ";
                    break;
                case 5:
                    convertedNumberInString += "FIVE ";
                    break;
                case 6:
                    convertedNumberInString += "SIX ";
                    break;
                case 7:
                    convertedNumberInString += "SEVEN ";
                    break;
                case 8:
                    convertedNumberInString += "EIGHT ";
                    break;
                case 9:
                    convertedNumberInString += "NINE ";
                    break;
                default:
                    break;
            }
        }

        //remove trailing space
        return convertedNumberInString.substring(0, convertedNumberInString.length() - 1);
    }
    
    //TODO add a flag to return whether 1st or first
    public static String convertNumberToOrdinal(int number) {
        return convertNumberToOrdinal(padInFront(number, 2));
    }
    
    public static String convertNumberToOrdinal(String paddedNumber) {
        //remove padded zeroes by converting to int and back to String
        int numeral = Integer.parseInt(paddedNumber);
        String number = String.valueOf(numeral);
        
        //get the last two digits, including the extra '0'
        //if the string is padded
        String lastTwoDigits = paddedNumber; 
        if (paddedNumber.length() > 2) {
            lastTwoDigits = paddedNumber.substring(paddedNumber.length() - 2);
        }
        
        if (!Character.isDigit(lastTwoDigits.charAt(0)) 
                || !Character.isDigit(lastTwoDigits.charAt(1)) ) {
            return "";
        }
        
        char lastDigit = lastTwoDigits.charAt(1);
        
        if (lastDigit == '1' && !lastTwoDigits.equals("11")) {
            return number + "st";
        }
        if (lastDigit == '2' && !lastTwoDigits.equals("12")) {
            return number + "nd";
        }
        if (lastDigit == '3' && !lastTwoDigits.equals("13")) {
            return number + "rd";
        }
        
        return number + "th";
    }
    

    /***
     * Transforms the first letter of the given string to upper case. Useful for
     * proper nouns. Does not modify other parts of the string except the first
     * character
     * <p>
     *
     * @param str the string to transform
     * @return same string with the first letter to upper case
     */
    public static String firstLetterToUpperCase(String str) {
        if (str == null || str.equals("")) {
            return "";
        }

        String firstLetter = String.valueOf(str.charAt(0));

        return firstLetter.toUpperCase() + str.substring(1, str.length());
    }

    /***
     * Transforms the first letter of the given string to lower case. Useful for
     * common nouns. Does not modify other parts of the string except the first
     * character
     * <p>
     *
     * @param str the string to transform
     * @return same string with the first letter to lower case
     */
    public static String firstLetterToLowerCase(String str) {
        if (str == null || str.equals("")) {
            return "";
        }

        String firstLetter = String.valueOf(str.charAt(0));

        return firstLetter.toLowerCase() + str.substring(1, str.length());
    }

    /***
     * Enlarges the given text using HTML font tag. Unit is em. Useful for
     * enlarging text on dialogs.
     * <p>
     *
     * @param toEnlare the string to enlarge
     * @param size the size that will be used in enlarging the string
     * @return the same string wrapped in html tags
     */
    public static String enlargeUsingHtml(String toEnlarge, int size) {
        return "<html><span style='font-size:" + size + "em'>" + toEnlarge + "</span></html>";
    }

    /***
     * Formats a given number (floating point or not) into a number with two
     * decimal places. Decimal will be truncated and not rounded off.
     * <p>
     * Examples: 
     * 	4 		=	4.00
     * 	4.675 	=	4.67
     * 	4.5 	=	4.50
     * <p>
     *
     * @param number the number to format
     * @return the number with two decimal places
     */
    public static String formatNumberIntoTwoDecimalPlaces(String number) {
        String[] components = number.split("\\.");

        if (components.length < 2) {
            return number + ".00";
        }

        if (components[1].length() < 2) {
            components[1] += "0";
        }

        return components[0] + "." + components[1].substring(0, 2);
    }

    /***
     * Formats a given number (floating point or not) into a number with two
     * decimal places. Decimal will be truncated and not rounded off.
     * <p>
     * Examples: 
     * 	4 		=	4.00
     * 	4.675 	=	4.67
     * 	4.5 	=	4.50
     * <p>
     *
     * @param number the number to format
     * @return the number with two decimal places
     */
    public static String formatNumberIntoTwoDecimalPlaces(double number) {
        return formatNumberIntoTwoDecimalPlaces(Double.toString(number));
    }

    /***
     * Put zeroes(0) before a number depending on the required number of digits.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad.
     * <p>
     *
     * @param number the number to be padded
     * @param numOfDigits the number of digits
     * @return the padded number
     */
    public static String padInFront(int number, int numOfDigits) {
        return pad(Integer.toString(number), "0", numOfDigits, true);
    }

    /***
     * Put zeroes(0) before a number depending on the required number of digits.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad.
     * <p>
     *
     * @param number the number to be padded
     * @param numOfDigits the number of digits
     * @return the padded number
     */
    public static String padInFront(String number, int numOfDigits) {
        return pad(number, "0", numOfDigits, true);
    }

    /***
     * Put characters before a string depending on the required length.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad.
     * <p>
     *
     * @param string the string to be padded
     * @param charToBePadded the character to be padded to the string
     * @param length the required length
     * @return the padded string
     */
    public static String padInFront(String string, String charToBePadded, int length) {
        return pad(string, charToBePadded, length, true);
    }

    /***
     * Put zeroes(0) after a number depending on the required number of digits.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad.
     * <p>
     *
     * @param number the number to be padded
     * @param numOfDigits the number of digits
     * @return the padded number
     */
    public static String padAtTheEnd(int number, int numOfDigits) {
        return pad(Integer.toString(number), "0", numOfDigits, false);
    }

    /***
     * Put zeroes(0) after a number depending on the required number of digits.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad.
     * <p>
     *
     * @param number the number to be padded
     * @param numOfDigits the number of digits
     * @return the padded number
     */
    public static String padAtTheEnd(String number, int numOfDigits) {
        return pad(number, "0", numOfDigits, false);
    }

    /***
     * Put characters after a string depending on the required length.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad.
     * <p>
     *
     * @param string the string to be padded
     * @param charToBePadded the character to be padded to the string
     * @param length the required length
     * @return the padded string
     */
    public static String padAtTheEnd(String string, String charToBePadded, int length) {
        return pad(string, charToBePadded, length, false);
    }
    
    /***
     * Put characters before or after a string depending on the required length.
     * Useful for formatting/sorting with Strings. Returns the string itself if
     * there's no need to pad. Please avoid using this directly, instead use 
     * padInFront or padAtTheEnd for your needs.
     * <p>
     *
     * @param string the string to be padded
     * @param charToBePadded the characer to be padded to the string
     * @param length the required length
     * @param toPadInFront 
     * @return the padded string
     */
    public static String pad(String string, String charToBePadded, int length, boolean toPadInFront) {
        if (length <= 1) {
            return string + "";
        }
        
        if(string.length() + charToBePadded.length() > length) {
            return string + "";
        }

        StringBuilder paddedString = new StringBuilder(string);
        while (paddedString.length() < length &&
        		paddedString.length() + charToBePadded.length() <= length) {
        	if(toPadInFront)
                paddedString = paddedString.insert(0, charToBePadded);
        	else 
        		paddedString = paddedString.append(charToBePadded);
        }

        return paddedString.toString();
    }
    
	/***
	 * Formats a string into all uppercase letters with the
	 * spaces as underscores. Useful for transforming a field
	 * variable name into a final variable name. Ignores camelCase.
	 * <p>
	 * 
	 * @param string the string to format
	 * @return the formatted string
	 */
    public static String formatToUpperCaseWithUnderscore(String string) {
    	return formatToUpperCaseWithUnderscore(string, false);
    }
    
    /***
	 * Formats a string into all uppercase letters with the
	 * spaces as underscores. Useful for transforming a field
	 * variable name into a final variable name. camelCase strings
	 * will be formatted to correct string if fromCamelCase is set
	 * to true. 
	 * <p>
	 * 
	 * @param string the string to format
	 * @param fromCamelCase flag set to perform formatting for camelCase
	 * @return the formatted string
	 */
    public static String formatToUpperCaseWithUnderscore(String string, boolean fromCamelCase) {
        StringBuilder formattedString = new StringBuilder("");
        
        for(int i = 0; i < string.length(); i++){
            if(string.charAt(i) == ' ') {
                formattedString.append("_");
        	} else if(fromCamelCase && Character.isUpperCase(string.charAt(i))) {
            		formattedString.append("_").append(string.charAt(i));
            } else formattedString.append((string.charAt(i) + "").toUpperCase());
        }
        
        return formattedString.toString();
    }
    
    /***
     * Creates a password of the length specified as 
     * parameter.
     * <p> 
     * 
     * @param length the length of the password created
     * @return a password of the length specified
     */
    public static String createPassword(int length) {
        char[] validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_+-".toCharArray();
        
        StringBuilder string = new StringBuilder();
        Random mizer = new Random();
        while (0 < length--) {
        	string.append(validCharacters[mizer.nextInt(validCharacters.length)]);
        }
        
        return string.toString();
    }
    
    
    //http://stackoverflow.com/a/24575417/2431281
    public static final String unescapeHtml3(final String input) {
        StringWriter writer = null;
        int len = input.length();
        int i = 1;
        int st = 0;
        while (true) {
            // look for '&'
            while (i < len && input.charAt(i-1) != '&')
                i++;
            if (i >= len)
                break;

            // found '&', look for ';'
            int j = i;
            while (j < len && j < i + MAX_ESCAPE + 1 && input.charAt(j) != ';')
                j++;
            if (j == len || j < i + MIN_ESCAPE || j == i + MAX_ESCAPE + 1) {
                i++;
                continue;
            }

            // found escape 
            if (input.charAt(i) == '#') {
                // numeric escape
                int k = i + 1;
                int radix = 10;

                final char firstChar = input.charAt(k);
                if (firstChar == 'x' || firstChar == 'X') {
                    k++;
                    radix = 16;
                }

                try {
                    int entityValue = Integer.parseInt(input.substring(k, j), radix);

                    if (writer == null) 
                        writer = new StringWriter(input.length());
                    writer.append(input.substring(st, i - 1));

                    if (entityValue > 0xFFFF) {
                        final char[] chrs = Character.toChars(entityValue);
                        writer.write(chrs[0]);
                        writer.write(chrs[1]);
                    } else {
                        writer.write(entityValue);
                    }

                } catch (NumberFormatException ex) { 
                    i++;
                    continue;
                }
            }
            else {
                // named escape
                CharSequence value = lookupMap.get(input.substring(i, j));
                if (value == null) {
                    i++;
                    continue;
                }

                if (writer == null) 
                    writer = new StringWriter(input.length());
                writer.append(input.substring(st, i - 1));

                writer.append(value);
            }

            // skip escape
            st = j + 1;
            i = st;
        }

        if (writer != null) {
            writer.append(input.substring(st, len));
            return writer.toString();
        }
        return input;
    }

    private static final String[][] ESCAPES = {
        {"\"",     "quot"}, // " - double-quote
        {"&",      "amp"}, // & - ampersand
        {"<",      "lt"}, // < - less-than
        {">",      "gt"}, // > - greater-than

        // Mapping to escape ISO-8859-1 characters to their named HTML 3.x equivalents.
        {"\u00A0", "nbsp"}, // non-breaking space
        {"\u00A1", "iexcl"}, // inverted exclamation mark
        {"\u00A2", "cent"}, // cent sign
        {"\u00A3", "pound"}, // pound sign
        {"\u00A4", "curren"}, // currency sign
        {"\u00A5", "yen"}, // yen sign = yuan sign
        {"\u00A6", "brvbar"}, // broken bar = broken vertical bar
        {"\u00A7", "sect"}, // section sign
        {"\u00A8", "uml"}, // diaeresis = spacing diaeresis
        {"\u00A9", "copy"}, // © - copyright sign
        {"\u00AA", "ordf"}, // feminine ordinal indicator
        {"\u00AB", "laquo"}, // left-pointing double angle quotation mark = left pointing guillemet
        {"\u00AC", "not"}, // not sign
        {"\u00AD", "shy"}, // soft hyphen = discretionary hyphen
        {"\u00AE", "reg"}, // ® - registered trademark sign
        {"\u00AF", "macr"}, // macron = spacing macron = overline = APL overbar
        {"\u00B0", "deg"}, // degree sign
        {"\u00B1", "plusmn"}, // plus-minus sign = plus-or-minus sign
        {"\u00B2", "sup2"}, // superscript two = superscript digit two = squared
        {"\u00B3", "sup3"}, // superscript three = superscript digit three = cubed
        {"\u00B4", "acute"}, // acute accent = spacing acute
        {"\u00B5", "micro"}, // micro sign
        {"\u00B6", "para"}, // pilcrow sign = paragraph sign
        {"\u00B7", "middot"}, // middle dot = Georgian comma = Greek middle dot
        {"\u00B8", "cedil"}, // cedilla = spacing cedilla
        {"\u00B9", "sup1"}, // superscript one = superscript digit one
        {"\u00BA", "ordm"}, // masculine ordinal indicator
        {"\u00BB", "raquo"}, // right-pointing double angle quotation mark = right pointing guillemet
        {"\u00BC", "frac14"}, // vulgar fraction one quarter = fraction one quarter
        {"\u00BD", "frac12"}, // vulgar fraction one half = fraction one half
        {"\u00BE", "frac34"}, // vulgar fraction three quarters = fraction three quarters
        {"\u00BF", "iquest"}, // inverted question mark = turned question mark
        {"\u00C0", "Agrave"}, // А - uppercase A, grave accent
        {"\u00C1", "Aacute"}, // Б - uppercase A, acute accent
        {"\u00C2", "Acirc"}, // В - uppercase A, circumflex accent
        {"\u00C3", "Atilde"}, // Г - uppercase A, tilde
        {"\u00C4", "Auml"}, // Д - uppercase A, umlaut
        {"\u00C5", "Aring"}, // Е - uppercase A, ring
        {"\u00C6", "AElig"}, // Ж - uppercase AE
        {"\u00C7", "Ccedil"}, // З - uppercase C, cedilla
        {"\u00C8", "Egrave"}, // И - uppercase E, grave accent
        {"\u00C9", "Eacute"}, // Й - uppercase E, acute accent
        {"\u00CA", "Ecirc"}, // К - uppercase E, circumflex accent
        {"\u00CB", "Euml"}, // Л - uppercase E, umlaut
        {"\u00CC", "Igrave"}, // М - uppercase I, grave accent
        {"\u00CD", "Iacute"}, // Н - uppercase I, acute accent
        {"\u00CE", "Icirc"}, // О - uppercase I, circumflex accent
        {"\u00CF", "Iuml"}, // П - uppercase I, umlaut
        {"\u00D0", "ETH"}, // Р - uppercase Eth, Icelandic
        {"\u00D1", "Ntilde"}, // С - uppercase N, tilde
        {"\u00D2", "Ograve"}, // Т - uppercase O, grave accent
        {"\u00D3", "Oacute"}, // У - uppercase O, acute accent
        {"\u00D4", "Ocirc"}, // Ф - uppercase O, circumflex accent
        {"\u00D5", "Otilde"}, // Х - uppercase O, tilde
        {"\u00D6", "Ouml"}, // Ц - uppercase O, umlaut
        {"\u00D7", "times"}, // multiplication sign
        {"\u00D8", "Oslash"}, // Ш - uppercase O, slash
        {"\u00D9", "Ugrave"}, // Щ - uppercase U, grave accent
        {"\u00DA", "Uacute"}, // Ъ - uppercase U, acute accent
        {"\u00DB", "Ucirc"}, // Ы - uppercase U, circumflex accent
        {"\u00DC", "Uuml"}, // Ь - uppercase U, umlaut
        {"\u00DD", "Yacute"}, // Э - uppercase Y, acute accent
        {"\u00DE", "THORN"}, // Ю - uppercase THORN, Icelandic
        {"\u00DF", "szlig"}, // Я - lowercase sharps, German
        {"\u00E0", "agrave"}, // а - lowercase a, grave accent
        {"\u00E1", "aacute"}, // б - lowercase a, acute accent
        {"\u00E2", "acirc"}, // в - lowercase a, circumflex accent
        {"\u00E3", "atilde"}, // г - lowercase a, tilde
        {"\u00E4", "auml"}, // д - lowercase a, umlaut
        {"\u00E5", "aring"}, // е - lowercase a, ring
        {"\u00E6", "aelig"}, // ж - lowercase ae
        {"\u00E7", "ccedil"}, // з - lowercase c, cedilla
        {"\u00E8", "egrave"}, // и - lowercase e, grave accent
        {"\u00E9", "eacute"}, // й - lowercase e, acute accent
        {"\u00EA", "ecirc"}, // к - lowercase e, circumflex accent
        {"\u00EB", "euml"}, // л - lowercase e, umlaut
        {"\u00EC", "igrave"}, // м - lowercase i, grave accent
        {"\u00ED", "iacute"}, // н - lowercase i, acute accent
        {"\u00EE", "icirc"}, // о - lowercase i, circumflex accent
        {"\u00EF", "iuml"}, // п - lowercase i, umlaut
        {"\u00F0", "eth"}, // р - lowercase eth, Icelandic
        {"\u00F1", "ntilde"}, // с - lowercase n, tilde
        {"\u00F2", "ograve"}, // т - lowercase o, grave accent
        {"\u00F3", "oacute"}, // у - lowercase o, acute accent
        {"\u00F4", "ocirc"}, // ф - lowercase o, circumflex accent
        {"\u00F5", "otilde"}, // х - lowercase o, tilde
        {"\u00F6", "ouml"}, // ц - lowercase o, umlaut
        {"\u00F7", "divide"}, // division sign
        {"\u00F8", "oslash"}, // ш - lowercase o, slash
        {"\u00F9", "ugrave"}, // щ - lowercase u, grave accent
        {"\u00FA", "uacute"}, // ъ - lowercase u, acute accent
        {"\u00FB", "ucirc"}, // ы - lowercase u, circumflex accent
        {"\u00FC", "uuml"}, // ь - lowercase u, umlaut
        {"\u00FD", "yacute"}, // э - lowercase y, acute accent
        {"\u00FE", "thorn"}, // ю - lowercase thorn, Icelandic
        {"\u00FF", "yuml"}, // я - lowercase y, umlaut
    };

    private static final int MIN_ESCAPE = 2;
    private static final int MAX_ESCAPE = 6;

    private static final HashMap<String, CharSequence> lookupMap;
    static {
        lookupMap = new HashMap<String, CharSequence>();
        for (final CharSequence[] seq : ESCAPES) 
            lookupMap.put(seq[1].toString(), seq[0]);
    }
    
	/**
	 * This class cannot be instantiated.
	 */
	 private StringTransformations(){
		//this prevents even the native class from 
		//calling this ctor as well
		throw new AssertionError();
	}
}
