## Non-affiliation ##

The App and its Author are not, in any way, affiliated with Good Smile Company and its affiliates. This app is not an official Nendoroid app by Good Smile Company, just an app made by a bored person having problems in managing his currently owned Nendoroids and the Nendoroids he is looking for.

## Data Ownership ##

All of the data in this app, the Nendoroid details, images, user-generated content, and other data are owned by their respective owners. The Author does not own any of the data in this app. Usage of these data, whether user-generated or not, copyrighted or not, falls under [Fair Use](http://www.copyright.gov/fair-use/more-info.html). 

Most of the data used in this App is directly obtained from the Good Smile Company in their website: [http://www.goodsmile.info/en/](http://www.goodsmile.info/en/). For Nendoroids not present in their website, they were obtained through other sources. A Nendoroid's main data source is indicated on its GSC Page/Reference Page. Primary usage of date includes management of currently owned Nendoroid(s), management of Nendoroid(s) planned to be obtained in the future, and as a quick reference for all the Nendoroids currently in existence. More on Fair Use [here](https://www.teachingcopyright.org/handout/fair-use-faq).

## License ##

This App is licensed under **Apache Software License, Version 2.0**

> Copyright (C) 2016 Nendoroid Hunter
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
> &nbsp;&nbsp;&nbsp;&nbsp;http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.